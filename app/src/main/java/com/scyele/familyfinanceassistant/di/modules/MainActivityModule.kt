package com.scyele.familyfinanceassistant.di.modules

//import com.scyele.familyfinanceassistant.AssistantApplication
//import com.scyele.familyfinanceassistant.ui.main.MainActivityViewModelFactory
//import dagger.Module
//import dagger.Provides
//import javax.inject.Singleton
//
//@Module
//class MainActivityModule(private val assistantApplication: AssistantApplication) {
//
//  @Provides
//  @Singleton
//  fun provideMainActivityViewModelFactory() = MainActivityViewModelFactory()
//}