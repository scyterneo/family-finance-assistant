package com.scyele.familyfinanceassistant.utils

import java.util.*

object Guid {
  fun new(): String {
    return UUID.randomUUID().toString()
  }
}