package com.scyele.familyfinanceassistant.entity

interface Operation : AccountOperation {

  fun sum(): String

  fun accountCategoryGuid(): String

  fun operationCategoryGuid(): String

  fun comment(): String?
}

fun List<Operation>.find(guid: String): Operation {
  return this.first { it.guid() == guid }
}

enum class OperationType { INCOME, EXPENSE }