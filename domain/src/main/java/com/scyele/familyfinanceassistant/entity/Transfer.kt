package com.scyele.familyfinanceassistant.entity

import com.scyele.familyfinanceassistant.utils.Guid
import java.math.BigDecimal
import java.util.*

data class Transfer(
  val guid: String,
  var sum: String = "0",
  var fromAccountCategoryGuid: String,
  var toAccountCategoryGuid: String,
  var date: Date = Date(),
  var comment: String? = null
) : AccountOperation {

  constructor(
    sum: String,
    fromAccountCategoryGuid: String,
    toAccountCategoryGuid: String,
    date: Date = Date(),
    comment: String? = null
  ) : this(Guid.new(), sum, fromAccountCategoryGuid, toAccountCategoryGuid, date, comment)

  override fun guid(): String = guid

  override fun date(): Date = date

  fun sum(value: BigDecimal) {
    sum = value.toString()
  }

  // Empty private constructor for DataSnapshot.getValue method
  private constructor () : this("", "0", "", "")
}