package com.scyele.familyfinanceassistant.ui.expences.setup

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.MoveToRepository
import com.scyele.familyfinanceassistant.repository.OperationCategorySetupRepository
import com.scyele.familyfinanceassistant.summary.ExpenseSummary
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.operation.setup.OperationCategorySetupFragmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class ExpenseCategorySetupFragmentViewModel(
  operationCategorySetupRepository: OperationCategorySetupRepository,
  mainRepository: MainRepository,
  moveToRepository: MoveToRepository
) :
  OperationCategorySetupFragmentViewModel<Expense, ExpenseCategory, ExpenseSummary>(
    operationCategorySetupRepository,
    mainRepository,
    moveToRepository
  ) {

  override fun getOperations(): MutableLiveData<List<Expense>> {
    return mainRepository.expenses
  }

  override fun getOperationCategories(): MutableLiveData<List<ExpenseCategory>> {
    return mainRepository.expenseCategories
  }

  override fun createNewOperationCategory(
    name: String,
    isParent: Boolean,
    parentGuid: String?,
    guid: String
  ): OperationCategory {
    return ExpenseCategory(name, isParent, parentGuid, guid)
  }

  override fun getTypedEvent(): Event<OperationType> {
    return Event(OperationType.EXPENSE)
  }
}