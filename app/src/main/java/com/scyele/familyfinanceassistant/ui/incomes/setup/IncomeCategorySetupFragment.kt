package com.scyele.familyfinanceassistant.ui.incomes.setup

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.summary.IncomeSummary
import com.scyele.familyfinanceassistant.ui.operation.setup.OperationCategorySetupFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class IncomeCategorySetupFragment :
  OperationCategorySetupFragment<Income, IncomeCategory, IncomeSummary, IncomeCategorySetupFragmentViewModel>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<IncomeCategorySetupFragmentViewModel> {
    return IncomeCategorySetupFragmentViewModel::class.java
  }
}