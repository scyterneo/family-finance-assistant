package com.scyele.familyfinanceassistant.ui.incomes.edit

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.summary.IncomeSummary
import com.scyele.familyfinanceassistant.ui.operation.edit.OperationEditFragment

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class IncomeEditFragment : OperationEditFragment<
    Income, IncomeCategory, IncomeSummary, IncomeEditFragmentViewModel>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<IncomeEditFragmentViewModel> =
    IncomeEditFragmentViewModel::class.java

  override fun tag() = "EditIncomeFragment"

  override fun getHeaderText(): String {
    return getString(R.string.income)
  }
}