package com.scyele.lib.extension

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*

fun getDate(year: Int, month: Int, dayOfMonth: Int): Date {
  val calendar = getInstance()
  calendar.set(YEAR, year)
  calendar.set(MONTH, month)
  calendar.set(DAY_OF_MONTH, dayOfMonth)
  return calendar.time
}

internal const val dateDisplayingFormatPattern = "d MMMM yyyy"
internal const val weekDisplayingFormatPattern = "dd MMM yyyy"
internal const val monthDisplayingFormatPattern = "MMMM yyyy"

@SuppressLint("ConstantLocale")
internal val dateDisplayingFormat =
  SimpleDateFormat(dateDisplayingFormatPattern, Locale.getDefault())

@SuppressLint("ConstantLocale")
internal val weekDisplayingFormat =
  SimpleDateFormat(weekDisplayingFormatPattern, Locale.getDefault())

@SuppressLint("ConstantLocale")
internal val monthDisplayingFormat =
  SimpleDateFormat(monthDisplayingFormatPattern, Locale.getDefault())

fun Date.display(): String {
  return dateDisplayingFormat.format(this)
}

/**
 * Get the date of the first day in the week of the provided date
 * @return The date of the first week day
 */
fun Date.getWeekStartDate(): Date {
  val calendar = getInstance()
  calendar.firstDayOfWeek = MONDAY
  calendar.time = this
  setBeginningOfTheDay(calendar)
  while (calendar.get(DAY_OF_WEEK) != MONDAY) {
    calendar.add(DATE, -1)
  }
  return calendar.time
}

/**
 * Get the date of the last day in the week of the provided date
 * @return The date of the last week day
 */
fun Date.getWeekEndDate(firstDayOfWeek: Int): Date {
  val calendar = getInstance()
  calendar.firstDayOfWeek = firstDayOfWeek
  calendar.time = this
  setEndOfTheDay(calendar)
  var currentDayOfWeek = calendar.get(DAY_OF_WEEK)
  if (currentDayOfWeek == firstDayOfWeek) {
    calendar.add(DATE, 1)
  }
  currentDayOfWeek = calendar.get(DAY_OF_WEEK)
  while (currentDayOfWeek != firstDayOfWeek) {
    calendar.add(DATE, 1)
    currentDayOfWeek = calendar.get(DAY_OF_WEEK)
  }
  calendar.add(DATE, -1)
  return calendar.time
}

fun Date.isDateInRange(date: Date, selectionType: DateSelectorSelectionType): Boolean {
  if (selectionType == DateSelectorSelectionType.ALL) return true

  val (startDate, endDate) = date.getRange(selectionType)
  val isAfter = this.after(startDate)
  val isBefore = this.before(endDate)
  return isAfter && isBefore
}

fun Date.isDateInRange(startDate: Date?, endDate: Date?): Boolean {
  val calendar = getInstance()
  val correctStartDate: Date? =
    if (startDate == null) null
    else {
      calendar.time = startDate
      setBeginningOfTheDay(calendar)
      calendar.time
    }

  val correctEndDate: Date? =
    if (endDate == null) null
    else {
      calendar.time = endDate
      setEndOfTheDay(calendar)
      calendar.time
    }

  val isAfter =
    if (correctStartDate == null) true
    else this.after(correctStartDate)

  val isBefore =
    if (correctEndDate == null) true
    else this.before(correctEndDate)

  return isAfter && isBefore
}

fun Date.getRange(selectionType: DateSelectorSelectionType): Pair<Date, Date> {
  var startDate = Date()
  var endDate = Date()
  val calendar = getInstance()
  when (selectionType) {
    DateSelectorSelectionType.DAY -> {
      calendar.time = this
      setBeginningOfTheDay(calendar)
      startDate = calendar.time
      calendar.time = this
      setEndOfTheDay(calendar)
      endDate = calendar.time
    }
    DateSelectorSelectionType.WEEK -> {
      startDate = getWeekStartDate()
      endDate = getWeekEndDate(MONDAY)
    }
    DateSelectorSelectionType.MONTH -> {
      calendar.time = this
      setBeginningOfTheDay(calendar)
      calendar.set(DAY_OF_MONTH, 1)
      startDate = calendar.time
      setEndOfTheDay(calendar)
      calendar.set(DAY_OF_MONTH, calendar.getActualMaximum(DAY_OF_MONTH))
      endDate = calendar.time
    }
    DateSelectorSelectionType.YEAR -> {
      calendar.time = this
      setBeginningOfTheDay(calendar)
      calendar.set(DAY_OF_YEAR, 1)
      startDate = calendar.time
      setEndOfTheDay(calendar)
      calendar.set(DAY_OF_YEAR, calendar.getActualMaximum(DAY_OF_YEAR))
      endDate = calendar.time
    }
    DateSelectorSelectionType.ALL -> {
      calendar.time = this
      calendar.set(
        calendar.getActualMinimum(YEAR),
        calendar.getActualMinimum(MONTH),
        calendar.getActualMinimum(DAY_OF_MONTH),
        calendar.getActualMinimum(HOUR),
        calendar.getActualMinimum(MINUTE),
        calendar.getActualMinimum(SECOND)
      )
      startDate = calendar.time

      calendar.set(
        calendar.getActualMaximum(YEAR),
        calendar.getActualMaximum(MONTH),
        calendar.getActualMaximum(DAY_OF_MONTH),
        calendar.getActualMaximum(HOUR),
        calendar.getActualMaximum(MINUTE),
        calendar.getActualMaximum(SECOND)
      )
      endDate = calendar.time
    }
  }
  return Pair(startDate, endDate)
}

fun Date.getWeekText(): String {
  return weekDisplayingFormat.format(this.getWeekStartDate()) +
      " -- " +
      weekDisplayingFormat.format(this.getWeekEndDate(MONDAY))
}

fun Date.getMonthText(): String {
  val calendar = getInstance()
  calendar.time = this
  setBeginningOfTheDay(calendar)
  return monthDisplayingFormat.format(calendar.time)
}

private fun setBeginningOfTheDay(calendar: Calendar) {
  calendar.set(HOUR_OF_DAY, 0)
  calendar.set(MINUTE, 0)
  calendar.set(SECOND, 0)
  calendar.set(MILLISECOND, 0)
}

private fun setEndOfTheDay(calendar: Calendar) {
  calendar.set(HOUR_OF_DAY, 23)
  calendar.set(MINUTE, 59)
  calendar.set(SECOND, 59)
  calendar.set(MILLISECOND, 999)
}