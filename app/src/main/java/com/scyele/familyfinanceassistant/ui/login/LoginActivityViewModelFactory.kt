package com.scyele.familyfinanceassistant.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class LoginActivityViewModelFactory : ViewModelProvider.Factory {
  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(LoginActivityViewModel::class.java)) {
      @Suppress("UNCHECKED_CAST")
      return LoginActivityViewModel() as T
    }

    throw IllegalArgumentException("Unknown ViewModel class")
  }
}