package com.scyele.views.base

interface MvvmCustomViewModel<T: MvvmCustomViewState> {
    var state: T?
}