package com.scyele.familyfinanceassistant.repository

import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.FirebaseDatabase
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.db.Tables
import com.scyele.familyfinanceassistant.entity.*
import com.scyele.familyfinanceassistant.utils.extension.*
import com.scyele.lib.coroutines.main
import com.scyele.lib.coroutines.start
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import javax.inject.Inject



class MainRepository {

  val accountCategories: MutableLiveData<List<AccountCategory>> = MutableLiveData(emptyList())
  val incomeCategories: MutableLiveData<List<IncomeCategory>> = MutableLiveData(emptyList())
  val expenseCategories: MutableLiveData<List<ExpenseCategory>> = MutableLiveData(emptyList())
  val transfers: MutableLiveData<List<Transfer>> = MutableLiveData(emptyList())
  val incomes: MutableLiveData<List<Income>> = MutableLiveData(emptyList())
  val expenses: MutableLiveData<List<Expense>> = MutableLiveData(emptyList())
  val location: MutableLiveData<Location> = MutableLiveData()

  @Inject
  lateinit var database: FirebaseDatabase

  init {
    AssistantApplication.instance.firebaseComponent.inject(this)

    start {
      database.getListAsFlow(Tables.INCOME_CATEGORY_TABLE_NAME, IncomeCategory::class.java)
        .collect {
          main {
            incomeCategories.value = it
          }
        }
    }
    start {
      database.getListAsFlow(Tables.ACCOUNT_CATEGORY_TABLE_NAME, AccountCategory::class.java)
        .collect {
          main {
            accountCategories.value = it.sortedBy { it.name }
          }
        }
    }
    start {
      database.getListAsFlow(Tables.EXPENSE_CATEGORY_TABLE_NAME, ExpenseCategory::class.java)
        .collect {
          main {
            expenseCategories.value = it
          }
        }
    }
    start {
      database.getListAsFlow(Tables.TRANSFER_TABLE_NAME, Transfer::class.java)
        .collect {
          main {
            transfers.value = it
          }
        }
    }
    start {
      database.getListAsFlow(Tables.INCOME_TABLE_NAME, Income::class.java)
        .collect {
          main {
            incomes.value = it
          }
        }
    }
    start {
      database.getListAsFlow(Tables.EXPENSE_TABLE_NAME, Expense::class.java)
        .collect {
          main {
            expenses.value = it
          }
        }
    }

    database.getLocation({
      location.value = it
    }, {
      location.value = null
    })
  }

  fun setIncome(income: Income) {
    database.setIncome(income)
  }

  fun setExpense(expense: Expense) {
    database.setExpense(expense)
  }

  fun setOperation(operation: Operation) {
    if (operation is Expense) {
      database.setExpense(operation)
    } else if (operation is Income) {
      database.setIncome(operation)
    }
  }

  fun setOperationCategory(operationCategory: OperationCategory) {
    if (operationCategory is ExpenseCategory) {
      database.setExpenseCategory(operationCategory)
    } else if (operationCategory is IncomeCategory) {
      database.setIncomeCategory(operationCategory)
    }
  }

  fun setAccountCategory(accountCategory: AccountCategory) {
    database.setAccountCategory(accountCategory)
  }

  fun removeAccountCategory(accountCategory: AccountCategory) {
    database.removeAccountCategory(accountCategory)
  }

  fun setIncomeCategory(incomeCategory: IncomeCategory) {
    database.setIncomeCategory(incomeCategory)
  }

  fun removeOperation(operation: Operation) {
    if (operation is Expense) {
      database.removeExpense(operation)
    } else if (operation is Income) {
      database.removeIncome(operation)
    }
  }

  fun removeOperationCategory(operationCategory: OperationCategory) {
    if (operationCategory is ExpenseCategory) {
      database.removeExpenseCategory(operationCategory)
    } else if (operationCategory is IncomeCategory) {
      database.removeIncomeCategory(operationCategory)
    }
  }

  fun setPassword(password: String) {
    database.setPassword(password)
  }

  fun setTransfer(transfer: Transfer) {
    database.setTransfer(transfer)
  }

  fun removeTransfer(transfer: Transfer) {
    database.removeTransfer(transfer)
  }
}