package com.scyele.familyfinanceassistant.ui.history

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.*
import com.scyele.familyfinanceassistant.repository.*
import com.scyele.familyfinanceassistant.ui.SpinnerModel
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferRepository
import com.scyele.familyfinanceassistant.ui.base.BaseProgressScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.LoadingParts
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel
import com.scyele.familyfinanceassistant.utils.extension.data.belongsToAccountCategory
import com.scyele.familyfinanceassistant.utils.extension.data.belongsToOperationCategory
import com.scyele.familyfinanceassistant.utils.extension.data.find
import com.scyele.lib.extension.DateSelectorSelectionType
import com.scyele.lib.extension.isDateInRange
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*


class HistoryFragmentViewModel(
  val mainRepository: MainRepository,
  private val operationCategorySetupRepository: OperationCategorySetupRepository,
  private val accountCategorySetupRepository: AccountCategorySetupRepository,
  private val incomeEditRepository: IncomeEditRepository,
  private val expenseEditRepository: ExpenseEditRepository,
  private val transferRepository: TransferRepository
) : BaseProgressScreenVM<HistoryFragmentViewModel.State>() {

  override fun initState(): State = State()

  private var operations: List<AccountOperation> = listOf()
  private var operationCategories: List<OperationCategory> = listOf()

  private var incomes: List<Income> = listOf()
  private var expenses: List<Expense> = listOf()
  private var transfers: List<Transfer> = listOf()

  private var incomeCategories: List<IncomeCategory> = listOf()
  private var expenseCategories: List<ExpenseCategory> = listOf()
  private var accountCategories: List<AccountCategory> = listOf()

  private var selectedDate: Date = Date()
  private var selectedSelectionType: DateSelectorSelectionType = DateSelectorSelectionType.DAY

  private val operationHistoryModels: MutableLiveData<List<OperationHistoryModel>> =
    MutableLiveData(listOf())

  private val operationCategorySpinnerModel: MutableLiveData<SpinnerModel> = MutableLiveData()

  private val accountCategorySpinnerModel: MutableLiveData<SpinnerModel> = MutableLiveData()

  private var isOperationCategoryEditable: MutableLiveData<Boolean> = MutableLiveData()

  private var isAccountCategoryEditable: MutableLiveData<Boolean> = MutableLiveData()

  private var operationCategory: OperationCategory? =
    operationCategorySetupRepository.operationCategory.value

  private var accountCategory: AccountCategory? =
    accountCategorySetupRepository.accountCategory.value

  override fun getListOfLoadingParts(): MutableSet<LoadingParts> =
    HashSet(
      listOf(
        LoadingParts.INCOMES,
        LoadingParts.EXPENSES,
        LoadingParts.TRANSFERS,
        LoadingParts.ACCOUNT_CATEGORIES,
        LoadingParts.INCOME_CATEGORIES,
        LoadingParts.EXPENSE_CATEGORIES
      )
    )

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(mainRepository.incomes) {
      incomes = it
      loadingParts.remove(LoadingParts.INCOMES)
      update()
    }
    mediatorState.addSource(mainRepository.expenses) {
      expenses = it
      loadingParts.remove(LoadingParts.EXPENSES)
      update()
    }
    mediatorState.addSource(mainRepository.transfers) {
      transfers = it
      loadingParts.remove(LoadingParts.TRANSFERS)
      update()
    }
    mediatorState.addSource(mainRepository.incomeCategories) {
      incomeCategories = it
      loadingParts.remove(LoadingParts.INCOME_CATEGORIES)
      update()
    }
    mediatorState.addSource(mainRepository.expenseCategories) {
      expenseCategories = it
      loadingParts.remove(LoadingParts.EXPENSE_CATEGORIES)
      update()
    }
    mediatorState.addSource(mainRepository.accountCategories) {
      accountCategories = it
      loadingParts.remove(LoadingParts.ACCOUNT_CATEGORIES)
      update()
    }
    return mediatorState
  }

  fun getOperationHistoryModels() = operationHistoryModels

  fun getOperationCategorySpinnerModel() = operationCategorySpinnerModel

  fun getAccountCategorySpinnerModel() = accountCategorySpinnerModel

  fun onOperationClicked(guid: String) {
    val accountOperation = operations.find(guid)
    when (accountOperation) {
      is Income -> {
        incomeEditRepository.operation.value = accountOperation
        state.value = state.value.copy(editOperationEvent = Event(OperationType.INCOME))
      }
      is Expense -> {
        expenseEditRepository.operation.value = accountOperation
        state.value = state.value.copy(editOperationEvent = Event(OperationType.EXPENSE))
      }
      is Transfer -> {
        transferRepository.transfer.value = accountOperation
        state.value = state.value.copy(editTransferEvent = SimpleEvent())
      }
    }

  }

  fun onDateSelectionChanged(date: Date, selectionType: DateSelectorSelectionType) {
    selectedDate = date
    selectedSelectionType = selectionType
    update()
  }

  fun onEditOperationCategoryClicked() {
    operationCategorySetupRepository.operationCategory.value = operationCategory
    when (operationCategory) {
      is IncomeCategory -> {
        state.value = state.value.copy(editOperationCategoryEvent = Event(OperationType.INCOME))
      }
      is ExpenseCategory -> {
        state.value = state.value.copy(editOperationCategoryEvent = Event(OperationType.EXPENSE))
      }
    }
  }

  fun isOperationCategoryEditable() = isOperationCategoryEditable

  fun onEditAccountCategoryClicked() {
    accountCategorySetupRepository.accountCategory.value = accountCategory
    state.value = state.value.copy(editAccountCategoryEvent = SimpleEvent())
  }

  fun isAccountCategoryEditable() = isAccountCategoryEditable

  val onOperationCategorySelected: (position: Int) -> Unit = { position ->
    updateOperationCategoryPosition(position)
  }

  val onAccountCategorySelected: (position: Int) -> Unit = { position ->
    updateAccountCategoryPosition(position)
  }

  private fun updateOperations() {
    val allOperations = mutableListOf<AccountOperation>()
    allOperations.addAll(incomes)
    allOperations.addAll(expenses)
    allOperations.addAll(transfers)
    operations = allOperations.sortedBy { it.date() }
  }

  private fun updateCategories() {
    val allCategories = mutableListOf<OperationCategory>()
    allCategories.addAll(incomeCategories)
    allCategories.addAll(expenseCategories)
    operationCategories = allCategories.sortedBy { it.name() }

    val newOperationCategoryNames = operationCategories.map { it.name() }
    val newOperationCategoryPosition = operationCategories.indexOf(operationCategory)

    if (newOperationCategoryNames != operationCategorySpinnerModel.value?.modelsNames
      || newOperationCategoryPosition != operationCategorySpinnerModel.value?.selectedPosition
    ) {
      operationCategorySpinnerModel.value = SpinnerModel(
        newOperationCategoryNames,
        newOperationCategoryPosition
      )
    }

    accountCategories = accountCategories.sortedBy { it.name }

    val newAccountCategoryNames = accountCategories.map { it.name }
    val newAccountCategoryPosition = accountCategories.indexOf(accountCategory)

    if (newAccountCategoryNames != accountCategorySpinnerModel.value?.modelsNames
      || newAccountCategoryPosition != accountCategorySpinnerModel.value?.selectedPosition
    ) {
      accountCategorySpinnerModel.value = SpinnerModel(
        newAccountCategoryNames,
        newAccountCategoryPosition
      )
    }
  }

  private fun updateHistoryModels() {
    try {
      val dateFiltered = operations.filter {
        it.date().isDateInRange(selectedDate, selectedSelectionType)
      }

      val categoryFiltered = dateFiltered.filter {
        isOperationRefersSelectedCategories(it)
      }

      operationHistoryModels.value = operations
        .asSequence()
        .filter {
          it.date().isDateInRange(selectedDate, selectedSelectionType)
        }
        .filter {
          isOperationRefersSelectedCategories(it)
        }
        .sortedByDescending {
          it.date()
        }
        .mapNotNull {
          convertOperationToHistoryModel(it)
        }
        .toList()
    } catch (exception: NoSuchElementException) {
      // this catch is required for case, when not all data loaded
    }
  }

  private fun isOperationRefersSelectedCategories(operation: AccountOperation): Boolean {
    return when (operation) {
      is Operation -> {
        operation.belongsToOperationCategory(operationCategory, operationCategories)
            && operation.belongsToAccountCategory(accountCategory)
      }
      is Transfer -> {
        operationCategory == null &&
            operation.belongsToAccountCategory(accountCategory)
      }
      else -> false
    }
  }

  private fun convertOperationToHistoryModel(operation: AccountOperation): OperationHistoryModel? {
    return when (operation) {
      is Income -> {
        val accountCategory =
          accountCategories.find(operation.accountCategoryGuid) ?: return null
        OperationHistoryModel.IncomeHistoryModel(
          operation,
          incomeCategories.find(operation.incomeCategoryGuid) as IncomeCategory,
          accountCategory
        )
      }
      is Expense -> {
        val accountCategory =
          accountCategories.find(operation.accountCategoryGuid) ?: return null
        OperationHistoryModel.ExpenseHistoryModel(
          operation,
          expenseCategories.find(operation.expenseCategoryGuid) as ExpenseCategory,
          accountCategory
        )
      }
      is Transfer -> {
        val accountCategoryFrom =
          accountCategories.find(operation.fromAccountCategoryGuid) ?: return null
        val accountCategoryTo =
          accountCategories.find(operation.toAccountCategoryGuid) ?: return null
        OperationHistoryModel.TransferHistoryModel(
          operation,
          accountCategoryFrom,
          accountCategoryTo
        )
      }
      else -> null
    }
  }

  private fun updateCategoryEditableStatus() {
    isOperationCategoryEditable.value = operationCategory != null

    isAccountCategoryEditable.value = accountCategory != null
  }

  private fun updateOperationCategoryPosition(position: Int) {
    operationCategory = operationCategories.getOrNull(position)
    update()
  }

  private fun updateAccountCategoryPosition(position: Int) {
    accountCategory = accountCategories.getOrNull(position)
    update()
  }

  private fun update() {
    if (loadingParts.size != 0) {
      return
    }
    updateOperations()
    updateCategories()
    updateHistoryModels()
    updateCategoryEditableStatus()
  }

  data class State(
    val editOperationEvent: Event<OperationType>? = null,
    val editTransferEvent: SimpleEvent? = null,
    val editOperationCategoryEvent: Event<OperationType>? = null,
    val editAccountCategoryEvent: SimpleEvent? = null
  )
}