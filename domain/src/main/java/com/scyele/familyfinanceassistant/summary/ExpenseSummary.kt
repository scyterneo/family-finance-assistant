package com.scyele.familyfinanceassistant.summary

import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.Operation
import java.math.BigDecimal

data class ExpenseSummary(
  val expenseCategory: ExpenseCategory,
  var expenseSum: BigDecimal = BigDecimal.ZERO,
  var expenses: List<Expense> = listOf(),
  var childExpenseSummaries: List<ExpenseSummary> = listOf(),
  val levelOfDepth: Int = 0,
  var isCollapsed: Boolean = true
) : OperationSummary {
  override fun operationCategory() = expenseCategory

  override fun operationSum() = expenseSum

  override fun setOperationSum(value: BigDecimal) {
    expenseSum = value
  }

  override fun operations() = expenses

  override fun setOperations(value: List<Operation>) {
    expenses = value.filterIsInstance(Expense::class.java)
  }

  override fun childOperationSummaries() = childExpenseSummaries

  override fun setChildOperationSummaries(value: List<OperationSummary>) {
    childExpenseSummaries = value.filterIsInstance(ExpenseSummary::class.java)
  }

  override fun levelOfDepth() = levelOfDepth

  override fun isCollapsedSummary() = isCollapsed

  override fun setCollapsedSummary(value: Boolean) {
    isCollapsed = value
  }

  constructor(originalModel: ExpenseSummary) : this(
    originalModel.expenseCategory,
    originalModel.expenseSum,
    originalModel.expenses,
    originalModel.childExpenseSummaries,
    originalModel.levelOfDepth,
    originalModel.isCollapsed
  )

  override fun calculateSum() {
    expenseSum = expenses
      .map { BigDecimal(it.sum) }
      .fold(BigDecimal.ZERO, BigDecimal::add)
  }
}