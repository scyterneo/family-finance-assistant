package com.scyele.familyfinanceassistant.entity

import com.google.firebase.database.Exclude
import com.scyele.familyfinanceassistant.utils.Guid


data class ExpenseCategory(
  var name: String,
  var isParent: Boolean = false,
  var parentGuid: String? = null,
  val guid: String = Guid.new()
) : OperationCategory {

  // Empty private constructor for DataSnapshot.getValue method
  private constructor() : this("")

  override fun name() = name

  @Exclude
  override fun isParentCategory() = isParent

  override fun parentGuid() = parentGuid

  override fun guid() = guid

  @Exclude
  override fun isFirstLevelParent(): Boolean = isParent && parentGuid == null
}

