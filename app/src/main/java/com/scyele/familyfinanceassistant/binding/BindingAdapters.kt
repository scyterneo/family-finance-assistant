package com.scyele.familyfinanceassistant.binding

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.scyele.familyfinanceassistant.R

import com.scyele.lib.extension.getParentActivity


//TODO 3 mutable visibility adapters should be renamed to match inner logic
@BindingAdapter("mutableGone")
fun setMutableGone(view: View, visibility: MutableLiveData<Boolean>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && visibility != null) {
    visibility.observe(
      parentActivity,
      Observer { value -> view.visibility = if (value) View.VISIBLE else View.GONE })
  }
}

@BindingAdapter("mutableInvisibility")
fun setMutableInvisibility(view: View, visibility: MutableLiveData<Boolean>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && visibility != null) {
    visibility.observe(
      parentActivity,
      Observer { value -> view.visibility = if (value) View.VISIBLE else View.INVISIBLE })
  }
}

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Boolean>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && visibility != null) {
    visibility.observe(
      parentActivity,
      Observer { value -> view.visibility = if (value) View.GONE else View.VISIBLE })
  }
}

@BindingAdapter("mutableEnabling")
fun setMutableEnabling(view: View, enabling: MutableLiveData<Boolean>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && enabling != null) {
    enabling.observe(
      parentActivity,
      Observer { value -> view.isEnabled = value })
  }
}

@BindingAdapter("mutableBackground")
fun setMutableBackground(view: View, parent: MutableLiveData<Boolean>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && parent != null) {
    parent.observe(parentActivity, Observer { value ->
      view.setBackgroundResource(
        if (value) R.drawable.bg_parent_list_item_states
        else R.drawable.bg_list_item_states
      )
    }
    )
  }
}

@BindingAdapter("mutableOpenedImage")
fun setMutableOpenedImage(view: ImageView, parent: MutableLiveData<Boolean>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && parent != null) {
    parent.observe(parentActivity, Observer { value ->
      view.setImageResource(
        if (value) R.drawable.plus
        else R.drawable.minus
      )
    }
    )
  }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && text != null) {
    text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
  }
}


@BindingAdapter("mutableHidingText")
fun setMutableHidingText(view: TextView, text: MutableLiveData<String?>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && text != null) {
    text.observe(parentActivity, Observer { value ->
      if (value == null || value.isEmpty()) {
        view.visibility = View.GONE
      } else {
        view.visibility = View.VISIBLE
        view.text = value
      }
    })
  }
}

@BindingAdapter("mutableIntegerText")
fun setMutableIntegerText(view: TextView, text: MutableLiveData<Int>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && text != null) {
    text.observe(parentActivity, Observer { value -> view.text = value.toString() })
  }
}

@BindingAdapter("mutableProgress")
fun setMutableProgress(view: ProgressBar, progress: MutableLiveData<Int?>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && progress != null) {
    progress.observe(parentActivity, Observer { value ->
      if (value == null) {
        view.visibility = View.GONE
      } else {
        view.visibility = View.VISIBLE
        view.progress = value
      }
    })
  }
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
  view.adapter = adapter
}

@BindingAdapter("mutableSize")
fun setMutableSize(view: View, depth: MutableLiveData<Int>?) {
  val parentActivity: AppCompatActivity? = view.getParentActivity()
  if (parentActivity != null && depth != null) {
    depth.observe(parentActivity, Observer { value ->
      view.layoutParams = ConstraintLayout.LayoutParams(
        1 + (value ?: 0) * 40,
        ConstraintLayout.LayoutParams.WRAP_CONTENT
      )
    })
  }
}