package com.scyele.familyfinanceassistant.ui.settings.location

import android.content.Intent
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.ShareLocationFragmentBinding
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import kotlinx.android.synthetic.main.share_location_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi




class ShareLocationFragment : BaseDialogFragment<
    ShareLocationFragmentViewModel,
    ShareLocationFragmentBinding,
    ShareLocationFragmentViewModel.State,
    SettingsFragmentViewModelFactory>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getLayoutId() = R.layout.share_location_fragment

  override fun bindToViewModel() {
    binding.viewModel = viewModel
  }

  override fun handleEvents(state: ShareLocationFragmentViewModel.State) {
    state.locationPassword.getContent()?.let {
      location_password.setText(it)
    }
    state.editPasswordState.getContent()?.let {
      password_setup_button.setImageResource(
        if (it == ShareLocationFragmentViewModel.EditPasswordState.SET) android.R.drawable.ic_menu_edit
        else R.drawable.share_location_password_ok_image
      )
    }
    state.onOkButtonClicked?.getContent()?.let {
      dismiss()
    }
    state.onShareClicked?.getContent()?.let {
      val sharingIntent = Intent(Intent.ACTION_SEND)
      sharingIntent.type = "text/plain"
      val shareBody = """Use this
        |login: ${it.id}
        |password: ${it.password}
        |to access shared location and have common budget""".trimMargin()
      sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Family Finance Assistant")
      sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
      startActivity(Intent.createChooser(sharingIntent, "Share via"))
    }
  }

  override fun getModelClass(): Class<ShareLocationFragmentViewModel> {
    return ShareLocationFragmentViewModel::class.java
  }

  override fun tag() = "ShareLocationFragment"
}