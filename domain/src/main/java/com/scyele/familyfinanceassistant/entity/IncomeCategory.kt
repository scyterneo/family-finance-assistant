package com.scyele.familyfinanceassistant.entity

import com.google.firebase.database.Exclude
import com.scyele.familyfinanceassistant.utils.Guid


data class IncomeCategory(
  var name: String,
  var isParent: Boolean = false,
  val parentGuid: String? = null,
  val guid: String = Guid.new()
) : OperationCategory {

  // Empty private constructor for DataSnapshot.getValue method
  private constructor() : this("")

  override fun name() = name

  @Exclude
  override fun isParentCategory() = isParent

  override fun parentGuid() = parentGuid

  override fun guid() = guid

  @Exclude
  override fun isFirstLevelParent(): Boolean = isParent && parentGuid == null
}

fun List<IncomeCategory>.find(guid: String): IncomeCategory {
  return this.first { it.guid == guid }
}