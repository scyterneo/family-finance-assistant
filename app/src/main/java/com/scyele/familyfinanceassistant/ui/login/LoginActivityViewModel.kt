package com.scyele.familyfinanceassistant.ui.login

import android.content.Intent
import android.content.res.Resources
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.FirebaseDatabase
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.db.Tables
import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.utils.extension.*
import com.scyele.lib.extension.string
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Inject



class LoginActivityViewModel : BaseScreenVM<LoginActivityViewModel.State>() {

  @Inject
  lateinit var resources: Resources

  @Inject
  lateinit var auth: FirebaseAuth

  @Inject
  lateinit var database: FirebaseDatabase

  private val isProgressVisible = MutableLiveData<Boolean>()
  private val isSignedIn = MutableLiveData<Boolean>()

  private lateinit var googleSignInClient: GoogleSignInClient
  private val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
    .requestIdToken("79942672914-rht5ooln50j90507h70u1v91ijes7sr1.apps.googleusercontent.com")
    .requestEmail()
    .build()

  init {
    AssistantApplication.instance.firebaseComponent.inject(this)
    isProgressVisible.value = true
    isSignedIn.value = false
    setUser(auth.currentUser)
  }

  override fun initState() = State()

  override fun tag() = "LoginActivityViewModel"

  fun onSignInClicked() {
    isProgressVisible.value = true
    state.value = state.value.copy(googleSignInClicked = Event(googleSignInOptions))
  }

  fun onJoinClicked() {
    state.value = state.value.copy(joinLocation = SimpleEvent())
  }

  fun onCreateClicked() {
    createNewLocation()
    state.value = state.value.copy(location = SimpleEvent())
  }

  fun isSignedIn() = isSignedIn

  fun isProgressVisible() = isProgressVisible

  fun setGoogleSingInClient(client: GoogleSignInClient) {
    googleSignInClient = client
    state.value = state.value.copy(clientReady = Event(googleSignInClient.signInIntent))
  }

  fun onSignInRequestResult(data: Intent?) {
    val task = GoogleSignIn.getSignedInAccountFromIntent(data)
    try {
      // Google Sign In was successful, authenticate with Firebase
      val account = task.getResult(ApiException::class.java)
      Log.d(tag(), account.toString())
      firebaseAuthWithGoogle(account!!)
    } catch (apiException: ApiException) {
      // Google Sign In failed
      // TODO update UI, show toast?
      Log.w(tag(), "Google sign in failed", apiException)
      setUser(null)
    }
  }

  fun onFirebaseSignInResult(task: Task<AuthResult>) {
    if (task.isSuccessful) {
      // Sign in success, update UI with the signed-in user's information
      Log.d(tag(), "signInWithCredential:success")
      setUser(auth.currentUser)
    } else {
      // If sign in fails, display a message to the user.
      Log.w(tag(), "signInWithCredential:failure", task.exception)
      setUser(null)
    }
  }

  private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
    Log.d(tag(), "firebaseAuthWithGoogle:" + acct.id!!)
    isProgressVisible.value = true

    val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
    state.value = state.value.copy(
      firebaseSignInTaskReady = Event(auth.signInWithCredential(credential))
    )
  }

  private fun setUser(currentUser: FirebaseUser?) {
    Log.d(tag(), "setUser: ${currentUser?.toString()}")
    if (currentUser != null) {
      Log.d(tag(), "user ${currentUser.uid}")
      state.value = state.value.copy(currentUser = Event(currentUser))
      AssistantApplication.instance.locationId = currentUser.uid
      checkSharingLocation()
    } else {
      isProgressVisible.value = false
      isSignedIn.value = false
    }
  }

  private fun checkSharingLocation() {
    database.getLocation(
      {
        Log.d(tag(), "checkSharingLocation: LOCATION $it")
        if (it.sharingLocation != null) {
          AssistantApplication.instance.locationId = it.sharingLocation!!
          isProgressVisible.value = false
          state.value = state.value.copy(location = SimpleEvent())
        } else {
          checkExistingLocation()
        }
      },
      {
        Log.d(tag(), "checkSharingLocation error: $it")
        checkExistingLocation()
      }
    )
  }

  private fun checkExistingLocation() {
    database.getList(
      Tables.ACCOUNT_CATEGORY_TABLE_NAME,
      AccountCategory::class.java
    ) {
      if (it.isEmpty()) {
        isSignedIn.value = true
        isProgressVisible.value = false
      } else {
        isProgressVisible.value = false
        state.value = state.value.copy(location = SimpleEvent())
      }
    }
  }

  private fun createNewLocation() {
    database.setAccountCategory(AccountCategory(string(R.string.cash)))
    val parentIncomeCategory = IncomeCategory(string(R.string.main_income), true)
    database.setIncomeCategory(parentIncomeCategory)
    database.setIncomeCategory(
      IncomeCategory(
        string(R.string.work),
        false,
        parentIncomeCategory.guid
      )
    )
    database.setIncomeCategory(
      IncomeCategory(
        string(R.string.business),
        false,
        parentIncomeCategory.guid
      )
    )
    val parentExpenseCategory = ExpenseCategory(string(R.string.food), true)
    database.setExpenseCategory(parentExpenseCategory)
    database.setExpenseCategory(
      ExpenseCategory(
        string(R.string.vegetables),
        false,
        parentExpenseCategory.guid
      )
    )
  }

  private fun string(id: Int): String =
    resources.string(id)

  data class State(
    val currentUser: Event<FirebaseUser>? = null,
    val googleSignInClicked: Event<GoogleSignInOptions>? = null,
    val clientReady: Event<Intent>? = null,
    val firebaseSignInTaskReady: Event<Task<AuthResult>>? = null,
    val joinLocation: SimpleEvent? = null,
    val location: SimpleEvent? = null
  )
}