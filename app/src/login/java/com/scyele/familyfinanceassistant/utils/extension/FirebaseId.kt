package com.scyele.familyfinanceassistant.utils.extension

import com.google.firebase.database.FirebaseDatabase
import com.scyele.familyfinanceassistant.AssistantApplication


fun FirebaseDatabase.getLocationId() = AssistantApplication.instance.locationId
