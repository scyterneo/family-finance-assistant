package com.scyele.familyfinanceassistant.di.components

//import com.scyele.familyfinanceassistant.di.modules.MainActivityModule
//import com.scyele.familyfinanceassistant.ui.main.MainActivity
//import dagger.Component
//import javax.inject.Singleton
//

//@Singleton
//@Component(
//  modules = [
//    (MainActivityModule::class)
//  ]
//)
//interface MainActivityComponent {
//
//  fun inject(mainActivity: MainActivity)
//
//  @Component.Builder
//  interface Builder {
//    fun build(): MainActivityComponent
//
//    fun mainActivityModule(mainActivityModule: MainActivityModule): Builder
//  }
//}