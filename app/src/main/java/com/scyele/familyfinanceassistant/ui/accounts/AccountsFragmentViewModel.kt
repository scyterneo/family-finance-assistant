package com.scyele.familyfinanceassistant.ui.accounts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.scyele.familyfinanceassistant.entity.*
import com.scyele.familyfinanceassistant.repository.AccountCategorySetupRepository
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.OperationCategorySetupRepository
import com.scyele.familyfinanceassistant.summary.AccountSummary
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferRepository
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.lib.coroutines.background
import com.scyele.lib.extension.DateSelectorSelectionType
import com.scyele.lib.extension.isDateInRange
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*



class AccountsFragmentViewModel(
  private val mainRepository: MainRepository,
  private val transferRepository: TransferRepository,
  private val accountCategorySetupRepository: AccountCategorySetupRepository,
  private val operationCategorySetupRepository: OperationCategorySetupRepository
) : BaseScreenVM<AccountsFragmentViewModel.State>() {
  override fun initState() = State()

  private var accountCategories: List<AccountCategory> = listOf()
  private var incomes: List<Income> = listOf()
  private var expenses: List<Expense> = listOf()
  private var transfers: List<Transfer> = listOf()

  private val startDateString = MutableLiveData<String>("Period Start Date")
  private val endDateString = MutableLiveData<String>("Period End Date")

  private val totalSum = MutableLiveData<String>()

  private val accountSummaries: MutableLiveData<List<AccountSummary>> =
    MutableLiveData(mutableListOf())

  private var selectedDate: Date = Date()
  private var selectedSelectionType: DateSelectorSelectionType = DateSelectorSelectionType.ALL


  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(mainRepository.accountCategories) {
      accountCategories = it
      updateData()
    }
    mediatorState.addSource(mainRepository.incomes) {
      incomes = it
      updateData()
    }
    mediatorState.addSource(mainRepository.expenses) {
      expenses = it
      updateData()
    }
    mediatorState.addSource(mainRepository.transfers) {
      transfers = it
      updateData()
    }
    return mediatorState
  }

  fun onAccountClicked(guid: String) {
    val account = accountSummaries.value?.firstOrNull { it.accountCategory.guid == guid }
      ?: return
    transferRepository.transfer.value =
      Transfer("0", account.accountCategory.guid, account.accountCategory.guid)
    state.value = state.value.copy(startTransferEvent = SimpleEvent())
  }

  fun onAccountLongClicked(guid: String) {
    accountCategorySetupRepository.accountCategory.value =
      accountCategories.find(guid)
    operationCategorySetupRepository.reinit()
    state.value = state.value.copy(showHistoryEvent = SimpleEvent())
  }

  fun getAccountSummaries() = accountSummaries

  fun getTotalSum() = totalSum

  fun onAddAccountCategoryClicked() {
    editAccountCategory(null)
  }

  fun getStartDate() = startDateString

  fun getEndDate() = endDateString

  private fun editAccountCategory(guid: String?) {
    accountCategorySetupRepository.accountCategory.value =
      accountCategories.firstOrNull { it.guid == guid }
    state.value = state.value.copy(editAccountCategoryEvent = SimpleEvent())
  }

  private fun calculateSum(accountSummaries: List<AccountSummary>): BigDecimal {
    var sum = BigDecimal.ZERO
    for (account in accountSummaries) {
      sum += account.calculateSum()
    }
    return sum
  }

  private fun onAccountSummariesCreated(accounts: List<AccountSummary>) {
    accountSummaries.value = accounts
  }

  /**
   * Creates list of accounts and notifies UI about it
   */
  private fun updateData() {
    viewModelScope.launch {
      val accountSummaries = background {
        createAccounts(
          accountCategories,
          incomes.filter { it.date.isDateInRange(selectedDate, selectedSelectionType) },
          expenses.filter { it.date.isDateInRange(selectedDate, selectedSelectionType) },
          transfers.filter { it.date.isDateInRange(selectedDate, selectedSelectionType) })
      }
      totalSum.value = background { calculateSum(accountSummaries).toString() }
      onAccountSummariesCreated(accountSummaries)
    }
  }

  /**
   * Method create AccountSummary list
   *
   * @param accountCategories list of Account categories
   * @param incomes list of all Incomes to all categories
   * @param expenses list of all Expenses to all categories
   * @param transfers list of all Transfers between categories
   */
  private fun createAccounts(
    accountCategories: List<AccountCategory>,
    incomes: List<Income>,
    expenses: List<Expense>,
    transfers: List<Transfer>
  ): MutableList<AccountSummary> {
    this.accountCategories = accountCategories
    val accounts = mutableListOf<AccountSummary>()
    for (accountCategory in accountCategories) {
      val accountIncomes = incomes.filter { income ->
        income.accountCategoryGuid == accountCategory.guid
      }
      val accountExpenses = expenses.filter { expense ->
        expense.accountCategoryGuid == accountCategory.guid
      }
      val accountTransfers = transfers.filter { transfer ->
        transfer.fromAccountCategoryGuid == accountCategory.guid
            || transfer.toAccountCategoryGuid == accountCategory.guid
      }
      val account = AccountSummary(
        accountCategory,
        accountIncomes,
        accountExpenses,
        accountTransfers
      )
      accounts.add(account)
    }

    return accounts
  }

  fun onDateSelectionChanged(date: Date, selectionType: DateSelectorSelectionType) {
    selectedDate = date
    selectedSelectionType = selectionType
    updateData()
  }

  data class State(
    val startTransferEvent: SimpleEvent? = null,
    val editAccountCategoryEvent: SimpleEvent? = null,
    val selectStartDate: Event<Date>? = null,
    val selectEndDate: Event<Date>? = null,
    val showHistoryEvent: SimpleEvent? = null
  )
}