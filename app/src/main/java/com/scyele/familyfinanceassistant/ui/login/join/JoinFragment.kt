package com.scyele.familyfinanceassistant.ui.login.join

import android.content.Intent
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.JoinFragmentBinding
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.main.MainActivity
import com.scyele.familyfinanceassistant.utils.helper.ToastHelper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class JoinFragment : BaseDialogFragment<
    JoinFragmentViewModel,
    JoinFragmentBinding,
    JoinFragmentViewModel.State,
    FragmentViewModelFactory>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getLayoutId() = R.layout.join_fragment;

  override fun bindToViewModel() {
    binding.viewModel = viewModel
  }

  override fun handleEvents(state: JoinFragmentViewModel.State) {
    state.joinedToLocation?.getContent()?.let {
      val intent = Intent(activity, MainActivity::class.java)
      intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
      startActivity(intent)
    }
    state.joinLocationError?.getContent()?.let {
      ToastHelper.showToast(requireContext(), it)
    }
    state.canceled?.getContent()?.let {
      dismiss()
    }
  }

  override fun getModelClass() = JoinFragmentViewModel::class.java

  override fun tag() = "JoinFragment"
}
