package com.scyele.lib.extension.livedata

import androidx.lifecycle.MediatorLiveData

class NonNullMediatorLiveData<T> : MediatorLiveData<T>()