package com.scyele.familyfinanceassistant.entity


data class User(val uid: String, val mail: String?) {

  // Empty private constructor for DataSnapshot.getValue method
  private constructor() : this("", "")
}