package com.scyele.familyfinanceassistant.ui.operation.setup

enum class OperationCategoryDeleteError(val message: String) {
  CHILD_CATEGORIES_EXIST("Unable to delete category with child sub-categories"),
  OPERATIONS_EXIST("Category should not have operations before deleting")
}