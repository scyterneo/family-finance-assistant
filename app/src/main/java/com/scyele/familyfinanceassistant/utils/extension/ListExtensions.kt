package com.scyele.familyfinanceassistant.utils.extension

import androidx.lifecycle.LiveData


fun <T> List<T>.getElementByPosition(position: Int): T? {
  val size = this.size
  return if (position >= 0 && size > 0 && position < size)
    this[position]
  else
    null
}

fun <T> getElementByPosition(position: Int, list: List<T>?) : T? {
  return list?.getElementByPosition(position)
}

fun <T> getElementByPosition(position: Int, liveData: LiveData<List<T>>?) : T? {
  return liveData?.value?.getElementByPosition(position)
}