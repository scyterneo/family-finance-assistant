package com.scyele.familyfinanceassistant.ui.operation.edit

import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.repository.ExpenseEditRepository
import com.scyele.familyfinanceassistant.repository.ProgressRepository
import com.scyele.familyfinanceassistant.ui.DefaultMainRepositoryTest
import com.scyele.familyfinanceassistant.ui.expences.edit.ExpenseEditFragmentViewModel
import com.scyele.lib.SharedPreferencesHelper
import io.mockk.mockk
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test


internal class OperationEditFragmentViewModelTest : DefaultMainRepositoryTest() {
  lateinit var testingVM: OperationEditFragmentViewModel<Expense, ExpenseCategory>

  private var state: OperationEditFragmentViewModel.State? = null

  private val expenseEditRepository: ExpenseEditRepository = ExpenseEditRepository()
  private val progressRepository = ProgressRepository(SharedPreferencesHelper(mockk()))

  init {
    testingVM = ExpenseEditFragmentViewModel(
      getMain(),
      expenseEditRepository,
      progressRepository
    )

    testingVM.state().observeForever { state = it }
  }

  @Nested
  inner class BaseScenario {
    @Test
    fun initState() {
      assertEquals(3, testingVM.operationCategories.value?.size)
      assertEquals(3, testingVM.accountCategories.value?.size)
      assertEquals(null, testingVM.operationCategoryPosition.value)
      assertEquals("", state!!.sum.peekContent())
      assertEquals("", state!!.commentary.peekContent())
    }
  }

  @Nested
  inner class ExpensePresetScenario {

    init {
      expenseEditRepository.operation.value =
        Expense("10", "accountCategory1", "expenseCategory1", comment = "someComment")
      testingVM = ExpenseEditFragmentViewModel(
        getMain(),
        expenseEditRepository,
        progressRepository
      )

      testingVM.state().observeForever { state = it }
    }

    @Test
    fun initState() {
      assertEquals("10", state!!.sum.peekContent())
      assertEquals("someComment", state!!.commentary.peekContent())
    }
  }
}