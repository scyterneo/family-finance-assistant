package com.scyele.familyfinanceassistant.ui.statistics

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent

class StatisticsFragmentViewModel :
  BaseScreenVM<StatisticsFragmentViewModel.State>() {

  private val totalSum = MutableLiveData<String>()

  override fun initState() = State()

  fun getTotalSum() = totalSum

  data class State(
    val event: SimpleEvent? = null
  )
}