package com.scyele.familyfinanceassistant.extension

import java.math.BigDecimal


fun BigDecimal.isPositive() = this > BigDecimal.ZERO

fun BigDecimal.isNegative() = this < BigDecimal.ZERO

fun BigDecimal.isZero() = signum() == 0