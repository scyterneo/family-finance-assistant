package com.scyele.familyfinanceassistant.summary

import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import java.math.BigDecimal

interface OperationSummary {

  fun operationCategory(): OperationCategory

  fun operationSum(): BigDecimal

  fun setOperationSum(value: BigDecimal)

  fun operations(): List<Operation>

  fun setOperations(value: List<Operation>)

  fun childOperationSummaries(): List<OperationSummary>

  fun setChildOperationSummaries(value: List<OperationSummary>)

  fun levelOfDepth(): Int

  fun isCollapsedSummary(): Boolean

  fun setCollapsedSummary(value: Boolean)

  fun calculateSum()
}

fun createNewSummary(
  operationCategory: OperationCategory,
  levelOfDepth: Int = 0
): OperationSummary? {
  if (operationCategory is IncomeCategory) {
    return IncomeSummary(operationCategory, levelOfDepth = levelOfDepth)
  } else if (operationCategory is ExpenseCategory) {
    return ExpenseSummary(operationCategory, levelOfDepth = levelOfDepth)
  }
  return null
}

fun createNewSummary(
  operationSummary: OperationSummary
): OperationSummary? {
  if (operationSummary is IncomeSummary) {
    return IncomeSummary(operationSummary)
  } else if (operationSummary is ExpenseSummary) {
    return ExpenseSummary(operationSummary)
  }
  return null
}