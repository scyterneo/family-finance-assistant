package com.scyele.familyfinanceassistant.ui.operation.history

import com.scyele.familyfinanceassistant.entity.*
import java.util.*

sealed class OperationHistoryModel(open val guid: String, open val sum: String) {

  data class IncomeHistoryModel(
    override val guid: String,
    override val sum: String,
    val incomeCategoryName: String,
    val accountCategoryName: String,
    val date: Date,
    val comment: String?
  ) : OperationHistoryModel(guid, sum) {
    constructor(
      income: Income,
      incomeCategory: IncomeCategory,
      accountCategory: AccountCategory
    ) : this(
      income.guid(),
      income.sum(),
      incomeCategory.name(),
      accountCategory.name,
      income.date(),
      income.comment()
    )
  }

  data class ExpenseHistoryModel(
    override val guid: String,
    override val sum: String,
    val expenseCategoryName: String,
    val accountCategoryName: String,
    val date: Date,
    val comment: String?
  ) : OperationHistoryModel(guid, sum) {
    constructor(
      expense: Expense,
      expenseCategory: ExpenseCategory,
      accountCategory: AccountCategory
    ) : this(
      expense.guid(),
      expense.sum(),
      expenseCategory.name(),
      accountCategory.name,
      expense.date(),
      expense.comment()
    )
  }

  data class TransferHistoryModel(
    override val guid: String,
    override val sum: String,
    val accountCategoryFromName: String,
    val accountCategoryToName: String,
    val date: Date,
    val comment: String?
  ) : OperationHistoryModel(guid, sum) {
    constructor(
      transfer: Transfer,
      accountCategoryFrom: AccountCategory,
      accountCategoryTo: AccountCategory
    ) : this(
      transfer.guid,
      transfer.sum,
      accountCategoryFrom.name,
      accountCategoryTo.name,
      transfer.date(),
      transfer.comment
    )
  }
}