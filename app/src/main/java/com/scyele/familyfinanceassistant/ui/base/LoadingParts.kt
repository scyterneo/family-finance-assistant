package com.scyele.familyfinanceassistant.ui.base

@Suppress("ClassName")
sealed class LoadingParts(val loadingPartName: String) {

  object INCOMES : LoadingParts("Incomes")
  object EXPENSES : LoadingParts("Expenses")
  object TRANSFERS : LoadingParts("Transfers")
  object OPERATIONS : LoadingParts("Operations")

  object INCOME_CATEGORIES : LoadingParts("Income categories")
  object EXPENSE_CATEGORIES : LoadingParts("Expense categories")
  object ACCOUNT_CATEGORIES : LoadingParts("Account categories")
  object OPERATION_CATEGORIES : LoadingParts("Operation categories")

  object INITIAL_ACCOUNT_CATEGORY : LoadingParts("Initial account category")

}