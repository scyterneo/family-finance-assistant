package com.scyele.familyfinanceassistant.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.ActivityLoginBinding
import com.scyele.familyfinanceassistant.ui.login.join.JoinFragment
import com.scyele.familyfinanceassistant.ui.main.MainActivity
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.lib.extension.livedata.observeNonNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Inject



class LoginActivity : AppCompatActivity() {

  private lateinit var binding: ActivityLoginBinding
  private lateinit var viewModel: LoginActivityViewModel

  @Inject
  lateinit var factory: LoginActivityViewModelFactory


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    injectDependency()
    binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
    viewModel = ViewModelProvider(this, factory).get(LoginActivityViewModel::class.java)
    binding.viewModel = viewModel

    viewModel.state().observeNonNull(this) {
      handleEvents(it)
    }
    // Unable to set via xml binding, because
    // SignInButton implements OnClickListener by itself
    binding.signInButton.setOnClickListener { viewModel.onSignInClicked() }
  }

  override fun onDestroy() {
    super.onDestroy()
    AssistantApplication.instance.releaseLoginActivityComponent()
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (requestCode == SIGN_IN_REQUEST_NUMBER) {
      viewModel.onSignInRequestResult(data)
    }
  }

  fun handleEvents(state: LoginActivityViewModel.State) {
    state.googleSignInClicked?.getContent()?.let {
      viewModel.setGoogleSingInClient(GoogleSignIn.getClient(this, it))
    }
    state.clientReady?.getContent()?.let {
      startActivityForResult(it, SIGN_IN_REQUEST_NUMBER)
    }
    state.currentUser?.getContent()?.let {
      binding.signInButton.isEnabled = false
    }
    state.firebaseSignInTaskReady?.getContent()?.let { task ->
      task.addOnCompleteListener(this) { taskWithResult ->
        viewModel.onFirebaseSignInResult(taskWithResult)
      }
    }
    state.joinLocation?.getContent()?.let {
      DialogHelper.showDialogFragment(JoinFragment(), this)
    }
    state.location?.getContent()?.let {
      val intent = Intent(this, MainActivity::class.java)
      intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
      startActivity(intent)
      finish()
    }
  }

  private fun injectDependency() {
    AssistantApplication.instance.loginActivityComponent().inject(this)
  }

  companion object {
    private const val SIGN_IN_REQUEST_NUMBER = 5942
  }
}