package com.scyele.familyfinanceassistant.ui.statistics

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.StatisticsFragmentBinding
import com.scyele.familyfinanceassistant.ui.base.BaseFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class StatisticsFragment : BaseFragment<
    StatisticsFragmentViewModel,
    StatisticsFragmentBinding,
    StatisticsFragmentViewModel.State,
    FragmentViewModelFactory>() {
  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getLayoutId() = R.layout.statistics_fragment

  override fun bindingViewModeSetViewModel() {
    binding.viewModel = viewModel
  }

  override fun handleEvents(state: StatisticsFragmentViewModel.State) {
    state.event?.getContent()?.let {
      //TODO
    }
  }

  override fun getModelClass(): Class<StatisticsFragmentViewModel> {
    return StatisticsFragmentViewModel::class.java
  }

  override fun tag() = "StatisticsFragment"
}