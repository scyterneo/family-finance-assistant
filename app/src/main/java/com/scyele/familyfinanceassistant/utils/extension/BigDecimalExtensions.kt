package com.scyele.familyfinanceassistant.utils.extension

import java.math.BigDecimal

internal fun convert(text: CharSequence) =
  try {
    BigDecimal(text.toString())
  } catch (e: NumberFormatException) {
    BigDecimal.ZERO
  }
