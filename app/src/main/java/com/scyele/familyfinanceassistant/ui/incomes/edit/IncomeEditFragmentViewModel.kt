package com.scyele.familyfinanceassistant.ui.incomes.edit

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.repository.IncomeEditRepository
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.ProgressRepository
import com.scyele.familyfinanceassistant.ui.operation.edit.OperationEditFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*


class IncomeEditFragmentViewModel(
  mainRepository: MainRepository,
  incomeEditRepository: IncomeEditRepository,
  progressRepository: ProgressRepository
) : OperationEditFragmentViewModel<Income, IncomeCategory>(
  mainRepository,
  incomeEditRepository,
  progressRepository
) {

  override fun getAllOperationCategories(): MutableLiveData<List<IncomeCategory>> {
    return mainRepository.incomeCategories
  }

  override fun createNewOperation(
    guid: String,
    sum: String,
    accountCategoryGuid: String,
    actionCategoryGuid: String,
    date: Date,
    comment: String?
  ): Income {
    return Income(guid, sum, accountCategoryGuid, actionCategoryGuid, date, comment)
  }

  override fun getOperations(): MutableLiveData<List<Income>> {
    return mainRepository.incomes
  }
}