package com.scyele.familyfinanceassistant.repository

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.OperationCategory

class OperationCategorySetupRepository {
  var operationCategory = MutableLiveData<OperationCategory?>()

  fun reinit() {
    operationCategory.value = null
  }
}