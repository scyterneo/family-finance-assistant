package com.scyele.views.base

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.lifecycle.LifecycleOwner

abstract class MvvmLinearLayout<S : MvvmCustomViewState, M : MvvmCustomViewModel<S>>(
  context: Context,
  attributeSet: AttributeSet?
) : LinearLayout(context, attributeSet), MvvmCustomView<S, M> {
  override fun onAttachedToWindow() {
    super.onAttachedToWindow()
    val lifecycleOwner = context as? LifecycleOwner
    onLifecycleOwnerAttached(lifecycleOwner)
  }

  override fun onSaveInstanceState() =
    MvvmCustomViewStateWrapper(super.onSaveInstanceState(), viewModel.state)

  @Suppress("UNCHECKED_CAST")
  override fun onRestoreInstanceState(state: Parcelable?) {
    if (state is MvvmCustomViewStateWrapper) {
      viewModel.state = state.state as S?
      super.onRestoreInstanceState(state.superState)
    }
  }
}