package com.scyele.views.base

class LifecycleOwnerNotFoundException(message: String? = null): Throwable(message)