package com.scyele.familyfinanceassistant.entity

interface OperationCategory {
  fun name(): String

  fun isParentCategory(): Boolean

  fun parentGuid(): String?

  fun guid(): String

  fun isFirstLevelParent(): Boolean
}