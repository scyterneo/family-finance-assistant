package com.scyele.familyfinanceassistant.utils

import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.summary.IncomeSummary

object AggregatorOld {
//  fun createIncomeSummaries(
//    incomes: List<Income>,
//    incomeCategories: List<IncomeCategory>
//  ): List<IncomeSummary> {
//    val incomeSummaries: MutableList<IncomeSummary> = mutableListOf()
//    for (incomeCategory in incomeCategories.filter { it.isParent }.sortedBy { it.name }) {
//      incomeSummaries.add(
//        createParentIncomeSummary(incomeCategory, incomes, incomeCategories)
//      )
//      incomeSummaries.addAll(incomeCategories
//        .filter { it.parentGuid == incomeCategory.guid }
//        .sortedBy { it.name }
//        .map { createIncomeSummary(it, incomes) }
//      )
//    }
//    return incomeSummaries
//  }
//
//  private fun createIncomeSummary(incomeCategory: IncomeCategory, incomes: List<Income>) =
//    IncomeSummary(incomeCategory,
//      incomes.filter { it.incomeCategoryGuid == incomeCategory.guid } as MutableList<Income>)
//
//  private fun createParentIncomeSummary(
//    incomeCategory: IncomeCategory,
//    incomes: List<Income>,
//    incomeCategories: List<IncomeCategory>
//  ): IncomeSummary {
//    val childIncomeCategories = incomeCategories.filter { it.parentGuid == incomeCategory.guid }
//    val childIncomes: MutableList<Income> = mutableListOf()
//    for (childIncomeCategory in childIncomeCategories) {
//      childIncomes.addAll(incomes.filter { it.incomeCategoryGuid == childIncomeCategory.guid })
//    }
//    return IncomeSummary(incomeCategory, childIncomes)
//  }
}