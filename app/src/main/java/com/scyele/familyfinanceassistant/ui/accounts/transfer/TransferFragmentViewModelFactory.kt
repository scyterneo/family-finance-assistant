package com.scyele.familyfinanceassistant.ui.accounts.transfer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.scyele.familyfinanceassistant.repository.MainRepository


class TransferFragmentViewModelFactory(
  private val mainRepository: MainRepository,
  private val transferRepository: TransferRepository
) : ViewModelProvider.Factory {

  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(TransferFragmentViewModel::class.java)) {
      @Suppress("UNCHECKED_CAST")
      return TransferFragmentViewModel(mainRepository, transferRepository) as T
    }

    throw IllegalArgumentException("Unknown ViewModel class")
  }
}