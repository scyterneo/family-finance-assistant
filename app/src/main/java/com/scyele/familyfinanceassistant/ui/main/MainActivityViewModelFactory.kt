package com.scyele.familyfinanceassistant.ui.main


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.scyele.familyfinanceassistant.repository.AccountCategorySetupRepository
import com.scyele.familyfinanceassistant.repository.OperationCategorySetupRepository
import com.scyele.familyfinanceassistant.repository.ProgressRepository


class MainActivityViewModelFactory(
  private val operationCategorySetupRepository: OperationCategorySetupRepository,
  private val accountCategorySetupRepository: AccountCategorySetupRepository,
  private val progressRepository: ProgressRepository
) : ViewModelProvider.Factory {
  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
      @Suppress("UNCHECKED_CAST")
      return MainActivityViewModel(
        operationCategorySetupRepository,
        accountCategorySetupRepository,
        progressRepository
      ) as T
    }

    throw IllegalArgumentException("Unknown ViewModel class")
  }
}