package com.scyele.familyfinanceassistant.ui.history

import androidx.recyclerview.widget.DiffUtil
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel.*

class HistoryModelDiff : DiffUtil.ItemCallback<OperationHistoryModel>() {
  override fun areItemsTheSame(
    oldItem: OperationHistoryModel,
    newItem: OperationHistoryModel
  ): Boolean {
    return oldItem::class == newItem::class
        && oldItem.guid == newItem.guid
  }

  override fun areContentsTheSame(
    oldItem: OperationHistoryModel,
    newItem: OperationHistoryModel
  ): Boolean {
    when {
      oldItem is TransferHistoryModel && newItem is TransferHistoryModel ->
        return oldItem as TransferHistoryModel == newItem as TransferHistoryModel
      oldItem is IncomeHistoryModel && newItem is IncomeHistoryModel ->
        return oldItem as IncomeHistoryModel == newItem as IncomeHistoryModel
      oldItem is ExpenseHistoryModel && newItem is ExpenseHistoryModel ->
        return oldItem as ExpenseHistoryModel == newItem as ExpenseHistoryModel
    }
    return false
  }
}