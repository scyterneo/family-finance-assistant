package com.scyele.familyfinanceassistant.utils.extension

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

/**
 * Method subscribes and observes Observable at Schedulers.io (background thread)
 */
fun <T> Observable<T>.background(): Observable<T> {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(Schedulers.io())
}

/**
 * Method subscribes Observable at Schedulers.io (background thread of working)
 * and observes at MainThread
 */
fun <T> Observable<T>.main(): Observable<T> {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
}


fun <T> Observable<T>.background(onNext: Consumer<in T>): Disposable {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(Schedulers.io())
    .subscribe(onNext, Consumer {})
}

fun <T> Observable<T>.background(
  onNext: Consumer<in T>,
  onError: Consumer<in Throwable>
): Disposable {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(Schedulers.io())
    .subscribe(onNext, onError)
}

fun <T> Observable<T>.main(onNext: Consumer<in T>, onError: Consumer<in Throwable>): Disposable {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
    .subscribe(onNext, onError)
}

fun <T> Observable<T>.main(onNext: Consumer<in T>): Disposable {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
    .subscribe(onNext, Consumer {})
}

fun <T> Observable<T>.main(onNextAction: (T) -> Unit): Disposable {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
    .subscribe({ onNextAction.invoke(it) }, {})
}

/**
 * Method subscribes and observes Observable at Schedulers.io (background thread)
 */
fun Completable.background(): Completable {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(Schedulers.io())
}
