package com.scyele.lib.extension

import android.content.res.Resources
import androidx.annotation.StringRes

fun Resources.string(@StringRes id: Int): String = try {
  getString(id)
} catch (e: Exception) {
  ""
}