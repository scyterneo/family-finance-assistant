package com.scyele.familyfinanceassistant.ui.expences.setup

import androidx.lifecycle.LiveData
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.MoveToRepository
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.ui.move.MoveToCategoryFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi


class MoveToExpenseCategoryFragmentViewModel(
  mainRepository: MainRepository,
  moveToRepository: MoveToRepository
) : MoveToCategoryFragmentViewModel<ExpenseCategory>(mainRepository, moveToRepository) {

  init {
    currentCategory = moveToRepository.operationCategory as ExpenseCategory?
  }

  override fun moveToModel() {
    val expensesToMove = mainRepository.expenses.value?.filter {
      it.expenseCategoryGuid == currentCategory?.guid
    } ?: listOf()
    for (expense in expensesToMove) {
      expense.expenseCategoryGuid = targetCategory!!.guid
      mainRepository.setExpense(expense)
    }
    moveToRepository.moveCompleted.value = true
    state.value = state.value.copy(moveCompleted = SimpleEvent())
  }

  override fun getAllCategoriesSource(): LiveData<List<ExpenseCategory>> {
    return mainRepository.expenseCategories
  }

  override fun filterElement(category: ExpenseCategory): Boolean {
    return !category.isParent && category.guid != currentCategory?.guid
  }

  override fun getCurrentCategoryName(): String {
    return currentCategory?.name ?: ""
  }
}