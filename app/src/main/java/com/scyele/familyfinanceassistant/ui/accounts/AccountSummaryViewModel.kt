package com.scyele.familyfinanceassistant.ui.accounts

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.summary.AccountSummary
import com.scyele.familyfinanceassistant.ui.base.BaseVM
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class AccountSummaryViewModel : BaseVM() {

  private val accountName = MutableLiveData<String>()
  private val accountSum = MutableLiveData<String>()

  fun bind(accountSummary: AccountSummary) {
    accountName.value = accountSummary.accountCategory.name
    accountSum.value = accountSummary.calculateSum().toString()
  }

  fun getAccountName() = accountName
  fun getAccountSum() = accountSum
}