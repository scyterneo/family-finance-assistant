package com.scyele.lib.coroutines

import kotlinx.coroutines.*

fun start(block: suspend CoroutineScope.() -> Unit) {
  GlobalScope.launch(Dispatchers.Main) {
    block(this)
  }
}

suspend fun <T> background(block: suspend CoroutineScope.() -> T): T {
  return withContext(Dispatchers.IO) {
    block(this)
  }
}

suspend fun <T> main(block: suspend CoroutineScope.() -> T): T {
  return withContext(Dispatchers.Main) {
    block(this)
  }
}