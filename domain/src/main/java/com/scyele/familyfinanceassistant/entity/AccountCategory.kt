package com.scyele.familyfinanceassistant.entity

import com.scyele.familyfinanceassistant.utils.Guid


data class AccountCategory(
  var name: String,
  val guid: String = Guid.new(),
  val debtAccount: Boolean = false
) {

  // Empty private constructor for DataSnapshot.getValue method
  private constructor() : this("")
}

fun List<AccountCategory>.find(guid: String): AccountCategory? {
  return this.firstOrNull { it.guid == guid }
}