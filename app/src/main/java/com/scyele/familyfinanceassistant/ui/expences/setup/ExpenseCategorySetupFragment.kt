package com.scyele.familyfinanceassistant.ui.expences.setup

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.summary.ExpenseSummary
import com.scyele.familyfinanceassistant.ui.operation.setup.OperationCategorySetupFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class ExpenseCategorySetupFragment :
  OperationCategorySetupFragment<Expense, ExpenseCategory, ExpenseSummary, ExpenseCategorySetupFragmentViewModel>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<ExpenseCategorySetupFragmentViewModel> {
    return ExpenseCategorySetupFragmentViewModel::class.java
  }
}