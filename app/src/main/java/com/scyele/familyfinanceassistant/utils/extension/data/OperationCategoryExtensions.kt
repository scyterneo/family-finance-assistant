package com.scyele.familyfinanceassistant.utils.extension.data

import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.OperationCategory


/**
 * @param operationCategories list of all operation categories
 *
 * @return list that contains category, it's children and all children of children categories
 */
fun OperationCategory.findAllRelatedCategories(operationCategories: List<OperationCategory>): List<OperationCategory> {
  val allRelatedCategories = mutableListOf<OperationCategory>()
  allRelatedCategories.add(this)
  if (isParentCategory()) {
    for (operationCategory in operationCategories) {
      if (operationCategory.parentGuid() == this.guid()) {
        allRelatedCategories.addAll(operationCategory.findAllRelatedCategories(operationCategories))
      }
    }
  }
  return allRelatedCategories
}

fun List<OperationCategory>.find(guid: String): OperationCategory {
  return this.first { it.guid() == guid }
}