package com.scyele.familyfinanceassistant.ui.login.join

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.scyele.familyfinanceassistant.entity.User
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.utils.extension.joinLocation
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Inject



class JoinFragmentViewModel() :
  BaseScreenVM<JoinFragmentViewModel.State>() {

  @Inject
  lateinit var auth: FirebaseAuth

  @Inject
  lateinit var database: FirebaseDatabase

  override fun initState() = State()

  fun onLoginChanged(text: CharSequence) {
    state.value = state.value.copy(
      locationLogin = Event(text.toString(), false)
    )
  }

  fun onPasswordChanged(text: CharSequence) {
    state.value = state.value.copy(
      locationPassword = Event(text.toString(), false)
    )
  }

  fun onOkButtonClicked() {
    val password = state.value.locationPassword.peekContent()
    val currentUser = auth.currentUser
    if (password.isEmpty() || currentUser == null) {
      state.value = state.value.copy(
        joinLocationError = Event("Password is empty or login process failed")
      )
      return
    }
    database.joinLocation(
      state.value.locationLogin.peekContent(),
      state.value.locationPassword.peekContent(),
      User(currentUser.uid, currentUser.email)
    ) { isConnected ->
      if (isConnected) {
        state.value = state.value.copy(joinedToLocation = SimpleEvent())
      } else {
        state.value =
          state.value.copy(joinLocationError = Event("Error in location id or password"))
      }
    }
  }

  fun onCancelClicked() {
    state.value = state.value.copy(canceled = SimpleEvent())
  }

  data class State(
    val locationLogin: Event<String> = Event(""),
    val locationPassword: Event<String> = Event(""),
    val joinLocationError: Event<String>? = null,
    val joinedToLocation: SimpleEvent? = null,
    val canceled: SimpleEvent? = null
  )
}