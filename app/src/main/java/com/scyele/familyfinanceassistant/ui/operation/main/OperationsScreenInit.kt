package com.scyele.familyfinanceassistant.ui.operation.main

import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.ui.base.LoadingParts

sealed class OperationsScreenInit {
  data class Loading(val emptyParts: List<LoadingParts>) : OperationsScreenInit()

  data class Loaded(val operationSummaryModels: List<OperationSummary>) : OperationsScreenInit()
}