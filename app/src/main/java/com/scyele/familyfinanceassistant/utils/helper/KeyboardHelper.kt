package com.scyele.familyfinanceassistant.utils.helper

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.Window
import android.view.inputmethod.InputMethodManager

object KeyboardHelper {
    fun dismiss(dialog: Dialog?) {
        if (dialog != null) {
            dismiss(dialog.window)
        }
    }

    fun dismiss(activity: Activity?) {
        if (activity != null) {
            dismiss(activity.window)
        }
    }

    private fun dismiss(window: Window?) {
        if (window != null) {
            Log.d("KeyboardHelper", "dismiss( $window ) :: dismissing the soft keyboard")
            val view = window.currentFocus
            if (view != null) {
                val imm = view.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
                view.clearFocus()
            }
        }
    }
}