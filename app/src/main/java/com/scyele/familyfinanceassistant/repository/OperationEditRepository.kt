package com.scyele.familyfinanceassistant.repository

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.*

abstract class OperationEditRepository<T : Operation, C : OperationCategory> {
  val operation = MutableLiveData<T?>()
  var operationCategory = MutableLiveData<OperationCategory?>()


  fun reinit() {
    operation.value = null
    operationCategory.value = null
  }
}

class ExpenseEditRepository : OperationEditRepository<Expense, ExpenseCategory>()

class IncomeEditRepository : OperationEditRepository<Income, IncomeCategory>()