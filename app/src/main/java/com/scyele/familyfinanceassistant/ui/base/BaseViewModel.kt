package com.scyele.familyfinanceassistant.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.scyele.lib.extension.livedata.SafeMediatorLiveData

abstract class BaseVM : ViewModel() {
  protected open fun tag(): String = "BaseVM"
}

abstract class BaseScreenVM<State> : BaseVM() {

  private val _state: LiveData<State> get() = state
  protected val state by lazy { getMediatorLiveDataState() }
  open fun getMediatorLiveDataState() =
    SafeMediatorLiveData(initState())

  abstract fun initState(): State

  fun state() = _state

  override fun tag(): String = "BaseScreenVM"
}

abstract class BaseProgressScreenVM<State> : BaseScreenVM<State>() {
  protected val loadingParts: MutableSet<LoadingParts> by lazy { getListOfLoadingParts() }

  protected abstract fun getListOfLoadingParts(): MutableSet<LoadingParts>
}