package com.scyele.familyfinanceassistant.ui

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.Location
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.lib.test.BaseTest
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll


open class DefaultMainRepositoryTest : BaseTest() {

  companion object {
    protected val mainRepository = mockk<MainRepository>()
    private val expenseCategory1 = ExpenseCategory("expenseCategory1", guid = "expenseCategory1")
    private val expenseCategory2 = ExpenseCategory("expenseCategory2", guid = "expenseCategory2")
    private val expenseCategory3 = ExpenseCategory("expenseCategory3", guid = "expenseCategory3")

    protected val expenseCategories = MutableLiveData(
      listOf(expenseCategory1, expenseCategory2, expenseCategory3)
    )

    private val accountCategory1 = AccountCategory("accountCategory1", guid = "accountCategory1")
    private val accountCategory2 = AccountCategory("accountCategory2", guid = "accountCategory2")
    private val accountCategory3 = AccountCategory("accountCategory3", guid = "accountCategory3")

    protected val accountCategories = MutableLiveData(
      listOf(accountCategory1, accountCategory2, accountCategory3)
    )

    private val expense1: Expense = Expense("1", accountCategory1.guid, expenseCategory1.guid)
    private val expense2: Expense = Expense("2", accountCategory2.guid, expenseCategory2.guid)
    private val expense3: Expense = Expense("3", accountCategory3.guid, expenseCategory1.guid)

    protected val expenses = MutableLiveData(
      listOf(expense1, expense2, expense3)
    )

    @BeforeAll
    @JvmStatic
    internal fun beforeAll() {
      every { mainRepository.expenses } returns expenses
      every { mainRepository.accountCategories } returns accountCategories
      every { mainRepository.expenseCategories } returns expenseCategories
      every { mainRepository.location } returns  MutableLiveData(Location("12345678"))
    }

    @AfterAll
    @JvmStatic
    internal fun afterAll() {
      //unused for now
    }

    @JvmStatic
    fun getMain() = mainRepository
  }
}