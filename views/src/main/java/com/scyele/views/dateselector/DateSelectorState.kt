package com.scyele.views.dateselector

import com.scyele.lib.extension.DateSelectorSelectionType
import com.scyele.views.base.MvvmCustomViewState
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class DateSelectorState(
  val selectionType: DateSelectorSelectionType = DateSelectorSelectionType.DAY,
  val date: Date = Date()
) : MvvmCustomViewState