package com.scyele.familyfinanceassistant.entity


data class Location(var password: String, val sharingLocation: String? = null, val users: MutableMap<String, User> = mutableMapOf()) {

  // Empty private constructor for DataSnapshot.getValue method
  private constructor() : this("")
}