package com.scyele.familyfinanceassistant.di.components

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.di.modules.AppModule
import com.scyele.familyfinanceassistant.ui.accounts.AccountsFragment
import com.scyele.familyfinanceassistant.ui.accounts.setup.AccountSetupFragment
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferFragment
import com.scyele.familyfinanceassistant.ui.expences.edit.ExpenseEditFragment
import com.scyele.familyfinanceassistant.ui.expences.main.ExpensesFragment
import com.scyele.familyfinanceassistant.ui.expences.setup.ExpenseCategorySetupFragment
import com.scyele.familyfinanceassistant.ui.expences.setup.MoveToExpenseCategoryFragment
import com.scyele.familyfinanceassistant.ui.history.HistoryFragment
import com.scyele.familyfinanceassistant.ui.incomes.edit.IncomeEditFragment
import com.scyele.familyfinanceassistant.ui.incomes.main.IncomesFragment
import com.scyele.familyfinanceassistant.ui.incomes.setup.IncomeCategorySetupFragment
import com.scyele.familyfinanceassistant.ui.incomes.setup.MoveToIncomeCategoryFragment
import com.scyele.familyfinanceassistant.ui.login.join.JoinFragment
import com.scyele.familyfinanceassistant.ui.main.MainActivity
import com.scyele.familyfinanceassistant.ui.settings.SettingsFragment
import com.scyele.familyfinanceassistant.ui.settings.location.ShareLocationFragment
import com.scyele.familyfinanceassistant.ui.statistics.StatisticsFragment
import dagger.Component
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    (AppModule::class)
  ]
)
interface AppComponent {
  fun inject(mainActivity: MainActivity)

  fun inject(assistantApplication: AssistantApplication)

  fun inject(accountsFragment: AccountsFragment)

  fun inject(accountSetupFragment: AccountSetupFragment)

  fun inject(transferFragment: TransferFragment)

  fun inject(incomesFragment: IncomesFragment)

  fun inject(historyFragment: HistoryFragment)

  fun inject(incomeCategorySetupFragment: IncomeCategorySetupFragment)

  fun inject(moveToIncomeCategoryFragment: MoveToIncomeCategoryFragment)

  fun inject(incomeEditFragment: IncomeEditFragment)

  fun inject(expenseEditFragment: ExpenseEditFragment)

  fun inject(expensesFragment: ExpensesFragment)

  fun inject(expenseCategorySetupFragment: ExpenseCategorySetupFragment)

  fun inject(moveToExpenseCategoryFragment: MoveToExpenseCategoryFragment)

  fun inject(statisticsFragment: StatisticsFragment)

  fun inject(settingsFragment: SettingsFragment)

  fun inject(shareLocationFragment: ShareLocationFragment)

  fun inject(joinFragment: JoinFragment)


  @Component.Builder
  interface Builder {
    fun build(): AppComponent

    fun appModule(appModule: AppModule): Builder
  }
}