package com.scyele.familyfinanceassistant.di.modules

import android.content.Context
import android.content.res.Resources
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.scyele.familyfinanceassistant.AssistantApplication
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Singleton




@Module
class FirebaseModule (private val assistantApplication: AssistantApplication) {

  @Singleton
  @Provides
  internal fun provideDatabase(): FirebaseDatabase {
    return FirebaseDatabase.getInstance()
  }

  @Singleton
  @Provides
  internal fun provideAuth(): FirebaseAuth {
    return FirebaseAuth.getInstance()
  }

  @Provides
  @Singleton
  fun provideContext(): Context = assistantApplication

  @Provides
  @Singleton
  fun provideResources(context: Context): Resources {
    return context.resources
  }
}