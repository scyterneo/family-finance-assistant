package com.scyele.familyfinanceassistant.ui.accounts.setup

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.Transfer
import com.scyele.familyfinanceassistant.repository.AccountCategorySetupRepository
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.ui.base.BaseProgressScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.LoadingParts
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.utils.Guid
import com.scyele.familyfinanceassistant.utils.extension.data.belongsToAccountCategory
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import com.scyele.lib.extension.string
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*


class AccountSetupFragmentViewModel(
  private val mainRepository: MainRepository,
  private val accountCategorySetupRepository: AccountCategorySetupRepository,
  private val resources: Resources
) : BaseProgressScreenVM<AccountSetupFragmentViewModel.State>() {

  private var initialModel: AccountCategory? = null

  private var incomes: List<Income> = listOf()
  private var expenses: List<Expense> = listOf()
  private var transfers: List<Transfer> = listOf()

  override fun initState() = State(loadingParts = Event(getListOfLoadingParts()))

  override fun getListOfLoadingParts(): MutableSet<LoadingParts> =
    HashSet(
      listOf(
        LoadingParts.INCOMES,
        LoadingParts.EXPENSES,
        LoadingParts.TRANSFERS,
        LoadingParts.INITIAL_ACCOUNT_CATEGORY
      )
    )

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(accountCategorySetupRepository.accountCategory) {
      initialModel = it
      loadingParts.remove(LoadingParts.INITIAL_ACCOUNT_CATEGORY)
      state.value = state.value.copy(
        name = Event(it?.name ?: ""),
        isDebtAccount = Event(it?.debtAccount ?: false)
      )
      updateLoading()
    }
    mediatorState.addSource(mainRepository.transfers) {
      transfers = it
      loadingParts.remove(LoadingParts.TRANSFERS)
      updateLoading()
    }
    mediatorState.addSource(mainRepository.incomes) {
      incomes = it
      loadingParts.remove(LoadingParts.INCOMES)
      updateLoading()
    }
    mediatorState.addSource(mainRepository.expenses) {
      expenses = it
      loadingParts.remove(LoadingParts.EXPENSES)
      updateLoading()
    }
    return mediatorState
  }

  fun onTextInputted(chars: CharSequence) {
    state.value = state.value.copy(name = Event(chars.toString(), false))
  }

  fun isDebtChanged(checked: Boolean) {
    state.value = state.value.copy(isDebtAccount = Event(checked, false))
  }

  fun saveModelAttempt() {
    if (checkModel()) {
      saveModel()
      state.value = state.value.copy(setupComplete = SimpleEvent())
    } else {
      state.value =
        state.value.copy(error = Event(resources.string(R.string.name_should_not_be_empty)))
    }
  }

  fun isRecordDeletable() = MutableLiveData<Boolean>().apply { value = initialModel != null }

  fun deleteModelAttempt() {
    initialModel?.let {
      val error = checkModelForDelete(it)
      if (error == null) {
        mainRepository.removeAccountCategory(it)
        state.value = state.value.copy(setupComplete = SimpleEvent())
        return@deleteModelAttempt
      }
      when (error) {
        AccountCategoryDeleteError.OPERATIONS_EXIST -> {
          state.value = state.value.copy(checkForDeleting = SimpleEvent())
          return@let
        }
      }
    }
  }

  fun deleteAllRelated() {
    initialModel?.let { accountCategory ->
      incomes
        .filter { it.accountCategoryGuid == accountCategory.guid }
        .forEach {
          mainRepository.removeOperation(it)
        }
      expenses
        .filter { it.accountCategoryGuid == accountCategory.guid }
        .forEach {
          mainRepository.removeOperation(it)
        }
      transfers
        .filter { it.belongsToAccountCategory(accountCategory) }
        .forEach {
          mainRepository.removeTransfer(it)
        }
      mainRepository.removeAccountCategory(accountCategory)
      state.value = state.value.copy(setupComplete = SimpleEvent())
    }

  }

  private fun checkModelForDelete(existingModel: AccountCategory): AccountCategoryDeleteError? {
    if (incomes.firstOrNull { it.accountCategoryGuid == existingModel.guid } != null) {
      return AccountCategoryDeleteError.OPERATIONS_EXIST
    }
    if (expenses.firstOrNull { it.accountCategoryGuid == existingModel.guid } != null) {
      return AccountCategoryDeleteError.OPERATIONS_EXIST
    }
    if (transfers.firstOrNull { it.belongsToAccountCategory(existingModel) } != null) {
      return AccountCategoryDeleteError.OPERATIONS_EXIST
    }
    return null
  }

  private fun updateLoading() {
    if (state.value.loadingParts != loadingParts) {
      state.value = state.value.copy(loadingParts = Event(loadingParts))
    }
  }


  private fun checkModel(): Boolean {
    state.value.name.peekContent().let {
      if (it.isNotEmpty()) return true
    }
    return false
  }

  private fun saveModel() {
    mainRepository.setAccountCategory(
      AccountCategory(
        state.value.name.peekContent(),
        initialModel?.guid ?: Guid.new(),
        state.value.isDebtAccount.peekContent()
      )
    )
  }

  data class State(
    val name: Event<String> = Event(""),
    val isDebtAccount: Event<Boolean> = Event(false),
    val setupComplete: SimpleEvent? = null,
    val loadingParts: Event<Set<LoadingParts>> = Event(setOf()),
    val checkForDeleting: SimpleEvent? = null,
    val error: Event<String>? = null
  )
}