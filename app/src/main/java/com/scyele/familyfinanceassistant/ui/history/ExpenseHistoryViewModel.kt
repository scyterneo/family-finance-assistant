package com.scyele.familyfinanceassistant.ui.history

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.ui.base.BaseVM
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel
import com.scyele.lib.extension.display

class ExpenseHistoryViewModel : HistoryViewModel() {
  private val expenseCategoryName = MutableLiveData<String>()
  private val accountCategoryName = MutableLiveData<String>()
  private val expenseComment = MutableLiveData<String?>()
  private val expenseSum = MutableLiveData<String>()
  private val expenseDate = MutableLiveData<String>()

  fun bind(expenseHistoryModel: OperationHistoryModel.ExpenseHistoryModel) {
    expenseSum.value = expenseHistoryModel.sum
    expenseCategoryName.value = expenseHistoryModel.expenseCategoryName
    accountCategoryName.value = expenseHistoryModel.accountCategoryName
    expenseComment.value = expenseHistoryModel.comment
    expenseDate.value = expenseHistoryModel.date.display()
  }

  fun getExpenseCategoryName() = expenseCategoryName
  fun getAccountCategoryName() = accountCategoryName
  fun getExpenseSum() = expenseSum
  fun getExpenseDate() = expenseDate
  fun getExpenseComment() = expenseComment
}