package com.scyele.familyfinanceassistant.ui.accounts.setup

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.AccountSetupLayoutBinding
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.familyfinanceassistant.utils.helper.ToastHelper
import com.scyele.lib.extension.setStateText
import kotlinx.android.synthetic.main.account_setup_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class AccountSetupFragment : BaseDialogFragment<
    AccountSetupFragmentViewModel,
    AccountSetupLayoutBinding,
    AccountSetupFragmentViewModel.State,
    FragmentViewModelFactory>() {

  override fun getLayoutId(): Int {
    return R.layout.account_setup_layout
  }

  override fun bindToViewModel() {
    binding.viewModel = viewModel

    setup_dialog_title.text = getSetupDialogTitle()
    input_layout.hint = getSetupHint()
  }

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<AccountSetupFragmentViewModel> {
    return AccountSetupFragmentViewModel::class.java
  }

  private fun getSetupHint(): String {
    return getString(R.string.account_name)
  }

  private fun getSetupDialogTitle(): String {
    return getString(R.string.account_setup)
  }

  override fun handleEvents(state: AccountSetupFragmentViewModel.State) {
    state.setupComplete?.getContent()?.let {
      dismiss()
    }
    state.name.getContent()?.let {
      edit_text.setStateText(it)
    }
    state.isDebtAccount.getContent()?.let {
      is_debt_account.isChecked = it
    }
    state.error?.getContent()?.let {
      ToastHelper.showToast(context, it)
    }
    state.checkForDeleting?.getContent()?.let {
      DialogHelper.showThreeButtonsDialog(
        activity,
        getString(R.string.delete_account_title),
        getString(R.string.delete_account_message),
        android.R.drawable.ic_delete,
        getString(R.string.do_not_delete_current_account),
        {},
        getString(R.string.delete_all_operations),
        { viewModel.deleteAllRelated() }
      )
    }
  }

  override fun tag() = "AccountSetupFragment"
}