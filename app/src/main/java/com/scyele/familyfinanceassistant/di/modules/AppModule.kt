package com.scyele.familyfinanceassistant.di.modules

import android.content.Context
import android.content.res.Resources
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.repository.*
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferFragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferRepository
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.main.MainActivityViewModelFactory
import com.scyele.familyfinanceassistant.ui.settings.location.SettingsFragmentViewModelFactory
import com.scyele.lib.SharedPreferencesHelper
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Singleton


@Module
class AppModule(private val assistantApplication: AssistantApplication) {

  @Provides
  @Singleton
  fun provideMainActivityViewModelFactory(
    operationCategorySetupRepository: OperationCategorySetupRepository,
    accountCategorySetupRepository: AccountCategorySetupRepository,
    progressRepository: ProgressRepository
  ) = MainActivityViewModelFactory(
    operationCategorySetupRepository,
    accountCategorySetupRepository,
    progressRepository
  )

  @Provides
  fun provideFragmentViewModelFactory(
    mainRepository: MainRepository,
    incomeEditRepository: IncomeEditRepository,
    expenseEditRepository: ExpenseEditRepository,
    operationCategorySetupRepository: OperationCategorySetupRepository,
    accountCategorySetupRepository: AccountCategorySetupRepository,
    progressRepository: ProgressRepository,
    transferRepository: TransferRepository,
    moveToRepository: MoveToRepository,
    resources: Resources
  ) =
    FragmentViewModelFactory(
      mainRepository,
      incomeEditRepository,
      expenseEditRepository,
      operationCategorySetupRepository,
      accountCategorySetupRepository,
      progressRepository,
      transferRepository,
      moveToRepository,
      resources
    )

  @Provides
  fun provideSettingsFragmentViewModelFactory(
    mainRepository: MainRepository,
    authRepository: AuthRepository
  ) =
    SettingsFragmentViewModelFactory(
      mainRepository,
      authRepository
    )

  @Provides
  fun provideTransferViewModelFactory(
    mainRepository: MainRepository,
    transferRepository: TransferRepository
  ) = TransferFragmentViewModelFactory(mainRepository, transferRepository)

  @Provides
  @Singleton
  fun provideTransferRepository() = TransferRepository()

  @Provides
  @Singleton
  fun provideMainRepository() = MainRepository()

  @Provides
  @Singleton
  fun provideMoveToRepository() = MoveToRepository()

  @Provides
  @Singleton
  fun provideEditIncomeRepository() = IncomeEditRepository()

  @Provides
  @Singleton
  fun provideOperationCategorySetupRepository() = OperationCategorySetupRepository()

  @Provides
  @Singleton
  fun provideEditExpenseRepository() = ExpenseEditRepository()

  @Provides
  @Singleton
  fun provideAccountCategorySetupRepository() = AccountCategorySetupRepository()

  @Provides
  @Singleton
  fun provideProgressRepository(sharedPreferencesHelper: SharedPreferencesHelper) =
    ProgressRepository(sharedPreferencesHelper)

  @Provides
  @Singleton
  fun provideContext(): Context = assistantApplication

  @Provides
  fun provideResources(context: Context): Resources {
    return context.resources
  }

  @Provides
  @Singleton
  fun provideSharedPreferencesHelper(context: Context) = SharedPreferencesHelper(context)

  @Provides
  @Singleton
  fun provideAuthRepository() = AuthRepository()
}