package com.scyele.familyfinanceassistant.utils.extension.data

import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.Transfer

/**
 * Method checks if Operation belongs to selected category or any of it's child category
 *
 * @param selectedOperationCategory selected category
 * @param operationCategories list of all categories
 *
 * @return true if belongs or if selected category is not selected (null)
 */
fun Operation.belongsToOperationCategory(
  selectedOperationCategory: OperationCategory?,
  operationCategories: List<OperationCategory>
): Boolean {
  if (selectedOperationCategory == null)
    return true

  if (operationCategoryGuid() == selectedOperationCategory.guid())
    return true

  val allRelatedCategories = selectedOperationCategory.findAllRelatedCategories(operationCategories)
  for (relatedCategory in allRelatedCategories) {
    if (operationCategoryGuid() == relatedCategory.guid()) return true
  }

  return false
}

/**
 * Method checks if Operation belongs to selected account category
 *
 * @param accountCategory selected account category
 *
 * @return true if transfer made to or from accountCategory or if category is not selected (null)
 */
fun Transfer.belongsToAccountCategory(
  accountCategory: AccountCategory?
): Boolean =
  accountCategory == null
      || this.fromAccountCategoryGuid == accountCategory.guid
      || this.toAccountCategoryGuid == accountCategory.guid


fun Operation.belongsToAccountCategory(
  accountCategory: AccountCategory?
): Boolean =
  accountCategory == null || this.accountCategoryGuid() == accountCategory.guid