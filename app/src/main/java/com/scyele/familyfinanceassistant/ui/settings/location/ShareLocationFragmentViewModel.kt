package com.scyele.familyfinanceassistant.ui.settings.location

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.repository.AuthRepository
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

class ShareLocationFragmentViewModel(
  private val mainRepository: MainRepository,
  private val authRepository: AuthRepository
) : BaseScreenVM<ShareLocationFragmentViewModel.State>() {

  private val isPasswordInputEnabled = MutableLiveData<Boolean>()
  private val isSetupButtonEnabled = MutableLiveData<Boolean>()
  private val isShareButtonEnabled = MutableLiveData<Boolean>()

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(mainRepository.location) {
      val passwordState =
        if (it.password.isEmpty()) EditPasswordState.NO_PASSWORD
        else EditPasswordState.SET
      state.value = state.value.copy(
        locationPassword = Event(it.password),
        editPasswordState = Event((passwordState))
      )
      updateFieldsEnabling()
    }
    return mediatorState
  }

  override fun initState() = State()

  fun onPasswordChanged(text: CharSequence) {
    val editPasswordState = when (state.value.editPasswordState.peekContent()) {
      EditPasswordState.EDITING_READY, EditPasswordState.EDITING_NOT_READY, EditPasswordState.NO_PASSWORD -> {
        if (text.length >= 8) EditPasswordState.EDITING_READY else EditPasswordState.EDITING_NOT_READY
      }
      EditPasswordState.SET -> EditPasswordState.SET
    }

    state.value = state.value.copy(
      locationPassword = Event(text.toString(), false),
      editPasswordState = Event(editPasswordState)
    )
    updateFieldsEnabling()
  }

  fun onPassButtonClicked() {
    when (state.value.editPasswordState.peekContent()) {
      EditPasswordState.SET -> {
        state.value = state.value.copy(
          editPasswordState = Event(EditPasswordState.EDITING_NOT_READY),
          locationPassword = Event("")
        )
        updateFieldsEnabling()
      }
      EditPasswordState.NO_PASSWORD, EditPasswordState.EDITING_NOT_READY -> {
        // this should never happen
      }
      EditPasswordState.EDITING_READY -> savePassword()
    }
  }

  fun onOkShareButtonClicked() {
    val userId = authRepository.getUserID()
    if (userId != null) {
      state.value = state.value.copy(
        onShareClicked = Event(
          LocationValues(userId, state.value.locationPassword.peekContent())
        )
      )
    }
  }

  fun onOkButtonClicked() {
    state.value = state.value.copy(onOkButtonClicked = SimpleEvent())
  }

  fun isPasswordInputEnabled() = isPasswordInputEnabled

  fun isSetupButtonEnabled() = isSetupButtonEnabled

  fun isShareButtonEnabled() = isShareButtonEnabled

  private fun savePassword() {
    mainRepository.setPassword(state.value.locationPassword.peekContent())
  }

  private fun updateFieldsEnabling() {
    when (state.value.editPasswordState.peekContent()) {
      EditPasswordState.NO_PASSWORD -> {
        isPasswordInputEnabled.value = true
        isSetupButtonEnabled.value = false
        isShareButtonEnabled.value = false
      }
      EditPasswordState.SET -> {
        isPasswordInputEnabled.value = false
        isSetupButtonEnabled.value = true
        isShareButtonEnabled.value = true
      }
      EditPasswordState.EDITING_NOT_READY -> {
        isPasswordInputEnabled.value = true
        isSetupButtonEnabled.value = false
        isShareButtonEnabled.value = false
      }
      EditPasswordState.EDITING_READY -> {
        isPasswordInputEnabled.value = true
        isSetupButtonEnabled.value = true
        isShareButtonEnabled.value = false
      }
    }
  }

  data class State(
    val locationPassword: Event<String> = Event(""),
    val editPasswordState: Event<EditPasswordState> = Event(EditPasswordState.NO_PASSWORD),
    val onOkButtonClicked: SimpleEvent? = null,
    val onShareClicked: Event<LocationValues>? = null
  )

  enum class EditPasswordState {
    NO_PASSWORD, SET, EDITING_NOT_READY, EDITING_READY
  }

  data class LocationValues(
    val id: String,
    val password: String
  )
}