package com.scyele.familyfinanceassistant.ui.operation.edit

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.find
import com.scyele.familyfinanceassistant.extension.isPositive
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.OperationEditRepository
import com.scyele.familyfinanceassistant.repository.ProgressRepository
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.utils.Guid
import com.scyele.familyfinanceassistant.utils.extension.data.find
import com.scyele.familyfinanceassistant.utils.extension.toRateFloat
import com.scyele.lib.extension.display
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import java.math.BigDecimal
import java.util.Date


abstract class OperationEditFragmentViewModel<Action, Category>(
  protected val mainRepository: MainRepository,
  private val operationEditRepository: OperationEditRepository<Action, Category>,
  private val progressRepository: ProgressRepository
) : BaseScreenVM<OperationEditFragmentViewModel.State>()
    where Action : Operation,
          Category : OperationCategory {

  val operationCategories: MutableLiveData<List<OperationCategory>> = MutableLiveData(listOf())
  val accountCategories: MutableLiveData<List<AccountCategory>> = MutableLiveData(listOf())
  private var operationCategory: OperationCategory? = null
  val operationCategoryPosition: MutableLiveData<Int> = MutableLiveData()
  private var accountCategory: AccountCategory? = null
  val accountCategoryPosition: MutableLiveData<Int> = MutableLiveData()

  private val initialOperation: Operation? = operationEditRepository.operation.value

  lateinit var date: Date
  private val dateString = MutableLiveData<String>()

  private var operations: List<Action> = emptyList()
  private val firstSumButton = MutableLiveData("1")
  private val secondSumButton = MutableLiveData("2")
  private val thirdSumButton = MutableLiveData("3")

  private val isCommentVisible = MutableLiveData(false)
  private val isRateInputVisible = MutableLiveData(false)

  init {
    state.value = state.value.copy(
      sum = Event(initialOperation?.sum() ?: ""),
      commentary = Event(initialOperation?.comment() ?: ""),
      rate = Event(
        if (initialOperation == null) progressRepository.getRate().toString()
        else 1f.toString()
      ),
    )
    setOperationCategory(operationEditRepository.operationCategory.value)
    updateDate(initialOperation?.date() ?: Date())
  }

  override fun initState(): State = State()

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(getAllOperationCategories()) {
      operationCategories.value = it
        .filter { operationCategory -> !operationCategory.isParentCategory() }
        .sortedBy { operationCategory -> operationCategory.name() }
      if (initialOperation != null) {
        setOperationCategory(it.find(initialOperation.operationCategoryGuid()))
      } else if (operationCategory != null) {
        setOperationCategory(operationCategory)
      }
    }
    mediatorState.addSource(mainRepository.accountCategories) { accountCategories ->
      this.accountCategories.value = accountCategories.filter { accountCategory -> !accountCategory.debtAccount }
      when {
        initialOperation != null -> {
          setAccountCategory(accountCategories.find(initialOperation.accountCategoryGuid()))
        }
        accountCategory != null -> {
          setAccountCategory(accountCategory)
        }
        else -> {
          operationCategory?.let { operationCategory ->
            setAccountCategoryForOperationCategory(operationCategory)
          }
        }
      }
    }
    mediatorState.addSource(getOperations()) {
      operations = it
      updateFastSumButtons()
    }
    return mediatorState
  }

  override fun onCleared() {
    super.onCleared()
    operationEditRepository.reinit()
  }

  abstract fun getAllOperationCategories(): MutableLiveData<List<Category>>

  abstract fun getOperations(): MutableLiveData<List<Action>>

  abstract fun createNewOperation(
    guid: String,
    sum: String = "0",
    accountCategoryGuid: String,
    actionCategoryGuid: String,
    date: Date = Date(),
    comment: String? = null
  ): Action

  fun onSelectDateClicked() {
    state.value = state.value.copy(selectDate = Event(Date()))
  }

  fun onDeleteClicked() {
    if (initialOperation != null) {
      mainRepository.removeOperation(initialOperation)
      state.value = state.value.copy(deleteOperation = SimpleEvent())
    }
  }

  fun hasInitialOperation() = MutableLiveData<Boolean>().apply { value = initialOperation != null }

  fun onOkClicked() {
    if (validateOperationData()) {
      saveOperation()
      state.value = state.value.copy(addOperationComplete = SimpleEvent())
    } else {
      state.value = state.value.copy(addOperationError = SimpleEvent())
    }
  }

  fun getDateString() = dateString

  fun onOperationSumChanged(text: CharSequence) {
    state.value = state.value.copy(sum = Event(text.toString(), false))
  }

  fun onRateChanged(text: CharSequence) {
    state.value = state.value.copy(rate = Event(text.toString(), false))
  }

  fun onOperationCommentChanged(text: CharSequence) {
    state.value = state.value.copy(
      commentary = Event(text.toString(), false)
    )
  }

  val onOperationCategorySelected: (position: Int) -> Unit = {
    operationCategory = operationCategories.value!![it]
  }

  val onAccountCategorySelected: (position: Int) -> Unit = {
    accountCategory = accountCategories.value!![it]
  }

  fun updateDate(date: Date) {
    this.date = date
    dateString.value = date.display()
  }

  fun onAddOperationComment() {
    isCommentVisible.value = true
  }

  fun onAddOperationRate() {
    if (initialOperation == null) {
      isRateInputVisible.value = true
    }
  }

  fun onFirstButtonClicked() {
    firstSumButton.value?.let {
      inputSum(it)
    }
  }

  fun onSecondButtonClicked() {
    secondSumButton.value?.let {
      inputSum(it)
    }
  }

  fun onThirdButtonClicked() {
    thirdSumButton.value?.let {
      inputSum(it)
    }
  }

  private fun inputSum(sum: String) {
    state.value = state.value.copy(sum = Event(sum))
    onOkClicked()
  }

  fun isRateInputVisible() = isRateInputVisible

  fun isCommentVisible() = isCommentVisible

  fun getFirstButtonText() = firstSumButton

  fun getSecondButtonText() = secondSumButton

  fun getThirdButtonText() = thirdSumButton

  private fun validateOperationData(): Boolean {
    return state.value.sum.peekContent().isNotEmpty()
        && accountCategory != null
        && operationCategory != null
        && BigDecimal(state.value.sum.peekContent()).isPositive()
  }

  private fun saveOperation() {
    val rate = state.value.rate.peekContent().toRateFloat()
    val inputSum = state.value.sum.peekContent().toFloatOrNull() ?: 0f
    val operation = createNewOperation(
        guid = initialOperation?.guid() ?: Guid.new(),
        sum = (rate * inputSum).toString(),
        accountCategoryGuid = accountCategory!!.guid,
        actionCategoryGuid = operationCategory!!.guid(),
        date = date,
        comment = state.value.commentary.peekContent()
    )

    mainRepository.setOperation(operation)

    progressRepository.saveOperationAccountCategory(
      operation.operationCategoryGuid(), operation.accountCategoryGuid()
    )
    if (initialOperation == null) {
      progressRepository.setRate(rate)
    }
  }

  private fun setOperationCategory(operationCategory: OperationCategory?) {
    this.operationCategory = operationCategory
    operationCategory?.let {
      operationCategories.value?.let { operationCategories ->
        operationCategoryPosition.value = operationCategories.indexOf(operationCategory)
      }
      try {
        setAccountCategoryForOperationCategory(operationCategory)
      } catch (exception: Exception) {
        // no need to log anything, this could happen if category deleted
      }
    }
    updateFastSumButtons()
  }

  private fun updateFastSumButtons() {
    val currentOperations = if (operationCategory == null) {
      operations
    } else {
      operations.filter { it.operationCategoryGuid() == operationCategory?.guid() }
    }

    currentOperations
      .groupingBy { it.sum() }
      .eachCount()
      .toList()
      .sortedByDescending { (_, value) -> value }
      .toMap()
      .forEach{(key, count) ->
        when (count) {
          0 -> {
            firstSumButton.value = key
          }
          1 -> {
            secondSumButton.value = key
          }
          2 -> {
            thirdSumButton.value = key
          }
        }
      }
  }

  private fun setAccountCategory(accountCategory: AccountCategory?) {
    this.accountCategory = accountCategory
    if (accountCategory != null && accountCategories.value != null) {
      this.accountCategoryPosition.value = accountCategories.value!!.indexOf(accountCategory)
    }
  }

  private fun setAccountCategoryForOperationCategory(operationCategory: OperationCategory) {
    val lastAccountCategoryGuid =
      progressRepository.getOperationAccountCategory(operationCategory.guid())
    if (lastAccountCategoryGuid.isNotEmpty()) {
      accountCategories.value?.let { accountCategories ->
        accountCategories.find(lastAccountCategoryGuid)?.let { lastAccountCategory ->
          accountCategoryPosition.value = accountCategories.indexOf(lastAccountCategory)
        }
      }
    }
  }

  data class State(
    val commentary: Event<String> = Event(""),
    val sum: Event<String> = Event(""),
    val rate: Event<String> = Event("1"),
    val selectDate: Event<Date>? = null,
    val addOperationComplete: SimpleEvent? = null,
    val addOperationError: SimpleEvent? = null,
    val deleteOperation: SimpleEvent? = null
  )
}
