package com.scyele.familyfinanceassistant.summary

import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.Transfer
import com.scyele.familyfinanceassistant.extension.value
import java.math.BigDecimal

data class AccountSummary(
  val accountCategory: AccountCategory,
  val incomes: List<Income>,
  val expenses: List<Expense>,
  var transfers: List<Transfer>
) {

  fun calculateSum(): BigDecimal {
    var sum = BigDecimal.ZERO
    for (income in incomes) {
      sum += income.sum.value()
    }
    for (expense in expenses) {
      sum -= expense.sum.value()
    }
    for (transfer in transfers) {
      if (transfer.fromAccountCategoryGuid == accountCategory.guid) {
        sum -= transfer.sum.value()
      } else if (transfer.toAccountCategoryGuid == accountCategory.guid) {
        sum += transfer.sum.value()
      }
    }
    return sum
  }
}