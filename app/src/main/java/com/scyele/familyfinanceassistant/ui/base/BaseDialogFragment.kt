package com.scyele.familyfinanceassistant.ui.base

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.scyele.familyfinanceassistant.utils.helper.KeyboardHelper
import com.scyele.lib.extension.livedata.observeNonNull
import javax.inject.Inject

abstract class BaseDialogFragment<
    Model : BaseScreenVM<State>,
    Binding : ViewDataBinding,
    State,
    Factory : ViewModelProvider.Factory>
  : DialogFragment() {
  lateinit var binding: Binding
  lateinit var viewModel: Model

  @Inject
  lateinit var factory: Factory

  override fun onAttach(context: Context) {
    super.onAttach(context)
    injectDependency()
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    binding = DataBindingUtil
      .inflate(inflater, getLayoutId(), container, false)

    setupView()

    if (dialog != null && dialog?.window != null) {
      dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
      dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }

    return binding.root
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    viewModel = ViewModelProvider(this, factory).get(getModelClass())
    bindToViewModel()

    viewModel.state().observeNonNull(this) {
      handleEvents(it)
    }
  }

  override fun dismiss() {
    KeyboardHelper.dismiss(dialog)
    super.dismiss()
  }

  open fun setupView() {
    // base empty implementation
  }

  abstract fun injectDependency()

  abstract fun getLayoutId(): Int

  abstract fun bindToViewModel()

  abstract fun handleEvents(state: State)

  abstract fun getModelClass(): Class<Model>

  abstract fun tag(): String
}