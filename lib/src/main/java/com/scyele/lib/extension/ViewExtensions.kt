package com.scyele.lib.extension

import android.content.Context
import android.content.ContextWrapper
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSpinner


fun View.getParentActivity(): AppCompatActivity? {
  var context = this.context
  while (context is ContextWrapper) {
    if (context is AppCompatActivity) {
      return context
    }
    context = context.baseContext
  }
  return null
}

inline fun EditText.onTextChanged(
  triggerSelection: Boolean = true,
  crossinline afterTextChanged: (String) -> Unit
) {
  val editText = this
  val textWatcher = object : TextWatcher {
    override fun beforeTextChanged(cs: CharSequence, start: Int, before: Int, count: Int) {
    }

    override fun onTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun afterTextChanged(editable: Editable?) {
      if (!editText.hasFocus()) return
      afterTextChanged.invoke(editable.toString())
      if (triggerSelection) setSelection(editable.toString().length)
    }
  }

  editText.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
    override fun onViewAttachedToWindow(v: View) {
      TODO("Not yet implemented")
    }

    override fun onViewDetachedFromWindow(v: View) {
      editText.removeTextChangedListener(textWatcher)
      editText.removeOnAttachStateChangeListener(this)
    }
  })

  editText.addTextChangedListener(textWatcher)
}

fun EditText.setStateText(string: String) {
  if (this.text.toString() == string) return
  setText(string)
}

fun setupSpinner(spinner: AppCompatSpinner, context: Context, items: List<String>) {
  val adapter = ArrayAdapter(
    context,
    android.R.layout.simple_spinner_item,
    items
  )

  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

  spinner.adapter = adapter
}

fun initSpinner(
  spinner: AppCompatSpinner,
  onItemSelected: (position: Int) -> Unit
) {
  spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
      onItemSelected.invoke(-1)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
      onItemSelected.invoke(position)
    }
  }
}

fun initSpinner(
  spinner: AppCompatSpinner,
  adapter: ArrayAdapter<String>,
  selectedPosition: Int,
  onItemSelected: (position: Int) -> Unit
) {
  spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
      onItemSelected.invoke(-1)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
      onItemSelected.invoke(position)
    }
  }
  spinner.adapter = adapter
  val elementsCount = adapter.count
  if (elementsCount == 0) return

  spinner.setSelection(
    if (selectedPosition < 0 || selectedPosition > elementsCount) {
      elementsCount - 1
    } else {
      selectedPosition
    },
    true
  )
}