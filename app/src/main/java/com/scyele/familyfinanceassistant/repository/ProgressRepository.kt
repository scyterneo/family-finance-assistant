package com.scyele.familyfinanceassistant.repository

import com.scyele.lib.SharedPreferencesHelper

class ProgressRepository(private val sharedPreferencesHelper: SharedPreferencesHelper) {
  companion object {
    const val LAST_SCREEN_ID = "last screen id"
  }

  fun setCollapsed(guid: String, isCollapsed: Boolean) {
    sharedPreferencesHelper.set(guid, isCollapsed)
  }

  fun getCollapsed(guid: String): Boolean =
    sharedPreferencesHelper.getBoolean(guid, true)

  fun setLastOpenedScreen(screenId: Int) {
    sharedPreferencesHelper.set(LAST_SCREEN_ID, screenId)
  }

  fun getLastOpenedScreen(): Int =
    sharedPreferencesHelper.getInt(LAST_SCREEN_ID)

  fun saveOperationAccountCategory(operationCategoryGuid: String, accountCategoryGuid: String) {
    sharedPreferencesHelper.setOperationAccountCategory(operationCategoryGuid, accountCategoryGuid)
  }

  fun getOperationAccountCategory(operationCategoryGuid: String): String =
    sharedPreferencesHelper.getOperationAccountCategory(operationCategoryGuid)

  fun setRate(rate: Float) = sharedPreferencesHelper.setRate(rate)

  fun getRate() = sharedPreferencesHelper.getRate()
}
