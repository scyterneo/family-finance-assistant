package com.scyele.familyfinanceassistant.ui.accounts.transfer

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.TransferEditLayoutBinding
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.familyfinanceassistant.utils.helper.ToastHelper
import kotlinx.android.synthetic.main.transfer_edit_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import java.math.BigDecimal




class TransferFragment : BaseDialogFragment<
    TransferFragmentViewModel,
    TransferEditLayoutBinding,
    TransferFragmentViewModel.State,
    TransferFragmentViewModelFactory>() {

  private lateinit var fromAdapter: ArrayAdapter<String>

  private lateinit var toAdapter: ArrayAdapter<String>

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getLayoutId() = R.layout.transfer_edit_layout

  override fun bindToViewModel() {
    binding.viewModel = viewModel

    setupAdapters(emptyList())

    setupSpinnersSelection()
  }

  override fun handleEvents(state: TransferFragmentViewModel.State) {
    state.selectDate?.getContent()?.let { currentDate ->
      DialogHelper.showDateSelectionDialog(
        activity,
        { newDate -> viewModel.setDate(newDate) },
        currentDate
      )
    }
    state.transferComplete?.getContent()?.let {
      dismiss()
    }
    state.transferError?.getContent()?.let {
      ToastHelper.showToast(requireContext(), getString(R.string.transfer_error))
    }
    state.accountCategories?.getContent()?.let { accountCategories ->
      setupAdapters(accountCategories.map { it.name })
    }
    val accounts = state.accountCategories?.peekContent()
    val fromAccountPosition = accounts?.indexOf(state.transferFromAccount) ?: -1
    val toAccountPosition = accounts?.indexOf(state.transferToAccount) ?: -1

    spinner_transfer_from.setSelection(fromAccountPosition)
    spinner_transfer_to.setSelection(toAccountPosition)

    val stateTransferSum = state.transferSum.toString()
    if (stateTransferSum != transfer_sum_input.text.toString()) {
      if (state.transferSum == BigDecimal.ZERO) {
        transfer_sum_input.setText("")
      } else {
        transfer_sum_input.setText(stateTransferSum)
      }
    }

    val stateTransferComment = state.transferComment
    if (stateTransferComment != transfer_comment_input.text.toString()) {
      transfer_comment_input.setText(stateTransferComment)
    }
  }

  override fun getModelClass(): Class<TransferFragmentViewModel> {
    return TransferFragmentViewModel::class.java
  }

  override fun tag() = "TransferFragment"

  private fun setupAdapters(accountsNames: List<String>) {
    fromAdapter = ArrayAdapter(
      requireContext(),
      android.R.layout.simple_spinner_item,
      accountsNames
    )

    toAdapter = ArrayAdapter(
      requireContext(),
      android.R.layout.simple_spinner_item,
      accountsNames
    )

    fromAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    toAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

    spinner_transfer_from.adapter = fromAdapter
    spinner_transfer_to.adapter = toAdapter
  }

  private fun setupSpinnersSelection() {
    spinner_transfer_from.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      override fun onNothingSelected(parent: AdapterView<*>?) {
        viewModel.onFromAccountSelected(-1)
      }

      override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.onFromAccountSelected(position)
      }
    }

    spinner_transfer_to.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      override fun onNothingSelected(parent: AdapterView<*>?) {
        viewModel.onToAccountSelected(-1)
      }

      override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.onToAccountSelected(position)
      }
    }
  }
}