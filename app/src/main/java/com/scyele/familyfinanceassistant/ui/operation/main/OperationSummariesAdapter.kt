package com.scyele.familyfinanceassistant.ui.operation.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.OperationSummaryItemBinding
import com.scyele.familyfinanceassistant.summary.OperationSummary

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class OperationSummariesAdapter(
  val clickEvent: (String) -> Unit,
  val longClickEvent: (String) -> Unit
) :
  ListAdapter<OperationSummary, OperationSummariesAdapter.OperationSummaryViewHolder>(
    OperationSummaryCallback()
  ) {

  private lateinit var binding: OperationSummaryItemBinding

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): OperationSummaryViewHolder {
    binding = DataBindingUtil.inflate(
      LayoutInflater.from(parent.context),
      R.layout.operation_summary_item, parent, false
    )
    return OperationSummaryViewHolder(binding)
  }

  override fun onBindViewHolder(holder: OperationSummaryViewHolder, position: Int) {
    holder.itemView.setBackgroundResource(R.drawable.bg_list_item_states)
    holder.bind(getItem(position))
  }

  inner class OperationSummaryViewHolder(private val binding: OperationSummaryItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    private val viewModel =
      OperationSummaryViewModel()

    fun bind(OperationSummary: OperationSummary) {
      viewModel.bind(OperationSummary)
      binding.viewModel = viewModel

      itemView.setOnClickListener {
        clickEvent.invoke(OperationSummary.operationCategory().guid())
      }
      itemView.setOnLongClickListener {
        longClickEvent.invoke(OperationSummary.operationCategory().guid())
        true
      }
    }
  }

  private class OperationSummaryCallback : DiffUtil.ItemCallback<OperationSummary>() {
    override fun areItemsTheSame(
      oldItem: OperationSummary,
      newItem: OperationSummary
    ): Boolean {
      return oldItem::class == newItem::class
          && oldItem.operationCategory().equals(newItem.operationCategory())
          && oldItem.operationCategory().isParentCategory() == newItem.operationCategory().isParentCategory()
          && oldItem.levelOfDepth() == newItem.levelOfDepth()
    }

    override fun areContentsTheSame(
      oldItem: OperationSummary,
      newItem: OperationSummary
    ): Boolean {
      return oldItem.operationSum() == newItem.operationSum()
          && oldItem.operations() == newItem.operations()
          && oldItem.levelOfDepth() == newItem.levelOfDepth()
          && oldItem.isCollapsedSummary() == newItem.isCollapsedSummary()
    }
  }
}