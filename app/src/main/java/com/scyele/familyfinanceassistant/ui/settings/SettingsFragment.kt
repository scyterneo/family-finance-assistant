package com.scyele.familyfinanceassistant.ui.settings

import com.google.android.gms.ads.AdRequest
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.SettingsFragmentBinding
import com.scyele.familyfinanceassistant.ui.base.BaseFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.settings.location.ShareLocationFragment
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class SettingsFragment : BaseFragment<
    SettingsFragmentViewModel,
    SettingsFragmentBinding,
    SettingsFragmentViewModel.State,
    FragmentViewModelFactory>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getLayoutId() = R.layout.settings_fragment

  override fun bindingViewModeSetViewModel() {
    binding.viewModel = viewModel
    val adRequest = AdRequest.Builder().build()
    binding.adView.loadAd(adRequest)
  }

  override fun handleEvents(state: SettingsFragmentViewModel.State) {
    state.shareLocationClicked?.getContent()?.let {
      DialogHelper.showDialogFragment(ShareLocationFragment(), activity)
    }
  }

  override fun getModelClass(): Class<SettingsFragmentViewModel> {
    return SettingsFragmentViewModel::class.java
  }

  override fun tag() = "SettingsFragment"
}