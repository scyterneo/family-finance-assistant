package com.scyele.views.base

import androidx.lifecycle.LifecycleOwner

interface MvvmCustomView<S : MvvmCustomViewState, M : MvvmCustomViewModel<S>> {
  val viewModel: M

  fun onLifecycleOwnerAttached(lifecycleOwner: LifecycleOwner?)
}