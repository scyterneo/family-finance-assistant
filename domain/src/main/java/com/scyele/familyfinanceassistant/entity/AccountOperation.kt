package com.scyele.familyfinanceassistant.entity

import java.util.*


interface AccountOperation {

  fun guid(): String

  fun date(): Date

}

fun List<AccountOperation>.find(guid: String): AccountOperation {
  return this.first { it.guid() == guid }
}