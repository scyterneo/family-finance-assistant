package com.scyele.familyfinanceassistant.ui.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.ExpenseHistoryItemBinding
import com.scyele.familyfinanceassistant.databinding.IncomeHistoryItemBinding
import com.scyele.familyfinanceassistant.databinding.TransferHistoryItemBinding
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel.*

class HistoryAdapter(
  val clickEvent: (String) -> Unit
) : ListAdapter<OperationHistoryModel, HistoryAdapter.BaseHistoryViewHolder>(
  HistoryModelDiff()
) {

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): BaseHistoryViewHolder {
    when (viewType) {
      TYPE_INCOME -> {
        val binding: IncomeHistoryItemBinding = DataBindingUtil.inflate(
          LayoutInflater.from(parent.context),
          R.layout.income_history_item, parent, false
        )
        return IncomeViewHolder(binding)
      }
      TYPE_EXPENSE -> {
        val binding: ExpenseHistoryItemBinding = DataBindingUtil.inflate(
          LayoutInflater.from(parent.context),
          R.layout.expense_history_item, parent, false
        )
        return ExpenseViewHolder(binding)
      }
      TYPE_TRANSFER -> {
        val binding: TransferHistoryItemBinding = DataBindingUtil.inflate(
          LayoutInflater.from(parent.context),
          R.layout.transfer_history_item, parent, false
        )
        return TransferViewHolder(binding)
      }
      else -> throw IllegalArgumentException("Unexpected View Type")
    }
  }

  override fun onBindViewHolder(
    holder: BaseHistoryViewHolder,
    position: Int
  ) {
    holder.bind(getItem(position))
  }

  override fun getItemViewType(position: Int): Int {
    return when (getItem(position)) {
      is IncomeHistoryModel -> TYPE_INCOME
      is ExpenseHistoryModel -> TYPE_EXPENSE
      is TransferHistoryModel -> TYPE_TRANSFER
    }
  }

  abstract inner class BaseHistoryViewHolder(binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(operationHistoryModel: OperationHistoryModel) {
      bindingViewModeSetViewModel(operationHistoryModel)

      itemView.setOnClickListener {
        clickEvent.invoke(operationHistoryModel.guid)
      }
    }

    abstract fun bindingViewModeSetViewModel(operationHistoryModel: OperationHistoryModel)
  }

  inner class TransferViewHolder(
    private val binding: TransferHistoryItemBinding
  ) : BaseHistoryViewHolder(binding) {

    private val viewModel = TransferHistoryViewModel()

    override fun bindingViewModeSetViewModel(operationHistoryModel: OperationHistoryModel) {
      val transferHistoryModel =
        if (operationHistoryModel is TransferHistoryModel) operationHistoryModel else return
      viewModel.bind(transferHistoryModel)
      binding.viewModel = viewModel
    }
  }

  inner class IncomeViewHolder(private val binding: IncomeHistoryItemBinding) :
    BaseHistoryViewHolder(binding) {
    private val viewModel = IncomeHistoryViewModel()

    override fun bindingViewModeSetViewModel(operationHistoryModel: OperationHistoryModel) {
      val incomeHistoryModel =
        if (operationHistoryModel is IncomeHistoryModel) operationHistoryModel else return
      viewModel.bind(incomeHistoryModel)
      binding.viewModel = viewModel
    }
  }

  inner class ExpenseViewHolder(private val binding: ExpenseHistoryItemBinding) :
    BaseHistoryViewHolder(binding) {
    private val viewModel = ExpenseHistoryViewModel()

    override fun bindingViewModeSetViewModel(operationHistoryModel: OperationHistoryModel) {
      val expenseHistoryModel =
        if (operationHistoryModel is ExpenseHistoryModel) operationHistoryModel else return
      viewModel.bind(expenseHistoryModel)
      binding.viewModel = viewModel
    }
  }

  companion object {
    private const val TYPE_INCOME = 0
    private const val TYPE_EXPENSE = 1
    private const val TYPE_TRANSFER = 2
  }
}