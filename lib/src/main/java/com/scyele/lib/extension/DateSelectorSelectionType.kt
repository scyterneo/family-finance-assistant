package com.scyele.lib.extension

import java.util.*

enum class DateSelectorSelectionType(
  val position: Int,
  val moveType: Int,
  val moveAmplifier: Int = 1
) {
  DAY(0, Calendar.DATE),
  WEEK(1, Calendar.DATE, 7),
  MONTH(2, Calendar.MONTH),
  YEAR(3, Calendar.YEAR),
  ALL(4, Calendar.DATE, 0);

  companion object {

    @JvmStatic
    fun getByPosition(position: Int): DateSelectorSelectionType {
      return when (position) {
        1 -> WEEK
        2 -> MONTH
        3 -> YEAR
        4 -> ALL
        else -> DAY
      }
    }
  }
}