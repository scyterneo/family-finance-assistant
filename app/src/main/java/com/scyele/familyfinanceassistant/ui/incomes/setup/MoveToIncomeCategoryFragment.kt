package com.scyele.familyfinanceassistant.ui.incomes.setup

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.move.MoveToCategoryFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class MoveToIncomeCategoryFragment : MoveToCategoryFragment<
    IncomeCategory,
    MoveToIncomeCategoryFragmentViewModel,
    FragmentViewModelFactory>() {
  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<MoveToIncomeCategoryFragmentViewModel> =
    MoveToIncomeCategoryFragmentViewModel::class.java

  override fun transformToVariants(element: IncomeCategory): String {
    return element.name
  }
}