package com.scyele.familyfinanceassistant.ui.expences.edit

import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.repository.ExpenseEditRepository
import com.scyele.familyfinanceassistant.repository.ProgressRepository
import com.scyele.familyfinanceassistant.ui.DefaultMainRepositoryTest
import com.scyele.familyfinanceassistant.ui.operation.edit.OperationEditFragmentViewModel
import com.scyele.lib.SharedPreferencesHelper
import io.mockk.mockk
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test


class ExpenseEditFragmentViewModelTest : DefaultMainRepositoryTest() {

  private val editExpenseRepository = ExpenseEditRepository()
  private val progressRepository = ProgressRepository(SharedPreferencesHelper(mockk()))

  lateinit var testingVM: ExpenseEditFragmentViewModel

  private var state: OperationEditFragmentViewModel.State? = null


  @Nested
  inner class InitState {
    init {
      testingVM = ExpenseEditFragmentViewModel(getMain(), editExpenseRepository, progressRepository)
      testingVM.state().observeForever { state = it }
    }

    @Test
    fun isRecordDeletable() {
      assert(testingVM.hasInitialOperation().value == false)
    }
  }

  @Nested
  inner class ExpensePreSet {

    init {
      editExpenseRepository.operation.value =
        Expense("guid", "0", "accountCategory1", "expenseCategory1")
      testingVM = ExpenseEditFragmentViewModel(getMain(), editExpenseRepository, progressRepository)
      testingVM.state().observeForever { state = it }
    }

    @Test
    fun isRecordDeletable() {
      assert(testingVM.hasInitialOperation().value == true)
    }
  }

  @Nested
  inner class ExpenseNotSet {

    init {
      testingVM = ExpenseEditFragmentViewModel(getMain(), editExpenseRepository, progressRepository)
      testingVM.state().observeForever { state = it }
    }

    @Test
    fun isRecordDeletable() {
      assert(testingVM.hasInitialOperation().value == false)
    }
  }
}