package com.scyele.familyfinanceassistant.ui.history

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel
import com.scyele.lib.extension.display

class TransferHistoryViewModel : HistoryViewModel() {
  private val accountCategoryToName = MutableLiveData<String>()
  private val accountCategoryFromName = MutableLiveData<String>()
  private val transferComment = MutableLiveData<String?>()
  private val transferSum = MutableLiveData<String>()
  private val transferDate = MutableLiveData<String>()

  fun bind(transferHistoryModel: OperationHistoryModel.TransferHistoryModel) {
    transferSum.value = transferHistoryModel.sum
    accountCategoryToName.value = transferHistoryModel.accountCategoryToName
    accountCategoryFromName.value = transferHistoryModel.accountCategoryFromName
    transferComment.value = transferHistoryModel.comment
    transferDate.value = transferHistoryModel.date.display()
  }

  fun getAccountCategoryToName() = accountCategoryToName
  fun getAccountCategoryFromName() = accountCategoryFromName
  fun getTransferSum() = transferSum
  fun getTransferDate() = transferDate
  fun getTransferComment() = transferComment
}