package com.scyele.familyfinanceassistant.utils.extension

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.db.Tables
import com.scyele.familyfinanceassistant.entity.*
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

fun <T> FirebaseDatabase.getList(
  tableName: String,
  valueType: Class<T>,
  onItemsReceived: (MutableList<T>) -> Unit,
  onErrorAction: ((message: String) -> Unit)? = null
) {
  onDataChange(
    tableName,
    {
      val items = mutableListOf<T>()
      for (child in it.children) {
        val item = child.get(valueType)
        if (item != null) {
          items.add(item)
        }
      }
      onItemsReceived(items)
    },
    {
      onErrorAction?.let { onErrorAction ->
        onErrorAction(it)
      }
    }
  )
}


suspend fun <T> FirebaseDatabase.getListAsFlow(
  tableName: String,
  valueType: Class<T>
): Flow<List<T>> =
  callbackFlow {
    getList(
      tableName,
      valueType,
      { trySend(it) },
      { trySend(emptyList<T>()) }
    )

    awaitClose { }
  }


// TODO use this for Login Activity
fun <T> FirebaseDatabase.getOnce(
  tableName: String,
  valueType: Class<T>
) {
  getReference(getLocationId()).child(tableName)
    .addListenerForSingleValueEvent(object : ValueEventListener {
      override fun onCancelled(p0: DatabaseError) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
      }

      override fun onDataChange(p0: DataSnapshot) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
      }
    })
}

/**
 * Method gets all T items from FBDB
 */


fun <T> FirebaseDatabase.getList(
  tableName: String,
  valueType: Class<T>
): Observable<MutableList<T>> {
  return Observable.create { emitter ->
    getList(
      tableName,
      valueType,
      { emitter.onNext(it) },
      { emitter.onError(Throwable(it)) }
    )
  }
}

/**
 * Method gets all T items from FBDB
 */


fun <T> FirebaseDatabase.getList(
  tableName: String,
  valueType: Class<T>,
  onItemsReceived: (MutableList<T>) -> Unit
): Disposable {
  val observable: Observable<MutableList<T>> = Observable.create { emitter ->
    getList(tableName, valueType, { emitter.onNext(it) }, { emitter.onError(Throwable(it)) })
  }
  return observable.main(Consumer { onItemsReceived(it) })
}

/**
 * Method gets single item from FBDB as a Disposable object
 */


fun <T> FirebaseDatabase.getSingle(
  tableName: String,
  valueType: Class<T>,
  onItemReceived: (T) -> Unit
): Disposable {
  val observable: Observable<T> = Observable.create { emitter ->
    getSingle(tableName, valueType, { emitter.onNext(it) }, { emitter.onError(Throwable(it)) })
  }
  return observable.main(Consumer { onItemReceived(it) })
}



fun FirebaseDatabase.getLocation(
  onLocationReceived: (Location) -> Unit,
  onNoLocationReceived: ((throwable: Throwable) -> Unit)? = null
): Disposable {
  val observable: Observable<Location> = Observable.create { emitter ->
    onDataChange(
      Tables.LOCATION,
      {
        val location = it.get(Location::class.java)
        if (location != null) {
          emitter.onNext(location)
        } else {
          emitter.onError(Throwable("Unable to obtain Location"))
        }
      },
      { emitter.onError(Throwable(it)) })
  }
  return observable.main(
    Consumer { onLocationReceived.invoke(it) },
    Consumer { onNoLocationReceived?.invoke(it) })
}



fun FirebaseDatabase.joinLocation(
  locationId: String,
  password: String,
  user: User,
  action: (isConnected: Boolean) -> Unit
) {
  getReference(locationId).child(Tables.LOCATION).child(Tables.PASSWORD)
    .addListenerForSingleValueEvent(object : ValueEventListener {
      override fun onCancelled(error: DatabaseError) {
        Log.d("joinLocation", "onCancelled $error")
        action.invoke(false)
      }

      override fun onDataChange(snapshot: DataSnapshot) {
        Log.d("joinLocation", "onDataChange $snapshot")
        val locationPassword = snapshot.getValue(String::class.java)
        val isCorrectPassword = password == locationPassword
        if (isCorrectPassword) {
          setSharingLocation(locationId)
          AssistantApplication.instance.locationId = locationId
          setUser(user)
        }
        action.invoke(isCorrectPassword)
      }
    })
}

/**
 * Method updates existing password
 */


fun FirebaseDatabase.setPassword(password: String) {
  getReference(getLocationId())
    .child(Tables.LOCATION)
    .child(Tables.PASSWORD)
    .setValue(password)
}

/**
 * Method updates sharing location id
 */


fun FirebaseDatabase.setSharingLocation(sharingLocation: String) {
  Log.d("firebase", "setSharingLocation $sharingLocation")
  getReference(getLocationId())
    .child(Tables.LOCATION)
    .child(Tables.SHARING_LOCATION)
    .setValue(sharingLocation)
}

/**
 * Method add email to used shared location
 */


fun FirebaseDatabase.setUser(user: User) {
  Log.d("firebase", "setUser $user")
  getReference(getLocationId())
    .child(Tables.LOCATION)
    .child(Tables.USERS)
    .child(user.uid)
    .setValue(user.mail)
}

/**
 * Method updates existing or adds new AccountCategory to DB
 */


fun FirebaseDatabase.setAccountCategory(accountCategory: AccountCategory) {
  getReference(getLocationId())
    .child(Tables.ACCOUNT_CATEGORY_TABLE_NAME)
    .child(accountCategory.guid)
    .setValue(accountCategory)
}

/**
 * Method updates existing or adds new IncomeCategory to DB
 */


fun FirebaseDatabase.setIncomeCategory(incomeCategory: IncomeCategory) {
  getReference(getLocationId())
    .child(Tables.INCOME_CATEGORY_TABLE_NAME)
    .child(incomeCategory.guid)
    .setValue(incomeCategory)
}

/**
 * Method updates existing or adds new ExpenseCategory to DB
 */


fun FirebaseDatabase.setExpenseCategory(expenseCategory: ExpenseCategory) {
  getReference(getLocationId())
    .child(Tables.EXPENSE_CATEGORY_TABLE_NAME)
    .child(expenseCategory.guid)
    .setValue(expenseCategory)
}

/**
 * Method updates existing or adds new Transfer to DB
 */


fun FirebaseDatabase.setTransfer(transfer: Transfer) {
  getReference(getLocationId())
    .child(Tables.TRANSFER_TABLE_NAME)
    .child(transfer.guid)
    .setValue(transfer)
}



fun FirebaseDatabase.removeTransfer(transfer: Transfer) {
  getReference(getLocationId())
    .child(Tables.TRANSFER_TABLE_NAME)
    .child(transfer.guid)
    .setValue(null)
}

/**
 * Method updates existing or adds new Income to DB
 */


fun FirebaseDatabase.setIncome(income: Income) {
  getReference(getLocationId())
    .child(Tables.INCOME_TABLE_NAME)
    .child(income.guid)
    .setValue(income)
}



fun FirebaseDatabase.removeIncome(income: Income) {
  getReference(getLocationId())
    .child(Tables.INCOME_TABLE_NAME)
    .child(income.guid)
    .setValue(null)
}



fun FirebaseDatabase.removeIncomeCategory(incomeCategory: IncomeCategory) {
  getReference(getLocationId())
    .child(Tables.INCOME_CATEGORY_TABLE_NAME)
    .child(incomeCategory.guid)
    .setValue(null)
}

/**
 * Method updates existing or adds new Expense to DB
 */


fun FirebaseDatabase.setExpense(expense: Expense) {
  getReference(getLocationId())
    .child(Tables.EXPENSE_TABLE_NAME)
    .child(expense.guid)
    .setValue(expense)
}



fun FirebaseDatabase.removeExpense(expense: Expense) {
  getReference(getLocationId())
    .child(Tables.EXPENSE_TABLE_NAME)
    .child(expense.guid)
    .setValue(null)
}



fun FirebaseDatabase.removeExpenseCategory(expenseCategory: ExpenseCategory) {
  getReference(getLocationId())
    .child(Tables.EXPENSE_CATEGORY_TABLE_NAME)
    .child(expenseCategory.guid)
    .setValue(null)
}



fun FirebaseDatabase.removeAccountCategory(accountCategory: AccountCategory) {
  getReference(getLocationId())
    .child(Tables.ACCOUNT_CATEGORY_TABLE_NAME)
    .child(accountCategory.guid)
    .setValue(null)
}

/**
 * Method simplifies usage of addValueEventListener() method with default handling on onCancelled event
 */
private fun FirebaseDatabase.onDataChange(
  path: String,
  onDataChangeAction: (dataSnapshot: DataSnapshot) -> Unit,
  onErrorAction: ((message: String) -> Unit)? = null
) {
  getReference(getLocationId()).child(path).addValueEventListener(object : ValueEventListener {
    override fun onCancelled(databaseError: DatabaseError) {
      Log.w("FBDB.onDataChange", databaseError.details)
      onErrorAction?.invoke(databaseError.details)
    }

    override fun onDataChange(dataSnapshot: DataSnapshot) {
      onDataChangeAction.invoke(dataSnapshot)
    }
  })
}

/**
 * Method covers DataSnapshot getValue() method with exception handling
 */
private fun <T> DataSnapshot.get(valueType: Class<T>): T? {
  return try {
    getValue(valueType)
  } catch (exception: Exception) {
    Log.w("DataSnapshot.get", exception.toString())
    null
  }
}



private fun <T> FirebaseDatabase.getSingle(
  tableName: String,
  valueType: Class<T>,
  onItemsReceived: (T) -> Unit,
  onErrorAction: ((message: String) -> Unit)? = null
) {
  onDataChange(
    tableName,
    {
      for (child in it.children) {
        val item = child.get(valueType)
        if (item != null) {
          onItemsReceived.invoke(item)
        }
      }
    },
    {
      onErrorAction?.invoke(it)
    }
  )
}