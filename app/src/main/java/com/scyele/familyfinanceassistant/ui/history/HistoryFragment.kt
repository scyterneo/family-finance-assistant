package com.scyele.familyfinanceassistant.ui.history

import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.HistoryFragmentBinding
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.ui.accounts.setup.AccountSetupFragment
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferFragment
import com.scyele.familyfinanceassistant.ui.base.BaseFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.expences.edit.ExpenseEditFragment
import com.scyele.familyfinanceassistant.ui.expences.setup.ExpenseCategorySetupFragment
import com.scyele.familyfinanceassistant.ui.incomes.edit.IncomeEditFragment
import com.scyele.familyfinanceassistant.ui.incomes.setup.IncomeCategorySetupFragment
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.lib.extension.DateSelectorSelectionType
import com.scyele.lib.extension.initSpinner
import kotlinx.android.synthetic.main.history_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*




class HistoryFragment : BaseFragment<
    HistoryFragmentViewModel,
    HistoryFragmentBinding,
    HistoryFragmentViewModel.State,
    FragmentViewModelFactory>() {

  private lateinit var historyAdapter: HistoryAdapter

  private lateinit var operationCategoriesAdapter: ArrayAdapter<String>
  private lateinit var accountCategoriesAdapter: ArrayAdapter<String>

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getLayoutId(): Int = R.layout.history_fragment

  override fun bindingViewModeSetViewModel() {
    binding.viewModel = viewModel

    viewModel.getOperationHistoryModels().observe(viewLifecycleOwner, Observer {
      historyAdapter.submitList(it)
    })

    viewModel.getOperationCategorySpinnerModel().observe(viewLifecycleOwner, Observer {
      val namesList: MutableList<String> = it.modelsNames.toMutableList()
      namesList.add(getString(R.string.all))

      binding.spinnerOperationCategory.onItemSelectedListener = null

      operationCategoriesAdapter =
        ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, namesList)
      operationCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

      initSpinner(
        binding.spinnerOperationCategory,
        operationCategoriesAdapter,
        it.selectedPosition,
        viewModel.onOperationCategorySelected
      )
    })
    viewModel.getAccountCategorySpinnerModel().observe(viewLifecycleOwner, Observer {
      val namesList: MutableList<String> = it.modelsNames.toMutableList()
      namesList.add(getString(R.string.all))

      binding.spinnerAccountCategory.onItemSelectedListener = null

      accountCategoriesAdapter =
        ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, namesList)
      accountCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

      initSpinner(
        binding.spinnerAccountCategory,
        accountCategoriesAdapter,
        it.selectedPosition,
        viewModel.onAccountCategorySelected
      )
    })

    date_selector.setDateSelectionChangedListener { date: Date, selectionType: DateSelectorSelectionType ->
      viewModel.onDateSelectionChanged(date, selectionType)
    }
  }

  override fun handleEvents(state: HistoryFragmentViewModel.State) {
    state.editOperationEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(
        when (it) {
          OperationType.INCOME -> IncomeEditFragment()
          OperationType.EXPENSE -> ExpenseEditFragment()
        },
        activity
      )
    }
    state.editTransferEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(
        TransferFragment(),
        activity
      )
    }
    state.editOperationCategoryEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(
        when (it) {
          OperationType.INCOME -> IncomeCategorySetupFragment()
          OperationType.EXPENSE -> ExpenseCategorySetupFragment()
        },
        activity
      )
    }
    state.editAccountCategoryEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(AccountSetupFragment(), activity)
    }
  }

  override fun getModelClass(): Class<HistoryFragmentViewModel> {
    return HistoryFragmentViewModel::class.java
  }

  override fun setupView() {
    binding.operationsList.layoutManager =
      LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
    historyAdapter = HistoryAdapter { viewModel.onOperationClicked(it) }
    binding.operationsList.adapter = historyAdapter

    context?.let {
      operationCategoriesAdapter = ArrayAdapter(it, android.R.layout.simple_spinner_item)
      operationCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
      binding.spinnerOperationCategory.adapter = operationCategoriesAdapter

      accountCategoriesAdapter = ArrayAdapter(it, android.R.layout.simple_spinner_item)
      accountCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
      binding.spinnerAccountCategory.adapter = accountCategoriesAdapter
    }
  }

  override fun tag() = "HistoryFragment"
}