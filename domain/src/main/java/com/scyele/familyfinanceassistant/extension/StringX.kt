package com.scyele.familyfinanceassistant.extension

import java.math.BigDecimal

fun String.value(): BigDecimal = try {
  BigDecimal(this)
} catch (e: NumberFormatException) {
  BigDecimal.ZERO
}