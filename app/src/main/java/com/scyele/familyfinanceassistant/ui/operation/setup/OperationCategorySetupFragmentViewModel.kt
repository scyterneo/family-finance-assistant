package com.scyele.familyfinanceassistant.ui.operation.setup

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.MoveToRepository
import com.scyele.familyfinanceassistant.repository.OperationCategorySetupRepository
import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.utils.Guid
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



abstract class OperationCategorySetupFragmentViewModel<Action, Category, Summary>(
  private val operationCategorySetupRepository: OperationCategorySetupRepository,
  protected val mainRepository: MainRepository,
  private val moveToRepository: MoveToRepository
) :
  BaseScreenVM<OperationCategorySetupFragmentViewModel.State>()
    where Action : Operation,
          Category : OperationCategory,
          Summary : OperationSummary {

  private var initialModel: OperationCategory? = null

  private val operations: SafeMediatorLiveData<List<Operation>> = SafeMediatorLiveData(listOf())
  private val childOperationCategories: SafeMediatorLiveData<List<OperationCategory>> =
    SafeMediatorLiveData(listOf())
  private val isParentAvailable = MutableLiveData<Boolean>(true)

  override fun initState() = State()

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(operationCategorySetupRepository.operationCategory) {
      initialModel = it
      state.value = state.value.copy(
        name = Event(it?.name() ?: ""),
        parentCategory = Event(getParentForOperationCategory(it)),
        isParent = Event(it?.isParentCategory() ?: false)
      )
    }
    mediatorState.addSource(getOperationCategories()) { operationCategories ->
      val parentOperationCategories = operationCategories
        .filter { operationCategory -> operationCategory.isParentCategory() }
        .sortedBy { operationCategory -> operationCategory.name() }
      childOperationCategories.value =
        operationCategories.filter { initialModel != null && it.parentGuid() == initialModel!!.guid() }
      state.value = state.value.copy(
        allCategories = Event(parentOperationCategories),
        parentCategory = Event(
          getParentForOperationCategory(initialModel, parentOperationCategories)
        )
      )
    }
    mediatorState.addSource(getOperations()) {
      operations.value = it.filter { operation ->
        operation.operationCategoryGuid() == initialModel?.guid()
      }
    }
    mediatorState.addSource(moveToRepository.moveCompleted) {
      it?.let {
        moveToRepository.reinit()
      }
    }
    return mediatorState
  }

  override fun onCleared() {
    super.onCleared()
    operationCategorySetupRepository.reinit()
  }

  abstract fun getOperations(): MutableLiveData<List<Action>>

  abstract fun getOperationCategories(): MutableLiveData<List<Category>>

  abstract fun getTypedEvent(): Event<OperationType>

  protected abstract fun createNewOperationCategory(
    name: String,
    isParent: Boolean = false,
    parentGuid: String? = null,
    guid: String = Guid.new()
  ): OperationCategory

  fun isParentAvailable() = isParentAvailable

  fun onDismiss() {
    operationCategorySetupRepository.reinit()
  }

  fun saveModelAttempt() {
    val error = checkModelForErrors()
    if (error == null) {
      saveModel()
      state.value = state.value.copy(setupComplete = SimpleEvent())
    } else {
      state.value = state.value.copy(saveError = Event(error))
    }
  }

  fun isRecordDeletable() = MutableLiveData<Boolean>().apply { value = initialModel != null }

  fun onTextInputted(string: String) {
    state.value = state.value.copy(name = Event(string, false))
  }

  fun deleteModelAttempt() {
    initialModel?.let {
      val error = checkModelForDelete()
      if (error == null) {
        mainRepository.removeOperationCategory(it)
        state.value = state.value.copy(setupComplete = SimpleEvent())
        return@deleteModelAttempt
      }
      when (error) {
        OperationCategoryDeleteError.OPERATIONS_EXIST -> {
          state.value = state.value.copy(checkForDeleting = Event(operations.value.size))
          return@let
        }
        OperationCategoryDeleteError.CHILD_CATEGORIES_EXIST -> {
          state.value = state.value.copy(deleteError = Event(error))
          return@let
        }
      }
    }
  }

  fun isParentCategory(checked: Boolean = false) {
    state.value = state.value.copy(isParent = Event(checked, false))
  }

  fun onParentCategorySelected(position: Int) {
    val operationCategory = getOperationCategoryByPosition(position)
    state.value = state.value.copy(parentCategory = Event(operationCategory, false))
  }

  fun changeCategoryRequired() {
    initialModel?.let {
      moveToRepository.operationCategory = initialModel
      state.value = state.value.copy(changeCategoryRequired = getTypedEvent())
    }
  }

  fun deleteAllRelated() {
    for (operation in operations.value) {
      mainRepository.removeOperation(operation)
    }
    mainRepository.removeOperationCategory(initialModel!!)
    state.value = state.value.copy(setupComplete = SimpleEvent())
  }

  private fun checkModelForDelete(): OperationCategoryDeleteError? {
    if (childOperationCategories.value.isNotEmpty()) {
      return OperationCategoryDeleteError.CHILD_CATEGORIES_EXIST
    }
    if (operations.value.isNotEmpty()) {
      return OperationCategoryDeleteError.OPERATIONS_EXIST
    }
    return null
  }

  private fun checkModelForErrors(): OperationCategorySaveError? {
    if (state.value.name.peekContent().isEmpty()) {
      return OperationCategorySaveError.EMPTY_NAME
    }
    if (!state.value.isParent.peekContent() && state.value.parentCategory.peekContent() == null) {
      return OperationCategorySaveError.EMPTY_PARENT
    }
    initialModel?.let {
      if (it.isParentCategory() != state.value.isParent.peekContent()) {
        return OperationCategorySaveError.PARENT_COULD_NOT_BE_CHILD_AND_VISE_VERSA
      }
    }
    return null
  }

  private fun saveModel() {
    val parentCategoryGuid = state.value.parentCategory.peekContent()?.guid()
    mainRepository.setOperationCategory(
      createNewOperationCategory(
        state.value.name.peekContent(),
        state.value.isParent.peekContent(),
        parentCategoryGuid,
        initialModel?.guid() ?: Guid.new()
      )
    )
  }

  private fun getOperationCategoryByPosition(position: Int): OperationCategory? {
    val categoriesSize = state.value.allCategories.peekContent().size
    return if (position in 0 until categoriesSize)
      state.value.allCategories.peekContent()[position]
    else
      null
  }

  private fun getParentForOperationCategory(
    operationCategory: OperationCategory?,
    allCategories: List<OperationCategory> = state.value.allCategories.peekContent()
  ): OperationCategory? {
    return if (operationCategory == null)
      null
    else
      allCategories.firstOrNull { it.guid() == operationCategory.parentGuid() }
  }

  data class State(
    val name: Event<String> = Event(""),
    val allCategories: Event<List<OperationCategory>> = Event(emptyList()),
    val isParent: Event<Boolean> = Event(false),
    val parentCategory: Event<OperationCategory?> = Event(null),
    val setupComplete: SimpleEvent? = null,
    val checkForDeleting: Event<Int>? = null,
    val saveError: Event<OperationCategorySaveError>? = null,
    val deleteError: Event<OperationCategoryDeleteError>? = null,
    val changeCategoryRequired: Event<OperationType>? = null
  )
}