package com.scyele.familyfinanceassistant.ui.operation.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.extension.value
import com.scyele.familyfinanceassistant.repository.*
import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.summary.createNewSummary
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.LoadingParts
import com.scyele.familyfinanceassistant.ui.base.LoadingParts.*
import com.scyele.familyfinanceassistant.ui.operation.main.OperationsScreenInit.Loaded
import com.scyele.familyfinanceassistant.ui.operation.main.OperationsScreenInit.Loading
import com.scyele.familyfinanceassistant.utils.Aggregator.createOperationSummaries
import com.scyele.lib.coroutines.background
import com.scyele.lib.extension.DateSelectorSelectionType
import com.scyele.lib.extension.isDateInRange
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*


abstract class OperationsFragmentViewModel<Action, Category, Summary>(
  protected val mainRepository: MainRepository,
  private val operationEditRepository: OperationEditRepository<Action, Category>,
  private val operationCategorySetupRepository: OperationCategorySetupRepository,
  private val accountCategorySetupRepository: AccountCategorySetupRepository,
  private val progressRepository: ProgressRepository
) : BaseScreenVM<OperationsFragmentViewModel.State>()
    where Action : Operation,
          Category : OperationCategory,
          Summary : OperationSummary {

  private var operations: List<Action> = listOf()
  private var operationCategories: List<Category> = listOf()
  private var accountCategories: List<AccountCategory> = listOf()
  private var operationSummaries: List<Summary> = listOf()
  private var selectedDate: Date = Date()
  private var selectedSelectionType: DateSelectorSelectionType = DateSelectorSelectionType.DAY

  private val totalSum = MutableLiveData<String>()

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(mainRepository.accountCategories) {
      accountCategories = it
      if (state.value.init.peekContent() is Loading) {
        updateLoading()
      }
    }
    mediatorState.addSource(getOperations()) { allOperations ->
      operations = allOperations
      onFilterOperationsRequired(allOperations)
    }
    mediatorState.addSource(getOperationCategories()) {
      operationCategories = it
      updateData()
    }

    return mediatorState
  }

  override fun initState() = State()

  abstract fun getOperations(): MutableLiveData<List<Action>>

  abstract fun getOperationCategories(): MutableLiveData<List<Category>>

  abstract fun getTypedEvent(): Event<OperationType>?

  fun onOperationCategoryClicked(guid: String) {
    val operationSummary = findOperationSummary(guid, operationSummaries)
    if (operationSummary == null) {
      updateData()
      return
    }
    if (operationSummary.operationCategory().isParentCategory()) {
      operationSummary.setCollapsedSummary(!operationSummary.isCollapsedSummary())
      progressRepository.setCollapsed(guid, operationSummary.isCollapsedSummary())
      displaySummaries()
    } else {
      addOperation(operationSummary)
    }
  }

  fun onOperationCategoryLongClicked(guid: String) {
    val operationSummary = findOperationSummary(guid, operationSummaries)
    if (operationSummary == null) {
      updateData()
      return
    }
    showOperationCategoryHistory(operationSummary)
  }

  fun onAddOperationCategoryClicked() {
    operationCategorySetupRepository.reinit()
    state.value = state.value.copy(editOperationCategoryEvent = getTypedEvent())
  }

  fun getTotalSum() = totalSum

  fun onDateSelectionChanged(date: Date, selectionType: DateSelectorSelectionType) {
    selectedDate = date
    selectedSelectionType = selectionType
    val allOperations = getOperations().value ?: return
    onFilterOperationsRequired(allOperations)
  }

  private fun onFilterOperationsRequired(allOperations: List<Action>) {
    operations =
      allOperations.filter { it.date().isDateInRange(selectedDate, selectedSelectionType) }
    updateData()
  }

  private fun updateData() {
    viewModelScope.launch {
      operationSummaries = background { createSummaries() }
      if (operationSummaries.isEmpty() && state.value.init.peekContent() is Loading) {
        updateLoading()
      } else {
        displaySummaries()
      }
    }
  }

  private fun displaySummaries() {
    viewModelScope.launch {
      val calculatedSum = background { calculateSum(operations).toString() }
      val operationSummariesToDisplay = background { getOperationSummariesForDisplay() }
      state.value = state.value.copy(init = Event(Loaded(operationSummariesToDisplay)))
      totalSum.value = calculatedSum
    }
  }

  private fun createSummaries(): List<Summary> {
    val summaries = createOperationSummaries(operations, operationCategories)
    for (summary in summaries) {
      updateOperationSummaryCollapsing(summary as Summary)
    }
    return summaries as List<Summary>
  }

  private fun updateOperationSummaryCollapsing(operationSummary: Summary) {
    operationSummary.setCollapsedSummary(
      progressRepository.getCollapsed(operationSummary.operationCategory().guid())
    )
    for (childOperationSummary in operationSummary.childOperationSummaries()) {
      updateOperationSummaryCollapsing(childOperationSummary as Summary)
    }
  }

  private fun getOperationSummariesForDisplay(): List<Summary> {
    val operationSummaryViewModels: MutableList<Summary> = mutableListOf()
    for (operationSummary in operationSummaries) {
      calculateSummary(operationSummary)

      addModels(operationSummary, operationSummaryViewModels)
    }

    return operationSummaryViewModels
  }

  private fun addModels(
    operationSummary: Summary,
    operationSummaryViewModels: MutableList<Summary>
  ) {
    operationSummaryViewModels.add(createNewSummary(operationSummary) as Summary)
    if (!operationSummary.isCollapsedSummary()) {
      for (childOperationSummary in operationSummary.childOperationSummaries()) {
        addModels(childOperationSummary as Summary, operationSummaryViewModels)
      }
    }
  }

  private fun calculateSummary(operationSummary: Summary) {
    if (operationSummary.operationCategory().isParentCategory()) {
      for (childOperationSummary in operationSummary.childOperationSummaries()) {
        calculateSummary(childOperationSummary as Summary)
      }
      operationSummary.setOperationSum(operationSummary.childOperationSummaries()
        .map { it.operationSum() }
        .fold(BigDecimal.ZERO, BigDecimal::add))
    } else {
      operationSummary.calculateSum()
    }
  }

  private fun findOperationSummary(
    guid: String,
    summariesToLookIn: List<Summary>
  ): Summary? {
    val operationSummary = summariesToLookIn.firstOrNull { it.operationCategory().guid() == guid }
    if (operationSummary != null) return operationSummary
    for (summaryToLookIn in summariesToLookIn) {
      val childOperationSummary =
        findOperationSummary(
          guid,
          summaryToLookIn.childOperationSummaries() as List<Summary>
        )
      if (childOperationSummary != null) return childOperationSummary
    }
    return null
  }

  private fun findChildOperationCategories(
    parentOperationCategoryGuid: String,
    operationSummaryModels: List<Summary>
  ) =
    operationSummaryModels.filter { it.operationCategory().parentGuid() == parentOperationCategoryGuid }

  private fun addOperation(operationSummaryModel: Summary) {
    operationEditRepository.operationCategory.value = operationSummaryModel.operationCategory()
    state.value = state.value.copy(editOperationEvent = getTypedEvent())
  }

  private fun showOperationCategoryHistory(operationSummaryModel: Summary) {
    operationCategorySetupRepository.operationCategory.value =
      operationSummaryModel.operationCategory()
    accountCategorySetupRepository.reinit()
    state.value = state.value.copy(showOperationCategoryHistoryEvent = getTypedEvent())
  }

  private fun calculateSum(operations: List<Action>): BigDecimal {
    var sum = BigDecimal.ZERO
    for (operation in operations) {
      sum += operation.sum().value()
    }
    return sum
  }

  private fun updateLoading() {
    val currentLoadingList: MutableList<LoadingParts> = mutableListOf()
    if (accountCategories.isEmpty()) {
      currentLoadingList.add(ACCOUNT_CATEGORIES)
    }
    if (operations.isEmpty()) {
      currentLoadingList.add(OPERATIONS)
    }
    if (operationCategories.isEmpty()) {
      currentLoadingList.add(OPERATION_CATEGORIES)
    }
    state.value = state.value.copy(init = Event(Loading(currentLoadingList)))
  }

  data class State(
    val init: Event<OperationsScreenInit> = Event(
      Loading(listOf(ACCOUNT_CATEGORIES, OPERATIONS, OPERATION_CATEGORIES))
    ),
    val editOperationEvent: Event<OperationType>? = null,
    val editOperationCategoryEvent: Event<OperationType>? = null,
    val showOperationCategoryHistoryEvent: Event<OperationType>? = null
  )
}