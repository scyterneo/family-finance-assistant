package com.scyele.familyfinanceassistant.ui.expences.edit

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.summary.ExpenseSummary
import com.scyele.familyfinanceassistant.ui.operation.edit.OperationEditFragment

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class ExpenseEditFragment : OperationEditFragment<
    Expense, ExpenseCategory, ExpenseSummary, ExpenseEditFragmentViewModel>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<ExpenseEditFragmentViewModel> =
    ExpenseEditFragmentViewModel::class.java

  override fun tag() = "EditExpenseFragment"

  override fun getHeaderText(): String {
    return getString(R.string.expense)
  }
}