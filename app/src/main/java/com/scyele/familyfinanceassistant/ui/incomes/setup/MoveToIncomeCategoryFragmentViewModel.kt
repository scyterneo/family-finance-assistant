package com.scyele.familyfinanceassistant.ui.incomes.setup

import androidx.lifecycle.LiveData
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.MoveToRepository
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.ui.move.MoveToCategoryFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi


class MoveToIncomeCategoryFragmentViewModel(
  mainRepository: MainRepository,
  moveToRepository: MoveToRepository
) : MoveToCategoryFragmentViewModel<IncomeCategory>(mainRepository, moveToRepository) {

  init {
    currentCategory = moveToRepository.operationCategory as IncomeCategory?
  }

  override fun moveToModel() {
    val incomesToMove = mainRepository.incomes.value?.filter {
      it.incomeCategoryGuid == currentCategory?.guid
    } ?: listOf()
    for (income in incomesToMove) {
      income.incomeCategoryGuid = targetCategory!!.guid
      mainRepository.setIncome(income)
    }
    moveToRepository.moveCompleted.value = true
    state.value = state.value.copy(moveCompleted = SimpleEvent())
  }

  override fun getAllCategoriesSource(): LiveData<List<IncomeCategory>> {
    return mainRepository.incomeCategories
  }

  override fun filterElement(category: IncomeCategory): Boolean {
    return !category.isParent && category.guid != currentCategory?.guid
  }

  override fun getCurrentCategoryName(): String {
    return currentCategory?.name ?: ""
  }
}