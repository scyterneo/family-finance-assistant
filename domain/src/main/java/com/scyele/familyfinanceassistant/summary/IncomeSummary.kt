package com.scyele.familyfinanceassistant.summary

import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.entity.Operation
import java.math.BigDecimal

data class IncomeSummary(
  val incomeCategory: IncomeCategory,
  var incomeSum: BigDecimal = BigDecimal.ZERO,
  var incomes: List<Income> = listOf(),
  var childIncomeSummaries: List<IncomeSummary> = listOf(),
  val levelOfDepth: Int = 0,
  var isCollapsed: Boolean = true
) : OperationSummary {
  override fun operationCategory() = incomeCategory

  override fun operationSum() = incomeSum

  override fun setOperationSum(value: BigDecimal) {
    incomeSum = value
  }

  override fun operations() = incomes

  override fun setOperations(value: List<Operation>) {
    incomes = value.filterIsInstance(Income::class.java)
  }

  override fun childOperationSummaries() = childIncomeSummaries

  override fun setChildOperationSummaries(value: List<OperationSummary>) {
    childIncomeSummaries = value.filterIsInstance(IncomeSummary::class.java)
  }

  override fun levelOfDepth() = levelOfDepth

  override fun isCollapsedSummary() = isCollapsed

  override fun setCollapsedSummary(value: Boolean) {
    isCollapsed = value
  }

  constructor(originalModel: IncomeSummary) : this(
    originalModel.incomeCategory,
    originalModel.incomeSum,
    originalModel.incomes,
    originalModel.childIncomeSummaries,
    originalModel.levelOfDepth,
    originalModel.isCollapsed
  )

  override fun calculateSum() {
    incomeSum = incomes
      .map { BigDecimal(it.sum) }
      .fold(BigDecimal.ZERO, BigDecimal::add)
  }
}