package com.scyele.familyfinanceassistant

import android.app.Application
import com.scyele.familyfinanceassistant.di.components.*
import com.scyele.familyfinanceassistant.di.modules.AppModule
import com.scyele.familyfinanceassistant.di.modules.FirebaseModule
import com.scyele.familyfinanceassistant.di.modules.LoginActivityModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

class AssistantApplication : Application() {

  companion object {
    lateinit var instance: AssistantApplication
      private set
  }

  lateinit var appComponent: AppComponent
  lateinit var firebaseComponent: FirebaseComponent

  private var loginActivityComponent: LoginActivityComponent? = null
  var locationId: String = ""

  override fun onCreate() {
    super.onCreate()
    instance = this

    appComponent = DaggerAppComponent.builder()
      .appModule(AppModule(this))
      .build()

    firebaseComponent = DaggerFirebaseComponent.builder()
      .firebaseModule(FirebaseModule(this))
      .build()

    appComponent.inject(this)
  }

  fun loginActivityComponent(): LoginActivityComponent {
    return loginActivityComponent ?: DaggerLoginActivityComponent.builder()
      .loginActivityModule(LoginActivityModule(this))
      .build()
      .also {
        loginActivityComponent = it
      }
  }

  fun releaseLoginActivityComponent() {
    loginActivityComponent = null
  }
}