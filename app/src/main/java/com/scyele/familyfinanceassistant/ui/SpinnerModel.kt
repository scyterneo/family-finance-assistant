package com.scyele.familyfinanceassistant.ui

data class SpinnerModel(val modelsNames: List<String>, val selectedPosition: Int)