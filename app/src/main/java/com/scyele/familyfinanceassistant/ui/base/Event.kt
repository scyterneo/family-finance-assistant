package com.scyele.familyfinanceassistant.ui.base

import com.scyele.familyfinanceassistant.utils.extension.convert
import java.math.BigDecimal
import java.util.*

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class Event<out T>(private val content: T, needToHandle: Boolean = true) {

  var hasBeenHandled = !needToHandle
    private set // Allow external read but not write

  /**
   * Returns the content and prevents its use again.
   */
  fun getContent(): T? = if (hasBeenHandled) {
    null
  } else {
    hasBeenHandled = true
    content
  }

  /**
   * Returns the content, even if it's already been handled.
   */
  fun peekContent(): T = content

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is Event<*>) return false

    if (content != other.content) return false
    if (hasBeenHandled != other.hasBeenHandled) return false

    return true
  }

  override fun hashCode(): Int {
    var result = content?.hashCode() ?: 0
    result = 31 * result + hasBeenHandled.hashCode()
    return result
  }

  override fun toString(): String = "[hasBeenHandled=$hasBeenHandled, content=$content]"
}

class SimpleEvent : Event<Unit>(Unit)

class DataEvent<out T>(content: T) : Event<T>(content)

//TODO make sure, that this code moved to correct place
internal fun sum(string: String?): Event<BigDecimal> {
  val nonNullString = if (string.isNullOrEmpty()) {
    "0"
  } else {
    string
  }
  val value = convert(nonNullString)
  return Event(value)
}

internal fun date(date: Date?) = Event(date ?: Date())