package com.scyele.familyfinanceassistant.utils.extension.data

import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue

class ExpenseExtensionsKtTest {

  @Test
  fun belongsToExpenseCategory() {
    val expense = Expense("2", "accountGuid", "guid1")
    val expenseCategoryParent = ExpenseCategory("parent", true, null, "parentId")
    val expenseCategory2 = ExpenseCategory("child1", false, expenseCategoryParent.guid, "guid1")
    val expenseCategory3 = ExpenseCategory("name", false, expenseCategoryParent.guid, "guid2")
    val expenseCategory4 = ExpenseCategory("name", true, null, "guid3")

    val expenseCategories = listOf<ExpenseCategory>(
      expenseCategoryParent,
      expenseCategory2,
      expenseCategory4,
      expenseCategory3
    )

    assertTrue(expense.belongsToOperationCategory(expenseCategory2, expenseCategories))
    assertFalse(expense.belongsToOperationCategory(expenseCategory3, expenseCategories))
    assertFalse(expense.belongsToOperationCategory(expenseCategory4, expenseCategories))
    assertTrue(expense.belongsToOperationCategory(expenseCategoryParent, expenseCategories))

    expense.expenseCategoryGuid = "guid3"

    assertFalse(expense.belongsToOperationCategory(expenseCategory2, expenseCategories))
    assertFalse(expense.belongsToOperationCategory(expenseCategory3, expenseCategories))
    assertTrue(expense.belongsToOperationCategory(expenseCategory4, expenseCategories))
    assertFalse(expense.belongsToOperationCategory(expenseCategoryParent, expenseCategories))
  }
}