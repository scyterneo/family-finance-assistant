package com.scyele.familyfinanceassistant.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.scyele.lib.extension.livedata.observeNonNull
import javax.inject.Inject

abstract class BaseFragment<
    Model : BaseScreenVM<State>,
    Binding : ViewDataBinding,
    State,
    Factory : ViewModelProvider.Factory>
  : BaseEmptyFragment() {
  lateinit var binding: Binding
  lateinit var viewModel: Model

  @Inject
  lateinit var factory: Factory

  override fun onAttach(context: Context) {
    super.onAttach(context)
    injectDependency()
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    binding = DataBindingUtil
      .inflate(inflater, getLayoutId(), container, false)

    setupView()

    return binding.root
  }

  open fun setupView() {
    // base empty implementation
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    viewModel = ViewModelProvider(this, factory).get(getModelClass())
    bindingViewModeSetViewModel()


    viewModel.state().observeNonNull(this) {
      handleEvents(it)
    }
  }

  abstract fun injectDependency()

  abstract fun getLayoutId(): Int

  abstract fun bindingViewModeSetViewModel()

  abstract fun handleEvents(state: State)

  abstract fun getModelClass(): Class<Model>
}