package com.scyele.lib.extension.livedata

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.nonNull(): NonNullMediatorLiveData<T> {
  val mediator: NonNullMediatorLiveData<T> =
    NonNullMediatorLiveData()
  mediator.addSource(this) { it -> it?.let { mediator.value = it } }
  return mediator
}

fun <T> LiveData<T>.observeNonNull(owner: LifecycleOwner?, observer: (t: T) -> Unit) {
  nonNull().observeNonNull(owner, observer)
}

fun <T> NonNullMediatorLiveData<T>.observeNonNull(
  owner: LifecycleOwner?,
  observer: (t: T) -> Unit
) {
  if (owner == null) return
  this.observe(owner, Observer {
    it?.let(observer)
  })
}

fun <T> LiveData<T>.observeLiveData(owner: LifecycleOwner, action: (t: T) -> Unit) {
  this.observe(owner, Observer {
    run {
      action.invoke(it)
    }
  })
}