package com.scyele.familyfinanceassistant.ui.accounts.transfer

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Transfer
import com.scyele.familyfinanceassistant.summary.AccountSummary

class TransferRepository {
  val transfer = MutableLiveData<Transfer?>()
}