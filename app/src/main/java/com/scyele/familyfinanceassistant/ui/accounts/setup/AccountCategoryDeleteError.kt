package com.scyele.familyfinanceassistant.ui.accounts.setup


enum class AccountCategoryDeleteError(val message: String) {
  OPERATIONS_EXIST("Category should not have operations before deleting")
}