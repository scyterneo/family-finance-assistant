package com.scyele.familyfinanceassistant.ui.expences.main

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.repository.*
import com.scyele.familyfinanceassistant.summary.ExpenseSummary
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.operation.main.OperationsFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi


class ExpensesFragmentViewModel(
  mainRepository: MainRepository,
  operationEditRepository: OperationEditRepository<Expense, ExpenseCategory>,
  operationCategorySetupRepository: OperationCategorySetupRepository,
  accountCategorySetupRepository: AccountCategorySetupRepository,
  progressRepository: ProgressRepository
) : OperationsFragmentViewModel<Expense, ExpenseCategory, ExpenseSummary>(
  mainRepository,
  operationEditRepository,
  operationCategorySetupRepository,
  accountCategorySetupRepository,
  progressRepository
) {
  override fun getOperations(): MutableLiveData<List<Expense>> {
    return mainRepository.expenses
  }

  override fun getOperationCategories(): MutableLiveData<List<ExpenseCategory>> {
    return mainRepository.expenseCategories
  }

  override fun getTypedEvent(): Event<OperationType>? {
    return Event(OperationType.EXPENSE)
  }
}