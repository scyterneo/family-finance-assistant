package com.scyele.familyfinanceassistant.ui.move

import androidx.lifecycle.ViewModelProvider
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.MoveToCategoryLayoutBinding
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import com.scyele.familyfinanceassistant.utils.helper.ToastHelper
import com.scyele.lib.extension.initSpinner
import com.scyele.lib.extension.livedata.observeNonNull
import com.scyele.lib.extension.setupSpinner
import kotlinx.android.synthetic.main.move_to_category_layout.*

abstract class MoveToCategoryFragment<
    T : OperationCategory,
    Model : MoveToCategoryFragmentViewModel<T>,
    Factory : ViewModelProvider.Factory>
  :
  BaseDialogFragment<
      Model,
      MoveToCategoryLayoutBinding,
      MoveToCategoryFragmentViewModel.State<T>,
      Factory>() {

  override fun getLayoutId(): Int {
    return R.layout.move_to_category_layout
  }

  override fun bindToViewModel() {
    binding.viewModel = viewModel

    initSpinner(
      move_to_category_spinner,
      viewModel.onCategorySelected
    )

    viewModel.availableCategories.observeNonNull(this) { availableCategories ->
      val context = context ?: return@observeNonNull
      setupSpinner(
        move_to_category_spinner,
        context,
        availableCategories.map { category -> transformToVariants(category) })
    }
  }

  abstract fun transformToVariants(element: T): String

  override fun handleEvents(state: MoveToCategoryFragmentViewModel.State<T>) {
    state.currentCategoryName?.getContent()?.let {
      move_to_category_dialog_title.text = getString(R.string.move_to_category, it)
    }
    state.targetCategoryNotSelected?.getContent()?.let {
      ToastHelper.showToast(context, getString(R.string.target_category_not_selected))
    }
    state.moveCompleted?.getContent()?.let {
      dismiss()
    }
  }

  override fun tag() = "MoveToCategoryFragment"
}