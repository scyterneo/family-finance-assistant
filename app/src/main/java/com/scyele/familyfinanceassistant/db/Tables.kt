package com.scyele.familyfinanceassistant.db

object Tables {
  const val ACCOUNT_CATEGORY_TABLE_NAME = "AccountCategory"
  const val INCOME_CATEGORY_TABLE_NAME = "IncomeCategory"
  const val EXPENSE_CATEGORY_TABLE_NAME = "ExpenseCategory"
  const val TRANSFER_TABLE_NAME = "Transfer"
  const val INCOME_TABLE_NAME = "Income"
  const val EXPENSE_TABLE_NAME = "Expense"
  const val LOCATION = "Location"
  const val PASSWORD = "password"
  const val SHARING_LOCATION = "sharingLocation"
  const val USERS = "users"
}