package com.scyele.familyfinanceassistant.ui.history

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.ui.base.BaseVM
import com.scyele.familyfinanceassistant.ui.operation.history.OperationHistoryModel
import com.scyele.lib.extension.display

class IncomeHistoryViewModel : HistoryViewModel() {
  private val incomeCategoryName = MutableLiveData<String>()
  private val accountCategoryName = MutableLiveData<String>()
  private val incomeComment = MutableLiveData<String?>()
  private val incomeSum = MutableLiveData<String>()
  private val incomeDate = MutableLiveData<String>()

  fun bind(incomeHistoryModel: OperationHistoryModel.IncomeHistoryModel) {
    incomeSum.value = incomeHistoryModel.sum
    incomeCategoryName.value = incomeHistoryModel.incomeCategoryName
    accountCategoryName.value = incomeHistoryModel.accountCategoryName
    incomeComment.value = incomeHistoryModel.comment
    incomeDate.value = incomeHistoryModel.date.display()
  }

  fun getIncomeCategoryName() = incomeCategoryName
  fun getAccountCategoryName() = accountCategoryName
  fun getIncomeSum() = incomeSum
  fun getIncomeDate() = incomeDate
  fun getIncomeComment() = incomeComment
}