package com.scyele.familyfinanceassistant.ui.incomes.main

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.repository.*
import com.scyele.familyfinanceassistant.summary.IncomeSummary
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.operation.main.OperationsFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi


class IncomesFragmentViewModel(
  mainRepository: MainRepository,
  operationEditRepository: OperationEditRepository<Income, IncomeCategory>,
  operationCategorySetupRepository: OperationCategorySetupRepository,
  accountCategorySetupRepository: AccountCategorySetupRepository,
  progressRepository: ProgressRepository
) : OperationsFragmentViewModel<Income, IncomeCategory, IncomeSummary>(
  mainRepository,
  operationEditRepository,
  operationCategorySetupRepository,
  accountCategorySetupRepository,
  progressRepository
) {
  override fun getOperations(): MutableLiveData<List<Income>> {
    return mainRepository.incomes
  }

  override fun getOperationCategories(): MutableLiveData<List<IncomeCategory>> {
    return mainRepository.incomeCategories
  }

  override fun getTypedEvent(): Event<OperationType>? {
    return Event(OperationType.INCOME)
  }
}