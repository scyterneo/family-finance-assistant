package com.scyele.views.dateselector

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.scyele.lib.extension.DateSelectorSelectionType
import com.scyele.lib.extension.display
import com.scyele.lib.extension.getMonthText
import com.scyele.lib.extension.getWeekText
import com.scyele.views.base.MvvmCustomViewModel
import java.util.*

class DateSelectorViewModel : MvvmCustomViewModel<DateSelectorState> {
  private val selectionType = MutableLiveData<DateSelectorSelectionType>()
  private val date = MutableLiveData<Date>()
  private val selectionVariants = MutableLiveData<List<String>>()

  init {
    createSelectionVariants()
    selectionType.value = DateSelectorSelectionType.DAY
    date.value = Date()
  }

  override var state: DateSelectorState? = null
    get() = DateSelectorState(
      getCurrentSelectionType(),
      getCurrentDate()
    )
    set(value) {
      field = value
      restore(value)
    }

  fun getDate(): LiveData<Date> = date
  fun getSelectionType(): LiveData<DateSelectorSelectionType> = selectionType
  fun getSelectionVariants(): LiveData<List<String>> = selectionVariants

  val onSelectionTypeChanged: (position: Int) -> Unit = {
    selectionType.value = DateSelectorSelectionType.getByPosition(it)
  }

  fun movePrevious() {
    move(false)
  }

  fun moveNext() {
    move(true)
  }

  fun setDate(date: Date) {
    this.date.value = date
  }

  fun setSelectionType(dateSelectorSelectionType: DateSelectorSelectionType) {
    this.selectionType.value = dateSelectorSelectionType
  }

  fun createSelectionVariants() {
    val currentDate = getCurrentDate()
    val variants = mutableListOf<String>()
    variants.add(currentDate.display())
    variants.add(currentDate.getWeekText())
    variants.add(currentDate.getMonthText())
    variants.add(getYear(currentDate))
    variants.add("All time")

    selectionVariants.value = variants
  }

  private fun move(isMoveForvard: Boolean) {
    val currentDate = getCurrentDate()
    val calendar = Calendar.getInstance()
    val changeDateAmplifier = if (isMoveForvard) 1 else -1
    calendar.time = currentDate
    calendar.add(
      getCurrentSelectionType().moveType,
      changeDateAmplifier * getCurrentSelectionType().moveAmplifier
    )
    date.value = calendar.time
    createSelectionVariants()
  }

  private fun restore(state: DateSelectorState?) {
    date.value = state?.date
    selectionType.value = state?.selectionType
    createSelectionVariants()
  }

  private fun getYear(currentDate: Date): String {
    val calendar = Calendar.getInstance()
    calendar.time = currentDate
    return calendar.get(Calendar.YEAR).toString()
  }

  private fun getCurrentSelectionType() = selectionType.value ?: DateSelectorSelectionType.DAY

  private fun getCurrentDate() = date.value ?: Date()
}