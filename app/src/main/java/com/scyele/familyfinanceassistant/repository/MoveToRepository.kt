package com.scyele.familyfinanceassistant.repository

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.entity.OperationCategory

class MoveToRepository {
  var operationCategory: OperationCategory? = null
  val moveCompleted: MutableLiveData<Any?> = MutableLiveData()

  fun reinit() {
    operationCategory = null
    moveCompleted.value = null
  }
}
