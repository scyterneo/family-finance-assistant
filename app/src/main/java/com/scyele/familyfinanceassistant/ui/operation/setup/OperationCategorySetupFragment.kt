package com.scyele.familyfinanceassistant.ui.operation.setup

import android.content.DialogInterface
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.OperationCategorySetupFragmentBinding
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.expences.setup.MoveToExpenseCategoryFragment
import com.scyele.familyfinanceassistant.ui.incomes.setup.MoveToIncomeCategoryFragment
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.familyfinanceassistant.utils.helper.ToastHelper
import com.scyele.lib.extension.onTextChanged
import com.scyele.lib.extension.setStateText
import kotlinx.android.synthetic.main.operation_category_setup_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



abstract class OperationCategorySetupFragment<
    Action,
    Category,
    Summary,
    Model>
  : BaseDialogFragment<
    Model,
    OperationCategorySetupFragmentBinding,
    OperationCategorySetupFragmentViewModel.State,
    FragmentViewModelFactory>()
    where Action : Operation,
          Category : OperationCategory,
          Summary : OperationSummary,
          Model : OperationCategorySetupFragmentViewModel<Action, Category, Summary> {

  private lateinit var parentCategoriesAdapter: ArrayAdapter<String>

  override fun bindToViewModel() {
    binding.viewModel = viewModel

    operation_category_name.onTextChanged { viewModel.onTextInputted(it) }
    operation_category_name.requestFocus()

    parent_category_spinner.visibility = View.VISIBLE
    createParentCategoriesAdapter(listOf())

    parent_category_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      override fun onNothingSelected(parent: AdapterView<*>?) {
        viewModel.onParentCategorySelected(-1)
      }

      override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.onParentCategorySelected(position - 1)
      }
    }
  }

  override fun handleEvents(state: OperationCategorySetupFragmentViewModel.State) {
    state.setupComplete?.getContent()?.let {
      dismiss()
    }
    state.name.getContent()?.let {
      operation_category_name.setStateText(it)
    }
    state.isParent.getContent()?.let {
      is_parent.isChecked = it
    }
    state.allCategories.getContent()?.let {
      showCategoriesSelection(it)
    }

    state.parentCategory.getContent()?.let {
      parent_category_spinner.setSelection(parentCategoriesAdapter.getPosition(it.name()))
    }

    state.checkForDeleting?.getContent()?.let {
      DialogHelper.showThreeButtonsDialog(
        activity,
        getString(R.string.delete_operation_category),
        getString(R.string.delete_operation_category_message, it),
        android.R.drawable.ic_delete,
        getString(R.string.do_not_delete_current_category),
        {},
        getString(R.string.delete_all_operations),
        { viewModel.deleteAllRelated() },
        getString(R.string.change_category_of_all_operations),
        { viewModel.changeCategoryRequired() }
      )
    }

    state.saveError?.getContent()?.let {
      ToastHelper.showToast(context, it.message)
    }
    state.deleteError?.getContent()?.let {
      ToastHelper.showToast(context, it.message)
    }

    state.changeCategoryRequired?.getContent()?.let {
      DialogHelper.showDialogFragment(
        when (it) {
          OperationType.INCOME -> MoveToIncomeCategoryFragment()
          OperationType.EXPENSE -> MoveToExpenseCategoryFragment()
        },
        activity
      )
    }
  }

  override fun getLayoutId(): Int {
    return R.layout.operation_category_setup_fragment
  }

  override fun tag() = "SetupOperationCategoryFragment"

  override fun onDismiss(dialog: DialogInterface) {
    super.onDismiss(dialog)
    viewModel.onDismiss()
  }

  private fun showCategoriesSelection(parentCategories: List<OperationCategory>) {
    val operationCategoryNames = mutableListOf<String>()
    operationCategoryNames.add(getString(R.string.operation_parent_category))
    operationCategoryNames.addAll(parentCategories.map { it.name() })

    createParentCategoriesAdapter(operationCategoryNames)
  }

  private fun createParentCategoriesAdapter(operationCategoryNames: List<String>) {
    parentCategoriesAdapter = ArrayAdapter(
      requireContext(),
      android.R.layout.simple_spinner_item,
      operationCategoryNames
    )
    parentCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    parent_category_spinner.adapter = parentCategoriesAdapter
  }
}