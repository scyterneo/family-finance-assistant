package com.scyele.familyfinanceassistant.ui.operation.setup

// TODO replace String with Res ID
// TODO replace with sealed class??
enum class OperationCategorySaveError(val message: String) {
  EMPTY_NAME("Name should not be empty"),
  EMPTY_PARENT("Parent category required"),
  PARENT_COULD_NOT_BE_CHILD_AND_VISE_VERSA("Parent category could not become child and vise-versa")
}