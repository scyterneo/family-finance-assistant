package com.scyele.familyfinanceassistant.ui.move

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.MoveToRepository
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import kotlinx.coroutines.InternalCoroutinesApi


abstract class MoveToCategoryFragmentViewModel<T : OperationCategory>(
  protected val mainRepository: MainRepository,
  protected val moveToRepository: MoveToRepository
) :
  BaseScreenVM<MoveToCategoryFragmentViewModel.State<T>>() {

  protected var currentCategory: T? = null
  protected var targetCategory: T? = null
  val availableCategories: MutableLiveData<List<T>> = MutableLiveData()

  override fun initState(): State<T> = State(currentCategoryName = Event(getCurrentCategoryName()))

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State<T>> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(getAllCategoriesSource()) {
      availableCategories.value = it.filter { category -> filterElement(category) }
    }
    return mediatorState
  }

  fun moveToModelAttempt() {
    if (targetCategory != null) {
      moveToModel()
    } else {
      state.value = state.value.copy(targetCategoryNotSelected = SimpleEvent())
    }
  }

  val onCategorySelected: (position: Int) -> Unit = {
    val list = availableCategories.value ?: listOf()
    targetCategory =
      if (list.size > it && it >= 0) list[it]
      else null
  }

  abstract fun moveToModel()

  abstract fun getAllCategoriesSource(): LiveData<List<T>>
  abstract fun filterElement(category: T): Boolean
  abstract fun getCurrentCategoryName(): String

  data class State<T>(
    val currentCategoryName: Event<String>? = null,
    val moveCompleted: SimpleEvent? = null,
    val targetCategoryNotSelected: SimpleEvent? = null
  )
}