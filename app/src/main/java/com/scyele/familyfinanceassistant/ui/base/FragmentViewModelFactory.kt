package com.scyele.familyfinanceassistant.ui.base

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.scyele.familyfinanceassistant.repository.*
import com.scyele.familyfinanceassistant.ui.accounts.AccountsFragmentViewModel
import com.scyele.familyfinanceassistant.ui.accounts.setup.AccountSetupFragmentViewModel
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferRepository
import com.scyele.familyfinanceassistant.ui.expences.edit.ExpenseEditFragmentViewModel
import com.scyele.familyfinanceassistant.ui.expences.main.ExpensesFragmentViewModel
import com.scyele.familyfinanceassistant.ui.expences.setup.ExpenseCategorySetupFragmentViewModel
import com.scyele.familyfinanceassistant.ui.expences.setup.MoveToExpenseCategoryFragmentViewModel
import com.scyele.familyfinanceassistant.ui.history.HistoryFragmentViewModel
import com.scyele.familyfinanceassistant.ui.incomes.edit.IncomeEditFragmentViewModel
import com.scyele.familyfinanceassistant.ui.incomes.main.IncomesFragmentViewModel
import com.scyele.familyfinanceassistant.ui.incomes.setup.IncomeCategorySetupFragmentViewModel
import com.scyele.familyfinanceassistant.ui.incomes.setup.MoveToIncomeCategoryFragmentViewModel
import com.scyele.familyfinanceassistant.ui.login.join.JoinFragmentViewModel
import com.scyele.familyfinanceassistant.ui.settings.SettingsFragmentViewModel
import com.scyele.familyfinanceassistant.ui.statistics.StatisticsFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi


class FragmentViewModelFactory(
  private val mainRepository: MainRepository,
  private val incomeEditRepository: IncomeEditRepository,
  private val expenseEditRepository: ExpenseEditRepository,
  private val operationCategorySetupRepository: OperationCategorySetupRepository,
  private val accountCategorySetupRepository: AccountCategorySetupRepository,
  private val progressRepository: ProgressRepository,
  private val transferRepository: TransferRepository,
  private val moveToRepository: MoveToRepository,
  private val resources: Resources
) : ViewModelProvider.Factory {
  @Suppress("UNCHECKED_CAST")
  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(AccountsFragmentViewModel::class.java)) {
      return AccountsFragmentViewModel(
        mainRepository,
        transferRepository,
        accountCategorySetupRepository,
        operationCategorySetupRepository
      ) as T
    }
    if (modelClass.isAssignableFrom(AccountSetupFragmentViewModel::class.java)) {
      return AccountSetupFragmentViewModel(
        mainRepository,
        accountCategorySetupRepository,
        resources
      ) as T
    }
    if (modelClass.isAssignableFrom(IncomeCategorySetupFragmentViewModel::class.java)) {
      return IncomeCategorySetupFragmentViewModel(
        operationCategorySetupRepository,
        mainRepository,
        moveToRepository
      ) as T
    }
    if (modelClass.isAssignableFrom(ExpenseCategorySetupFragmentViewModel::class.java)) {
      return ExpenseCategorySetupFragmentViewModel(
        operationCategorySetupRepository,
        mainRepository,
        moveToRepository
      ) as T
    }
    if (modelClass.isAssignableFrom(IncomeEditFragmentViewModel::class.java)) {
      return IncomeEditFragmentViewModel(
        mainRepository,
        incomeEditRepository,
        progressRepository
      ) as T
    }
    if (modelClass.isAssignableFrom(ExpenseEditFragmentViewModel::class.java)) {
      return ExpenseEditFragmentViewModel(
        mainRepository,
        expenseEditRepository,
        progressRepository
      ) as T
    }
    if (modelClass.isAssignableFrom(SettingsFragmentViewModel::class.java)) {
      return SettingsFragmentViewModel() as T
    }
    if (modelClass.isAssignableFrom(IncomesFragmentViewModel::class.java)) {
      return IncomesFragmentViewModel(
        mainRepository,
        incomeEditRepository,
        operationCategorySetupRepository,
        accountCategorySetupRepository,
        progressRepository
      ) as T
    }
    if (modelClass.isAssignableFrom(ExpensesFragmentViewModel::class.java)) {
      return ExpensesFragmentViewModel(
        mainRepository,
        expenseEditRepository,
        operationCategorySetupRepository,
        accountCategorySetupRepository,
        progressRepository
      ) as T
    }
    if (modelClass.isAssignableFrom(HistoryFragmentViewModel::class.java)) {
      return HistoryFragmentViewModel(
        mainRepository,
        operationCategorySetupRepository,
        accountCategorySetupRepository,
        incomeEditRepository,
        expenseEditRepository,
        transferRepository
      ) as T
    }

    if (modelClass.isAssignableFrom(StatisticsFragmentViewModel::class.java)) {
      return StatisticsFragmentViewModel() as T
    }
    if (modelClass.isAssignableFrom(JoinFragmentViewModel::class.java)) {
      return JoinFragmentViewModel() as T
    }
    if (modelClass.isAssignableFrom(MoveToExpenseCategoryFragmentViewModel::class.java)) {
      return MoveToExpenseCategoryFragmentViewModel(mainRepository, moveToRepository) as T
    }
    if (modelClass.isAssignableFrom(MoveToIncomeCategoryFragmentViewModel::class.java)) {
      return MoveToIncomeCategoryFragmentViewModel(mainRepository, moveToRepository) as T
    }

    throw IllegalArgumentException("Unknown ViewModel class")
  }
}