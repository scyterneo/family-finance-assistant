package com.scyele.utils.extension

import com.scyele.lib.extension.getWeekEndDate
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.util.*

class DateExtensionsKtTest {

  @Test
  fun getWeekStartDate() {
  }
}

@RunWith(Parameterized::class)
class DateExtensionGetWeekEndDateTest(
  private val year: Int,
  private val month: Int,
  private val day: Int,
  private val firstDayOfWeek: Int,
  private val expectedEndOfWeekDayOfMonth: Int
) {
  companion object {
    @JvmStatic
    @Parameterized.Parameters
    fun data(): Collection<Array<Int>> {
      return listOf(
        arrayOf(1997, 7/*August*/, 29, Calendar.MONDAY, 31), // make sure to stop Judgment day
        arrayOf(1997, 7, 31, Calendar.MONDAY, 31),
        arrayOf(1997, 8, 1, Calendar.MONDAY, 7),
        arrayOf(1997, 8, 2, Calendar.MONDAY, 7),
        arrayOf(1997, 8, 3, Calendar.MONDAY, 7),
        arrayOf(1997, 8, 4, Calendar.MONDAY, 7),
        arrayOf(1997, 8, 5, Calendar.MONDAY, 7),
        arrayOf(1997, 8, 6, Calendar.MONDAY, 7),
        arrayOf(1997, 8, 7, Calendar.MONDAY, 7)
      )
    }
  }

  @Test
  fun test() {
    val date = com.scyele.lib.extension.getDate(year, month, day) //Friday
    val weekEndDate = date.getWeekEndDate(firstDayOfWeek)
    val calendar = Calendar.getInstance()
    calendar.firstDayOfWeek = firstDayOfWeek
    calendar.time = weekEndDate
    val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
    val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
    assertEquals(Calendar.SUNDAY, dayOfWeek)
    assertEquals(expectedEndOfWeekDayOfMonth, dayOfMonth)
  }

}