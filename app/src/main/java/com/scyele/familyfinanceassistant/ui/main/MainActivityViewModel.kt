package com.scyele.familyfinanceassistant.ui.main

import com.scyele.familyfinanceassistant.repository.AccountCategorySetupRepository
import com.scyele.familyfinanceassistant.repository.OperationCategorySetupRepository
import com.scyele.familyfinanceassistant.repository.ProgressRepository
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event


class MainActivityViewModel(
  private val operationCategorySetupRepository: OperationCategorySetupRepository,
  private val accountCategorySetupRepository: AccountCategorySetupRepository,
  private val progressRepository: ProgressRepository
) : BaseScreenVM<MainActivityViewModel.State>() {
  override fun initState(): State {
    val screenId = progressRepository.getLastOpenedScreen()
    //TODO replace with correct constants
    return when (screenId) {
      1 -> State(showIncomeScreen = Event(false))
      2 -> State(showExpensesScreen = Event(false))
      3 -> State(showStatisticsScreen = Event(false))
      else -> State(showAccountsScreen = Event(false))
    }
  }

  fun onNavigationAccountsClicked() {
    progressRepository.setLastOpenedScreen(0)
    state.value = state.value.copy(showAccountsScreen = Event(false))
  }

  fun onNavigationIncomeClicked() {
    progressRepository.setLastOpenedScreen(1)
    state.value = state.value.copy(showIncomeScreen = Event(false))
  }

  fun onNavigationExpenseClicked() {
    progressRepository.setLastOpenedScreen(2)
    state.value = state.value.copy(showExpensesScreen = Event(false))
  }

  fun onNavigationStatisticsClicked() {
    progressRepository.setLastOpenedScreen(3)
    operationCategorySetupRepository.reinit()
    accountCategorySetupRepository.reinit()
    state.value = state.value.copy(showStatisticsScreen = Event(false))
  }

  fun onNavigationSettingsClicked() {
    state.value = state.value.copy(showSettingsScreen = Event(false))
  }

  data class State(
    val showAccountsScreen: Event<Boolean>? = null,
    val showIncomeScreen: Event<Boolean>? = null,
    val showExpensesScreen: Event<Boolean>? = null,
    val showStatisticsScreen: Event<Boolean>? = null,
    val showSettingsScreen: Event<Boolean>? = null
  )
}