package com.scyele.views.dateselector

import com.scyele.lib.extension.DateSelectorSelectionType
import java.util.*

interface DateSelectionChangedListener {
  fun onDateSelectionChanged(date: Date, selectionType: DateSelectorSelectionType)
}