package com.scyele.familyfinanceassistant.ui.settings

import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent


class SettingsFragmentViewModel() :
  BaseScreenVM<SettingsFragmentViewModel.State>() {

  init {

  }

  override fun initState() = State()

  fun onShareLocationClicked() {
    state.value = state.value.copy(shareLocationClicked = SimpleEvent())
  }

  data class State(
    val shareLocationClicked: SimpleEvent? = null
  )
}