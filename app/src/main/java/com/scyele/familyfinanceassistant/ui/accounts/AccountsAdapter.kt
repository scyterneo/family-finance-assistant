package com.scyele.familyfinanceassistant.ui.accounts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.AccountSummaryItemBinding
import com.scyele.familyfinanceassistant.summary.AccountSummary

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class AccountsAdapter(val onAccountClickEvent: (String) -> Unit, val onAccountLongClickEvent: (String) -> Unit) :
  ListAdapter<AccountSummary, AccountsAdapter.AccountViewHolder>(AccountSummaryCallback()) {

  private lateinit var binding: AccountSummaryItemBinding

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
    binding = DataBindingUtil.inflate(
      LayoutInflater.from(parent.context),
      R.layout.account_summary_item, parent, false
    )
    return AccountViewHolder(binding)
  }

  override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
    holder.itemView.setBackgroundResource(R.drawable.bg_list_item_states)
    holder.bind(getItem(position))
  }

  inner class AccountViewHolder(private val binding: AccountSummaryItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    private val viewModel = AccountSummaryViewModel()

    fun bind(accountSummary: AccountSummary) {
      viewModel.bind(accountSummary)
      binding.viewModel = viewModel

      itemView.setOnClickListener {
        onAccountClickEvent(accountSummary.accountCategory.guid)
      }
      itemView.setOnLongClickListener {
        onAccountLongClickEvent(accountSummary.accountCategory.guid)
        true
      }
    }
  }

  class AccountSummaryCallback : DiffUtil.ItemCallback<AccountSummary>() {
    override fun areItemsTheSame(oldItem: AccountSummary, newItem: AccountSummary): Boolean {
      return oldItem.accountCategory == newItem.accountCategory
    }

    override fun areContentsTheSame(oldItem: AccountSummary, newItem: AccountSummary): Boolean {
      return oldItem.accountCategory == newItem.accountCategory
          && oldItem.expenses == newItem.expenses
          && oldItem.incomes == newItem.incomes
          && oldItem.transfers == newItem.transfers
    }
  }
}