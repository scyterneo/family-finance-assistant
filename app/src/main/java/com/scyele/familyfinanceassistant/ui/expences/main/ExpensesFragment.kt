package com.scyele.familyfinanceassistant.ui.expences.main

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.summary.ExpenseSummary
import com.scyele.familyfinanceassistant.ui.operation.main.OperationsFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class ExpensesFragment :
  OperationsFragment<Expense, ExpenseCategory, ExpenseSummary, ExpensesFragmentViewModel>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<ExpensesFragmentViewModel> {
    return ExpensesFragmentViewModel::class.java
  }
}