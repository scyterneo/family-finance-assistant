package com.scyele.views.dateselector

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.LifecycleOwner
import com.scyele.lib.extension.DateSelectorSelectionType
import com.scyele.lib.extension.initSpinner
import com.scyele.lib.extension.livedata.observeNonNull
import com.scyele.views.R
import com.scyele.views.base.MvvmLinearLayout
import kotlinx.android.synthetic.main.date_selector_layout.view.*
import java.util.*

class DateSelector @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null
) : MvvmLinearLayout<DateSelectorState, DateSelectorViewModel>(context, attrs) {

  override val viewModel = DateSelectorViewModel()
  private var listener: (date: Date, selectionType: DateSelectorSelectionType) -> Unit =
    { _: Date, _: DateSelectorSelectionType -> }

  init {
    init()
  }

  override fun onLifecycleOwnerAttached(lifecycleOwner: LifecycleOwner?) {
    if (lifecycleOwner == null) {
      return
    }
    observeLiveData(lifecycleOwner)
  }

  fun setDateSelectionChangedListener(listener: (date: Date, selectionType: DateSelectorSelectionType) -> Unit) {
    this.listener = listener
    viewModel.createSelectionVariants()
  }

  fun setDate(date: Date) {
    viewModel.setDate(date)
  }

  fun setSelectionType(dateSelectorSelectionType: DateSelectorSelectionType) {
    viewModel.setSelectionType(dateSelectorSelectionType)
  }

  private fun init() {
    View.inflate(context, R.layout.date_selector_layout, this)
    btn_previous.setOnClickListener { viewModel.movePrevious() }
    btn_next.setOnClickListener { viewModel.moveNext() }
    setupSpinner()
  }

  private fun observeLiveData(lifecycleOwner: LifecycleOwner) {
    viewModel.getDate().observeNonNull(lifecycleOwner) {
      setupSpinner()
    }
    viewModel.getSelectionType().observeNonNull(lifecycleOwner) {
      setupSpinner()
      notifyListener()
    }
    viewModel.getSelectionVariants().observeNonNull(lifecycleOwner) {
      setupSpinner(it, viewModel.getSelectionType().value?.position)
      notifyListener()
    }
  }

  private fun setupSpinner(textVariants: List<String>, position: Int?) {
    spinner_date_range.onItemSelectedListener = null
    val context = context ?: return
    val adapter = ArrayAdapter(
      context,
      android.R.layout.simple_spinner_item,
      textVariants
    )
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    spinner_date_range.adapter = adapter
    spinner_date_range.setSelection(position ?: 0, true)
    setupSpinner()
  }

  private fun notifyListener() {
    val date = viewModel.getDate().value
    val selectionType = viewModel.getSelectionType().value
    date?.let { selectedDate ->
      selectionType?.let { selectedSelectionType ->
        listener.invoke(selectedDate, selectedSelectionType)
      }
    }
  }

  private fun setupSpinner() {
    initSpinner(spinner_date_range, viewModel.onSelectionTypeChanged)
  }
}
