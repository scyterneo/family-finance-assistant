package com.scyele.familyfinanceassistant.utils

import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.summary.createNewSummary

object Aggregator {
  fun createOperationSummaries(
    operations: List<Operation>,
    operationCategories: List<OperationCategory>
  ): List<OperationSummary> {
    val parentOperationSummaries = operationCategories
      .filter { it.isFirstLevelParent() }
      .sortedBy { it.name() }
      .mapNotNull {
        createNewSummary(it)
      }

    for (parentOperationSummary in parentOperationSummaries) {
      fillOperationCategories(parentOperationSummary, operationCategories, operations)
    }
    return parentOperationSummaries
  }

  private fun fillOperationCategories(
    operationSummary: OperationSummary,
    allOperationCategories: List<OperationCategory>,
    allOperations: List<Operation>
  ) {
    if (operationSummary.operationCategory().isParentCategory()) {
      val childOperationSummaries = allOperationCategories
        .filter { it.parentGuid() == operationSummary.operationCategory().guid() }
        .sortedBy { (!it.isParentCategory()).toString() + it.name() }
        .mapNotNull {
          createNewSummary(
            operationCategory = it,
            levelOfDepth = operationSummary.levelOfDepth() + 1
          )
        }
      operationSummary.setChildOperationSummaries(childOperationSummaries)
      for (childOperationCategory in operationSummary.childOperationSummaries()) {
        fillOperationCategories(childOperationCategory, allOperationCategories, allOperations)
      }
    } else {
      operationSummary.setOperations(allOperations
        .filter { it.operationCategoryGuid() == operationSummary.operationCategory().guid() }
      )
    }
  }
}