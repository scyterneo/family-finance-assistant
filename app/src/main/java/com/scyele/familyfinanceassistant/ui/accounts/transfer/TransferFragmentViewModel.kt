package com.scyele.familyfinanceassistant.ui.accounts.transfer

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.AccountCategory
import com.scyele.familyfinanceassistant.entity.Transfer
import com.scyele.familyfinanceassistant.extension.isPositive
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.ui.base.BaseScreenVM
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.base.SimpleEvent
import com.scyele.familyfinanceassistant.utils.Guid
import com.scyele.familyfinanceassistant.utils.extension.getElementByPosition
import com.scyele.lib.extension.display
import com.scyele.lib.extension.livedata.SafeMediatorLiveData
import kotlinx.coroutines.InternalCoroutinesApi
import java.math.BigDecimal
import java.util.*


class TransferFragmentViewModel(
  private val mainRepository: MainRepository,
  private val transferRepository: TransferRepository
) : BaseScreenVM<TransferFragmentViewModel.State>() {

  override fun initState() = State()
  private var transfer: Transfer? = null

  private val dateString = MutableLiveData<String>()

  private val isCommentVisible = MutableLiveData<Boolean>(false)

  private val isRecordDeletable = MutableLiveData<Boolean>(false)

  override fun getMediatorLiveDataState(): SafeMediatorLiveData<State> {
    val mediatorState = SafeMediatorLiveData(initState())
    mediatorState.addSource(transferRepository.transfer) {
      transfer = it
      configureTransferAccounts()
    }
    mediatorState.addSource(mainRepository.accountCategories) { accountCategories ->
      state.value = state.value.copy(accountCategories = Event(accountCategories))
      configureTransferAccounts()
    }
    return mediatorState
  }

  fun getDate() = dateString

  fun isCommentVisible() = isCommentVisible

  fun onOkClicked() {
    if (validateTransferData()) {
      createTransfer()
      state.value = state.value.copy(transferComplete = SimpleEvent())
    } else {
      state.value = state.value.copy(transferError = SimpleEvent())
    }
  }

  fun onDeleteClicked() {
    transfer?.let {
      mainRepository.removeTransfer(it)
      state.value = state.value.copy(transferComplete = SimpleEvent())
    }
  }

  fun onAddComment() {
    isCommentVisible.value = true
  }

  fun isRecordDeletable() = isRecordDeletable

  fun onTransferSumChanged(text: CharSequence) {
    val transferSum = try {
      BigDecimal(text.toString())
    } catch (e: NumberFormatException) {
      BigDecimal.ZERO
    }

    if (transferSum != state.value.transferSum) {
      state.value = state.value.copy(transferSum = transferSum)
    }
  }

  fun onSelectDateClicked() {
    state.value = state.value.copy(selectDate = Event(state.value.date))
  }

  fun onTransferCommentChanged(text: CharSequence) {
    state.value = state.value.copy(
      transferComment = text.toString()
    )
  }

  fun onFromAccountSelected(position: Int) {
    val fromAccount = getElementByPosition(position, state.value.accountCategories?.peekContent())
    if (state.value.transferFromAccount != fromAccount) {
      state.value = state.value.copy(transferFromAccount = fromAccount)
    }
  }

  fun onToAccountSelected(position: Int) {
    val toAccount = getElementByPosition(position, state.value.accountCategories?.peekContent())
    if (state.value.transferToAccount != toAccount) {
      state.value = state.value.copy(transferToAccount = toAccount)
    }
  }

  private fun validateTransferData() =
    state.value.transferFromAccount != null
        && state.value.transferToAccount != null
        && state.value.transferSum.isPositive()
        && state.value.transferFromAccount != state.value.transferToAccount

  private fun createTransfer() {
    val transfer = Transfer(
      transfer?.guid ?: Guid.new(),
      state.value.transferSum.toString(),
      state.value.transferFromAccount!!.guid,
      state.value.transferToAccount!!.guid,
      state.value.date,
      state.value.transferComment
    )
    mainRepository.setTransfer(transfer)
  }

  private fun configureTransferAccounts() {
    transfer?.let { transfer ->
      state.value.accountCategories?.peekContent()?.let { accountCategories ->
        state.value = state.value.copy(
          transferSum = transfer.sum.toBigDecimal(),
          transferFromAccount = accountCategories.firstOrNull { account ->
            account.guid == transfer.fromAccountCategoryGuid
          },
          transferToAccount = accountCategories.firstOrNull { account ->
            account.guid == transfer.toAccountCategoryGuid
          },
          date = transfer.date,
          transferComment = transfer.comment ?: ""
        )
        dateString.value = transfer.date.display()
        isCommentVisible.value = !transfer.comment.isNullOrBlank()
      }
    }
    isRecordDeletable.value = transfer != null
  }

  fun setDate(date: Date) {
    state.value = state.value.copy(date = date)
    dateString.value = date.display()
  }

  data class State(
    val transferFromAccount: AccountCategory? = null,
    val accountCategories: Event<List<AccountCategory>>? = null,
    val transferToAccount: AccountCategory? = null,
    val date: Date = Date(),
    val selectDate: Event<Date>? = null,
    val transferSum: BigDecimal = BigDecimal.ZERO,
    val transferComment: String = "",
    val transferComplete: SimpleEvent? = null,
    val transferError: SimpleEvent? = null,
    val cancelClicked: SimpleEvent? = null
  )
}