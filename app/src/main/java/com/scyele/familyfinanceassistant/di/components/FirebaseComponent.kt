package com.scyele.familyfinanceassistant.di.components

import com.scyele.familyfinanceassistant.di.modules.FirebaseModule
import com.scyele.familyfinanceassistant.repository.AuthRepository
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.ui.login.LoginActivityViewModel
import com.scyele.familyfinanceassistant.ui.settings.location.ShareLocationFragmentViewModel
import dagger.Component
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    (FirebaseModule::class)
  ]
)
interface FirebaseComponent {

  fun inject(shareLocationFragmentViewModel: ShareLocationFragmentViewModel)

  fun inject(loginActivityViewModel: LoginActivityViewModel)

  fun inject(mainRepository: MainRepository)

  fun inject(authRepository: AuthRepository)

  @Component.Builder
  interface Builder {

    fun build(): FirebaseComponent

    fun firebaseModule(firebaseModule: FirebaseModule): Builder
  }
}