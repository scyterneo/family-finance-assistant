package com.scyele.familyfinanceassistant.ui.expences.setup

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.move.MoveToCategoryFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class MoveToExpenseCategoryFragment : MoveToCategoryFragment<
    ExpenseCategory,
    MoveToExpenseCategoryFragmentViewModel,
    FragmentViewModelFactory>() {
  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<MoveToExpenseCategoryFragmentViewModel> =
    MoveToExpenseCategoryFragmentViewModel::class.java

  override fun transformToVariants(element: ExpenseCategory): String {
    return element.name
  }
}