package com.scyele.familyfinanceassistant.di.modules

import android.content.Context
import android.content.res.Resources
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.ui.login.LoginActivityViewModelFactory
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Singleton




@Module
class LoginActivityModule(private val assistantApplication: AssistantApplication) {

  @Provides
  @Singleton
  fun provideContext(): Context = assistantApplication

  @Provides
  fun provideResources(context: Context): Resources {
    return context.resources
  }

  @Provides
  @Singleton
  fun provideLoginActivityViewModelFactory() = LoginActivityViewModelFactory()
}