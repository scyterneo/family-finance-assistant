package com.scyele.familyfinanceassistant.repository

import com.google.firebase.auth.FirebaseAuth
import com.scyele.familyfinanceassistant.AssistantApplication
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Inject



class AuthRepository {
  @Inject
  lateinit var auth: FirebaseAuth

  init {
    AssistantApplication.instance.firebaseComponent.inject(this)
  }

  fun getUserID() = auth.uid
}