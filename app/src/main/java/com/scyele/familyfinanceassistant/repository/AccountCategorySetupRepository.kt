package com.scyele.familyfinanceassistant.repository

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.AccountCategory

class AccountCategorySetupRepository {
  var accountCategory = MutableLiveData<AccountCategory?>()

  fun reinit() {
    accountCategory.value = null
  }
}