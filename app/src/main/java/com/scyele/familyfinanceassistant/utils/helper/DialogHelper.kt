package com.scyele.familyfinanceassistant.utils.helper


import android.app.DatePickerDialog
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import com.scyele.lib.extension.getDate
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*



object DialogHelper {

  fun showDialogFragment(
    fragment: BaseDialogFragment<*, *, *, *>,
    activity: FragmentActivity?
  ) {
    if (activity == null) return
    fragment.show(activity.supportFragmentManager, fragment.tag())
  }

  fun showDateSelectionDialog(
    activity: FragmentActivity?,
    onDateSelected: (date: Date) -> Unit,
    date: Date = Date()
  ) {
    if (activity == null) return

    val calendar = GregorianCalendar()
    calendar.time = date
    val datePicker = DatePickerDialog(
      activity,
      DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        onDateSelected.invoke(getDate(year, month, dayOfMonth))
      },
      calendar[Calendar.YEAR],
      calendar[Calendar.MONTH],
      calendar[Calendar.DAY_OF_MONTH]
    )
    datePicker.show()
  }

  fun showThreeButtonsDialog(
    activity: FragmentActivity?,
    dialogTitle: String = "",
    message: String = "",
    dialogIcon: Int? = null,
    firstButtonTitle: String? = null,
    firstButtonCallback: () -> Unit,
    secondButtonTitle: String? = null,
    secondButtonCallback: () -> Unit,
    thirdButtonTitle: String? = null,
    thirdButtonCallback: (() -> Unit)? = null
  ) {
    if (activity == null) return

    val builder = AlertDialog.Builder(activity)
    builder.setTitle(dialogTitle)
    if (dialogIcon != null) {
      builder.setIcon(dialogIcon)
    }
    builder.setMessage(message)

    builder.setPositiveButton(firstButtonTitle) { dialog, _ ->
      firstButtonCallback.invoke()
      dialog.cancel()
    }
    builder.setNeutralButton(secondButtonTitle) { dialog, _ ->
      secondButtonCallback.invoke()
      dialog.cancel()
    }
    if (thirdButtonTitle != null && thirdButtonCallback != null) {
      builder.setNegativeButton(thirdButtonTitle) { dialog, _ ->
        thirdButtonCallback.invoke()
        dialog.cancel()
      }
    }

    builder.create().show()
  }
}