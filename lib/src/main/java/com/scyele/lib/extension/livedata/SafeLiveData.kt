package com.scyele.lib.extension.livedata

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

// TODO should be replaced when LiveData 2.1.0 released
/**
 * Use to wrap LiveData with initial value
 */
class SafeLiveData<T>(initData: T) : MutableLiveData<T>() {

    init {
        value = initData
    }

    @Suppress("UnsafeCallOnNullableType")
    override fun getValue(): T = super.getValue()!!
}

// TODO should be replaced when LiveData 2.1.0 released
/**
 * Use to wrap MediatorLiveData with initial value
 */
class SafeMediatorLiveData<T>(initData: T) : MediatorLiveData<T>() {

    init {
        value = initData
    }

    @Suppress("UnsafeCallOnNullableType")
    override fun getValue(): T = super.getValue()!!
}
