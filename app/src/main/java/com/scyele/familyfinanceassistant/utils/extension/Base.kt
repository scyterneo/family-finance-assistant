package com.scyele.familyfinanceassistant.utils.extension

fun String.toRateFloat() = this.toFloatOrNull() ?: 1f
