package com.scyele.lib.test

import android.annotation.SuppressLint
import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext

@SuppressLint("RestrictedApi")
class InstantExecutableExtension : BeforeAllCallback, AfterAllCallback {

  override fun beforeAll(context: ExtensionContext?) {
    ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
      override fun executeOnDiskIO(runnable: Runnable) {
        runnable.run()
      }

      override fun isMainThread(): Boolean {
        return true
      }

      override fun postToMainThread(runnable: Runnable) {
        runnable.run()
      }
    })
  }

  override fun afterAll(context: ExtensionContext?) {
    ArchTaskExecutor.getInstance().setDelegate(null)
  }
}
