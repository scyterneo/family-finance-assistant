package com.scyele.familyfinanceassistant.ui.settings.location

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.scyele.familyfinanceassistant.repository.AuthRepository
import com.scyele.familyfinanceassistant.repository.MainRepository


class SettingsFragmentViewModelFactory(
  private val mainRepository: MainRepository,
  private val authRepository: AuthRepository
) : ViewModelProvider.Factory {

  @Suppress("UNCHECKED_CAST")
  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(ShareLocationFragmentViewModel::class.java)) {
      return ShareLocationFragmentViewModel(mainRepository, authRepository) as T
    }

    throw IllegalArgumentException("Unknown ViewModel class")
  }
}
