package com.scyele.familyfinanceassistant.ui.accounts

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.AccountsFragmentBinding
import com.scyele.familyfinanceassistant.ui.accounts.setup.AccountSetupFragment
import com.scyele.familyfinanceassistant.ui.accounts.transfer.TransferFragment
import com.scyele.familyfinanceassistant.ui.base.BaseFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.history.HistoryFragment
import com.scyele.familyfinanceassistant.ui.main.MainActivity
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.lib.extension.DateSelectorSelectionType
import kotlinx.android.synthetic.main.accounts_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*



class AccountsFragment : BaseFragment<
    AccountsFragmentViewModel,
    AccountsFragmentBinding,
    AccountsFragmentViewModel.State,
    FragmentViewModelFactory>() {

  private lateinit var accountsAdapter: AccountsAdapter

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getLayoutId(): Int {
    return R.layout.accounts_fragment
  }

  override fun setupView() {
    binding.accountsList.layoutManager =
      LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
    accountsAdapter =
      AccountsAdapter({ viewModel.onAccountClicked(it) }, { viewModel.onAccountLongClicked(it) })
    binding.accountsList.adapter = accountsAdapter
  }

  override fun tag() = "AccountsFragment"

  override fun getModelClass(): Class<AccountsFragmentViewModel> {
    return AccountsFragmentViewModel::class.java
  }

  override fun bindingViewModeSetViewModel() {
    binding.viewModel = viewModel
    viewModel.getAccountSummaries().observe(viewLifecycleOwner, Observer {
      accountsAdapter.submitList(it)
    })
    date_selector.setDate(Date())
    date_selector.setSelectionType(DateSelectorSelectionType.ALL)
    date_selector.setDateSelectionChangedListener { date: Date, selectionType: DateSelectorSelectionType ->
      viewModel.onDateSelectionChanged(date, selectionType)
    }
  }

  override fun handleEvents(state: AccountsFragmentViewModel.State) {
    state.editAccountCategoryEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(AccountSetupFragment(), activity)
    }
    state.startTransferEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(TransferFragment(), activity)
    }
    state.showHistoryEvent?.getContent()?.let {
      // TODO fix this to repo usage
      if (activity is MainActivity) {
        (activity as MainActivity).showFragment(HistoryFragment(), true)
      }
    }
  }
}