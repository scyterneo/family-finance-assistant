package com.scyele.familyfinanceassistant.ui.incomes.main

import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.summary.IncomeSummary
import com.scyele.familyfinanceassistant.ui.operation.main.OperationsFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class IncomesFragment :
  OperationsFragment<Income, IncomeCategory, IncomeSummary, IncomesFragmentViewModel>() {

  override fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  override fun getModelClass(): Class<IncomesFragmentViewModel> {
    return IncomesFragmentViewModel::class.java
  }
}