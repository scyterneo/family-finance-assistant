package com.scyele.familyfinanceassistant.di.components

import com.scyele.familyfinanceassistant.di.modules.LoginActivityModule
import com.scyele.familyfinanceassistant.ui.login.LoginActivity
import dagger.Component
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Singleton



@Singleton
@Component(
  modules = [
    (LoginActivityModule::class)
  ]
)
interface LoginActivityComponent {

  fun inject(loginActivity: LoginActivity)

  @Component.Builder
  interface Builder {
    fun build(): LoginActivityComponent

    fun loginActivityModule(loginActivityModule: LoginActivityModule): Builder
  }
}