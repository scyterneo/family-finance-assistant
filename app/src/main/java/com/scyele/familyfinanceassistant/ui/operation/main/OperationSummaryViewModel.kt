package com.scyele.familyfinanceassistant.ui.operation.main

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.ui.base.BaseVM
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



class OperationSummaryViewModel : BaseVM() {

  private val operationName = MutableLiveData<String>()
  private val operationSum = MutableLiveData<String>()
  private val isParent = MutableLiveData<Boolean>()
  private val isCollapsed = MutableLiveData<Boolean>()
  private val level = MutableLiveData<Int>()

  fun bind(operationSummary: OperationSummary) {
    operationName.value = operationSummary.operationCategory().name()
    operationSum.value = operationSummary.operationSum().toString()
    isParent.value = operationSummary.operationCategory().isParentCategory()
    isCollapsed.value = operationSummary.isCollapsedSummary()
    level.value = operationSummary.levelOfDepth()
  }

  fun getOperationName() = operationName
  fun getOperationSum() = operationSum
  fun isParent() = isParent
  fun isCollapsed() = isCollapsed
  fun getLevel() = level
}