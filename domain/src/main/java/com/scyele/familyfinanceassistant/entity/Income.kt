package com.scyele.familyfinanceassistant.entity

import com.scyele.familyfinanceassistant.utils.Guid
import java.math.BigDecimal
import java.util.*


data class Income(
  val guid: String,
  var sum: String = "0",
  var accountCategoryGuid: String,
  var incomeCategoryGuid: String,
  var date: Date = Date(),
  var comment: String? = null
) : Operation {

  constructor(
    sum: String,
    accountCategoryGuid: String,
    expenseCategoryGuid: String,
    date: Date = Date(),
    comment: String? = null
  ) : this(Guid.new(), sum, accountCategoryGuid, expenseCategoryGuid, date, comment)

  fun sum(value: BigDecimal) {
    sum = value.toString()
  }

  // Empty private constructor for DataSnapshot.getValue method
  private constructor() : this("", "0", "", "")

  override fun guid() = guid

  override fun sum() = sum

  override fun accountCategoryGuid() = accountCategoryGuid

  override fun operationCategoryGuid() = incomeCategoryGuid

  override fun date() = date

  override fun comment() = comment
}

fun List<Income>.find(guid: String): Income {
  return this.first { it.guid == guid }
}