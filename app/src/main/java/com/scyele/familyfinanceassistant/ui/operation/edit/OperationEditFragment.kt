package com.scyele.familyfinanceassistant.ui.operation.edit

import androidx.lifecycle.Observer
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.OperationEditLayoutBinding
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.ui.base.BaseDialogFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.familyfinanceassistant.utils.helper.ToastHelper
import com.scyele.lib.extension.initSpinner
import com.scyele.lib.extension.setupSpinner
import kotlinx.android.synthetic.main.operation_edit_layout.*


import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi



abstract class OperationEditFragment<
    Action,
    Category,
    Summary,
    Model>
  : BaseDialogFragment<
    Model,
    OperationEditLayoutBinding,
    OperationEditFragmentViewModel.State,
    FragmentViewModelFactory>()
    where Action : Operation,
          Category : OperationCategory,
          Summary : OperationSummary,
          Model : OperationEditFragmentViewModel<Action, Category> {

  override fun getLayoutId() = R.layout.operation_edit_layout

  override fun bindToViewModel() {
    binding.viewModel = viewModel

    initSpinner(
      spinner_operation_from_category,
      viewModel.onOperationCategorySelected
    )
    initSpinner(
      spinner_operation_to_account,
      viewModel.onAccountCategorySelected
    )

    viewModel.operationCategories.observe(this, Observer {
      setupSpinner(
        spinner_operation_from_category,
        requireContext(),
        it.map { operationCategory -> operationCategory.name() })
    })

    viewModel.operationCategoryPosition.observe(this, Observer {
      spinner_operation_from_category.setSelection(it)
    })

    viewModel.accountCategories.observe(this, Observer {
      setupSpinner(
        spinner_operation_to_account,
        requireContext(),
        it.map { accountCategory -> accountCategory.name })
    })

    viewModel.accountCategoryPosition.observe(this, Observer {
      spinner_operation_to_account.setSelection(it)
    })

    operation_header.text = getHeaderText()
  }

  override fun handleEvents(state: OperationEditFragmentViewModel.State) {
    state.selectDate?.getContent()?.let { currentDate ->
      DialogHelper.showDateSelectionDialog(
        activity,
        { newDate -> viewModel.updateDate(newDate) },
        currentDate
      )
    }
    state.addOperationComplete?.getContent()?.let {
      dismiss()
    }
    state.addOperationError?.getContent()?.let {
      ToastHelper.showToast(requireContext(), getString(R.string.operation_error))
    }
    state.sum.getContent()?.let {
      operation_sum_input.setText(it)
    }
    state.commentary.getContent()?.let {
      operation_comment_input.setText(it)
    }
    state.deleteOperation?.getContent()?.let {
      dismiss()
    }
    state.rate.getContent()?.let {
      currency_rate_input.setText(it)
      text_currency_conversion.text = getString(R.string.currency_conversion_text, it)
    }
  }

  override fun tag() = "EditOperationFragment"

  protected abstract fun getHeaderText(): String
}