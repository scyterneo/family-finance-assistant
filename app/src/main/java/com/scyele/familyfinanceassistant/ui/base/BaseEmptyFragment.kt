package com.scyele.familyfinanceassistant.ui.base

import androidx.fragment.app.Fragment

abstract class BaseEmptyFragment: Fragment() {
  abstract fun tag(): String
}