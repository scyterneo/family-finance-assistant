package com.scyele.familyfinanceassistant.ui.incomes.setup

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Income
import com.scyele.familyfinanceassistant.entity.IncomeCategory
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.MoveToRepository
import com.scyele.familyfinanceassistant.repository.OperationCategorySetupRepository
import com.scyele.familyfinanceassistant.summary.IncomeSummary
import com.scyele.familyfinanceassistant.ui.base.Event
import com.scyele.familyfinanceassistant.ui.operation.setup.OperationCategorySetupFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi



class IncomeCategorySetupFragmentViewModel(
  operationCategorySetupRepository: OperationCategorySetupRepository,
  mainRepository: MainRepository,
  moveToRepository: MoveToRepository
) :
  OperationCategorySetupFragmentViewModel<Income, IncomeCategory, IncomeSummary>(
    operationCategorySetupRepository,
    mainRepository,
    moveToRepository
  ) {

  override fun getOperations(): MutableLiveData<List<Income>> {
    return mainRepository.incomes
  }

  override fun getOperationCategories(): MutableLiveData<List<IncomeCategory>> {
    return mainRepository.incomeCategories
  }

  override fun createNewOperationCategory(
    name: String,
    isParent: Boolean,
    parentGuid: String?,
    guid: String
  ): OperationCategory {
    return IncomeCategory(name, isParent, parentGuid, guid)
  }

  override fun getTypedEvent(): Event<OperationType> {
    return Event(OperationType.INCOME)
  }
}