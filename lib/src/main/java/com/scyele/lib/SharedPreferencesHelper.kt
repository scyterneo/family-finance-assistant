package com.scyele.lib

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.preference.PreferenceManager


class SharedPreferencesHelper(private val context: Context) {

  private val defaultPreferences: SharedPreferences
      by lazy { PreferenceManager.getDefaultSharedPreferences(context) }

  private val operationPreferences: SharedPreferences
      by lazy { context.getSharedPreferences(OPERATION_PREFERENCES_FILE_NAME, MODE_PRIVATE) }

  fun getString(key: String): String? {
    return defaultPreferences.getString(key, null)
  }

  fun set(key: String, value: String) {
    defaultPreferences.edit().putString(key, value).apply()
  }

  fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
    return defaultPreferences.getBoolean(key, defaultValue)
  }

  fun set(key: String, value: Boolean) {
    defaultPreferences.edit().putBoolean(key, value).apply()
  }

  fun getInt(key: String): Int {
    return defaultPreferences.getInt(key, 0)
  }

  fun set(key: String, value: Int) {
    defaultPreferences.edit().putInt(key, value).apply()
  }

  fun setOperationAccountCategory(operationCategoryGuid: String, accountCategoryGuid: String) {
    operationPreferences.edit().putString(operationCategoryGuid, accountCategoryGuid).apply()
  }

  fun getOperationAccountCategory(operationCategoryGuid: String): String {
    return operationPreferences.getString(operationCategoryGuid, "") ?: ""
  }

  fun setRate(rate: Float) {
    defaultPreferences.edit().putFloat(RATE, rate).apply()
  }

  fun getRate(): Float {
    return defaultPreferences.getFloat(RATE, 1f)
  }

  companion object {
    private val OPERATION_PREFERENCES_FILE_NAME by lazy { "Operation" }
    private val RATE by lazy { "Rate" }
  }
}
