package com.scyele.familyfinanceassistant.utils.helper

import android.content.Context
import android.widget.Toast

object ToastHelper {
  fun showToast(context: Context?, message: String) {
    if (context == null) return

    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
  }
}