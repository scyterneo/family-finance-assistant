package com.scyele.familyfinanceassistant.ui.settings.location

import com.scyele.familyfinanceassistant.repository.AuthRepository
import com.scyele.familyfinanceassistant.ui.DefaultMainRepositoryTest
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test


internal class ShareLocationFragmentViewModelTest : DefaultMainRepositoryTest() {
  lateinit var testingVM: ShareLocationFragmentViewModel

  private var state: ShareLocationFragmentViewModel.State? = null

  private val authRepository = mockk<AuthRepository> {
    every { getUserID() } returns "123"
  }

  @Nested
  inner class InitState {
    init {
      testingVM = ShareLocationFragmentViewModel(
        getMain(),
        authRepository
      )

      testingVM.state().observeForever { state = it }
    }

    @Test
    fun initState() = runBlocking<Unit> {
      assertEquals("12345678", state?.locationPassword?.peekContent())
      assertEquals(
        ShareLocationFragmentViewModel.EditPasswordState.SET,
        state?.editPasswordState?.peekContent()
      )
    }
  }
}