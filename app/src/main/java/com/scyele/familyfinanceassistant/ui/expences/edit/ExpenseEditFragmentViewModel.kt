package com.scyele.familyfinanceassistant.ui.expences.edit

import androidx.lifecycle.MutableLiveData
import com.scyele.familyfinanceassistant.entity.Expense
import com.scyele.familyfinanceassistant.entity.ExpenseCategory
import com.scyele.familyfinanceassistant.repository.ExpenseEditRepository
import com.scyele.familyfinanceassistant.repository.MainRepository
import com.scyele.familyfinanceassistant.repository.ProgressRepository
import com.scyele.familyfinanceassistant.ui.operation.edit.OperationEditFragmentViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*


class ExpenseEditFragmentViewModel(
  mainRepository: MainRepository,
  expenseEditRepository: ExpenseEditRepository,
  progressRepository: ProgressRepository
) : OperationEditFragmentViewModel<Expense, ExpenseCategory>(
  mainRepository,
  expenseEditRepository,
  progressRepository
) {

  override fun getAllOperationCategories(): MutableLiveData<List<ExpenseCategory>> {
    return mainRepository.expenseCategories
  }

  override fun createNewOperation(
    guid: String,
    sum: String,
    accountCategoryGuid: String,
    actionCategoryGuid: String,
    date: Date,
    comment: String?
  ): Expense {
    return Expense(guid, sum, accountCategoryGuid, actionCategoryGuid, date, comment)
  }

  override fun getOperations(): MutableLiveData<List<Expense>> {
    return mainRepository.expenses
  }
}