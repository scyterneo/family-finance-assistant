package com.scyele.familyfinanceassistant.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.ads.MobileAds
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import com.scyele.familyfinanceassistant.AssistantApplication
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.ActivityMainBinding
import com.scyele.familyfinanceassistant.ui.accounts.AccountsFragment
import com.scyele.familyfinanceassistant.ui.base.BaseEmptyFragment
import com.scyele.familyfinanceassistant.ui.expences.main.ExpensesFragment
import com.scyele.familyfinanceassistant.ui.history.HistoryFragment
import com.scyele.familyfinanceassistant.ui.incomes.main.IncomesFragment
import com.scyele.familyfinanceassistant.ui.settings.SettingsFragment
import com.scyele.familyfinanceassistant.ui.statistics.StatisticsFragment
import com.scyele.lib.extension.livedata.observeNonNull
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Inject



class MainActivity : AppCompatActivity() {

  private lateinit var accountsFragment: AccountsFragment
  private lateinit var incomesFragment: IncomesFragment
  private lateinit var expensesFragment: ExpensesFragment
  private lateinit var statisticsFragment: StatisticsFragment
  private lateinit var settingsFragment: SettingsFragment

  private lateinit var binding: ActivityMainBinding
  private lateinit var viewModel: MainActivityViewModel

  @Inject
  lateinit var factory: MainActivityViewModelFactory

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    MobileAds.initialize(this) {}

    injectDependency()
    binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    viewModel = ViewModelProvider(this, factory).get(MainActivityViewModel::class.java)
    binding.viewModel = viewModel

    createFragments()
    navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

    viewModel.state().observeNonNull(this) {
      handleEvents(it)
    }
    navigation.itemIconTintList = null
  }

  private fun handleEvents(state: MainActivityViewModel.State) {
    state.showAccountsScreen?.getContent()?.let {
      showFragment(accountsFragment, it)
      navigation.menu.findItem(R.id.navigation_accounts).isChecked = true
    }
    state.showIncomeScreen?.getContent()?.let {
      showFragment(incomesFragment, it)
      navigation.menu.findItem(R.id.navigation_income).isChecked = true
    }
    state.showExpensesScreen?.getContent()?.let {
      showFragment(expensesFragment)
      navigation.menu.findItem(R.id.navigation_expenses).isChecked = true
    }
    state.showStatisticsScreen?.getContent()?.let {
      // currently replaced with History Screen
      //showFragment(statisticsFragment)
      showFragment(HistoryFragment())
      navigation.menu.findItem(R.id.navigation_statistics).isChecked = true
    }
    state.showSettingsScreen?.getContent()?.let {
      showFragment(settingsFragment)
      navigation.menu.findItem(R.id.navigation_settings).isChecked = true
    }
  }

  private fun createFragments() {
    accountsFragment = AccountsFragment()
    incomesFragment = IncomesFragment()
    expensesFragment = ExpensesFragment()
    statisticsFragment = StatisticsFragment()
    settingsFragment = SettingsFragment()
  }

  fun showFragment(fragment: BaseEmptyFragment, addToBacStack: Boolean = false) {
    val fragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.replace(R.id.content, fragment, fragment.tag())
    // TODO add instead of replace required. This requires layout rework
    if (addToBacStack) {
      fragmentTransaction.addToBackStack(fragment.tag())
    }
    fragmentTransaction.commit()
  }

  private fun injectDependency() {
    AssistantApplication.instance.appComponent.inject(this)
  }

  private val onNavigationItemSelectedListener = OnNavigationItemSelectedListener { item ->
    when (item.itemId) {
      R.id.navigation_accounts -> {
        viewModel.onNavigationAccountsClicked()
        return@OnNavigationItemSelectedListener true
      }
      R.id.navigation_income -> {
        viewModel.onNavigationIncomeClicked()
        return@OnNavigationItemSelectedListener true
      }
      R.id.navigation_expenses -> {
        viewModel.onNavigationExpenseClicked()
        return@OnNavigationItemSelectedListener true
      }
      R.id.navigation_statistics -> {
        viewModel.onNavigationStatisticsClicked()
        return@OnNavigationItemSelectedListener true
      }
      R.id.navigation_settings -> {
        viewModel.onNavigationSettingsClicked()
        return@OnNavigationItemSelectedListener true
      }
    }
    false
  }
}
