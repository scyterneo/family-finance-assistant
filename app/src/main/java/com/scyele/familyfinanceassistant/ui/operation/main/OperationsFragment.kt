package com.scyele.familyfinanceassistant.ui.operation.main

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.scyele.familyfinanceassistant.R
import com.scyele.familyfinanceassistant.databinding.OperationsFragmentBinding
import com.scyele.familyfinanceassistant.entity.Operation
import com.scyele.familyfinanceassistant.entity.OperationCategory
import com.scyele.familyfinanceassistant.entity.OperationType
import com.scyele.familyfinanceassistant.summary.OperationSummary
import com.scyele.familyfinanceassistant.ui.base.BaseFragment
import com.scyele.familyfinanceassistant.ui.base.FragmentViewModelFactory
import com.scyele.familyfinanceassistant.ui.expences.edit.ExpenseEditFragment
import com.scyele.familyfinanceassistant.ui.expences.setup.ExpenseCategorySetupFragment
import com.scyele.familyfinanceassistant.ui.history.HistoryFragment
import com.scyele.familyfinanceassistant.ui.incomes.edit.IncomeEditFragment
import com.scyele.familyfinanceassistant.ui.incomes.setup.IncomeCategorySetupFragment
import com.scyele.familyfinanceassistant.ui.main.MainActivity
import com.scyele.familyfinanceassistant.utils.helper.DialogHelper
import com.scyele.lib.extension.DateSelectorSelectionType
import kotlinx.android.synthetic.main.operations_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*



abstract class OperationsFragment<
    Action,
    Category,
    Summary,
    Model>
  : BaseFragment<
    Model,
    OperationsFragmentBinding,
    OperationsFragmentViewModel.State,
    FragmentViewModelFactory>()
    where Action : Operation,
          Category : OperationCategory,
          Summary : OperationSummary,
          Model : OperationsFragmentViewModel<Action, Category, Summary> {

  private lateinit var operationSummariesAdapter: OperationSummariesAdapter


  override fun getLayoutId(): Int {
    return R.layout.operations_fragment
  }

  override fun setupView() {
    binding.operationCategoriesList.layoutManager =
      LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
    operationSummariesAdapter = OperationSummariesAdapter(
      { viewModel.onOperationCategoryClicked(it) },
      { viewModel.onOperationCategoryLongClicked(it) })
    binding.operationCategoriesList.adapter = operationSummariesAdapter
  }

  override fun bindingViewModeSetViewModel() {
    binding.viewModel = viewModel
    date_selector.setDateSelectionChangedListener { date: Date, selectionType: DateSelectorSelectionType ->
      viewModel.onDateSelectionChanged(date, selectionType)
    }
  }

  override fun handleEvents(state: OperationsFragmentViewModel.State) {
    state.init.getContent()?.let {
      if (it is OperationsScreenInit.Loaded) {
        operation_categories_list.visibility = View.VISIBLE
        loading_message.visibility = View.GONE
        operationSummariesAdapter.submitList(it.operationSummaryModels)
      } else if (it is OperationsScreenInit.Loading) {
        operation_categories_list.visibility = View.GONE
        loading_message.visibility = View.VISIBLE
        loading_message.text = it.emptyParts.joinToString { operationsScreenLoadingParts ->
          operationsScreenLoadingParts.loadingPartName
        }
      }
    }
    state.editOperationCategoryEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(
        when (it) {
          OperationType.INCOME -> IncomeCategorySetupFragment()
          OperationType.EXPENSE -> ExpenseCategorySetupFragment()
        },
        activity
      )
    }
    state.editOperationEvent?.getContent()?.let {
      DialogHelper.showDialogFragment(
        when (it) {
          OperationType.INCOME -> IncomeEditFragment()
          OperationType.EXPENSE -> ExpenseEditFragment()
        },
        activity
      )
    }
    state.showOperationCategoryHistoryEvent?.getContent()?.let {
      // TODO fix this to repo usage
      if (activity is MainActivity) {
        (activity as MainActivity).showFragment(HistoryFragment(), true)
      }
    }
  }

  override fun tag() = "OperationsFragment"
}